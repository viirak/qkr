# encoding=utf8
# _author_:viirak
# _date_: January 09, 2016
# coding=utf8

import re
import os
import json
from extraction.techniques import Technique
from extraction import Extractor, Extracted
from lxml import html
from bs4 import BeautifulSoup

class ProductExtracted(Extracted):
    def __init__(self, brands=None, prices=None, *args, **kwargs):
        self.brands = brands or []
        self.prices = prices or []
        super(ProductExtracted, self).__init__(*args, **kwargs)

    @property
    def brand(self):
        return self.brands[0] if self.brands else None

    @property
    def price(self):
        return self.prices[0] if self.prices else None

class ProductExtractor(Extractor):
    extracted_class = ProductExtracted
    def __init__(self):
        super(ProductExtractor, self).__init__()
        self.techniques.append("qkr.extraction_extra.ProductTechnique")


class ProductTechnique(Technique):
    """ Technique to scrape product price from product page """

    def extract(self, tree):
        soup = BeautifulSoup(tree, "lxml")
        price = None
        currency = None
        # look for itemprop
        price_items = soup.find_all(attrs={'itemprop':'price'})
        currency_items = soup.find_all(attrs={'itemprop':'priceCurrency'})
        if len(price_items) == 1:
            if len(price_items) > 0:
                price_val = price_items[0].get('content', None)
            if len(currency_items) > 0:
                currency = currency_items[0].get('content', None)
            if not price_val:
                item_decs = [re.sub(r"\s+",' ',d).strip() for d in price_items[0].descendants if not d.name]
                price_str = ''.join(item_decs)
                price = self.price_data_from_string(price_str)
                price_val = price['amount']
                if price['code'] and not price['currency'] and not currency:
                    with open(os.path.join(os.path.dirname(__file__), 'data', 'world_currencies.json'), 'r') as f:
                        wc = json.load(f)
                        price['currency'] = next((x['currency'] for x in wc if x['code'] == price['code']), None)
                        currency = price['currency']
            price = (currency, price_val)
        if not price:
            print "### start scraping ... "
            addtocart_btn = self.find_add_to_cart_button(soup)
            if addtocart_btn:
                price = self.get_product_price(addtocart_btn)
            else:
                print "### button not found."

        print "### price: " + str(price)
        return {'brands':[],'prices':[price]}

    def get_price_el_score(self, el):
        score = 1

        el_vals = [c for c in el.contents if not c.name]
        el_val = el_vals[0].encode('utf-8').strip()
        if el_val.replace('$','') in ['0.00','0']:
            score -= 5

        # attrs = ['id','class','title','name','value','type','itemprop']
        baned_attrs = ['scr','href','onClick','itemtype']
        el_attrs = self.get_tag_attrs(el=el, baned_keys=baned_attrs, parent=True, parent_depth=9, parent_sibling=True)
        styles = []
        all_attrs = ','.join(el_attrs)

        kw01 = [
            'offer',
            'sell',
            'selling',
            'sale',
            'our',
            'main',
            'special',
            'current',
            'low',
            'min',
            'final',
            'total',
            'purchase',
            'discount',
            'discounted',
            'regular',
            'now'
        ]
        kw02 = [
            'list',
            'old',
            'was',
            'low',
            'retail',
            'normal',
            'standard',
            'regular'
        ]
        if any(s in all_attrs for s in ['price'])\
        or any(s in all_attrs for s in kw01):
            score += 1
        if any(s in all_attrs for s in kw02):
            score -= 1

        for k in kw01:
            signs = [' ','-','_','']
            ss = [k+s+'price' for s in signs]
            if any(s in all_attrs for s in ss):
                score += 5

        for k in kw02:
            signs = [' ','-','_','']
            ss = [k+s+'price' for s in signs]
            if any(s in all_attrs for s in ss):
                score -= 1

        if all(s in all_attrs for s in ['item','amount']):
            score += 2
        relstrs = [
            'recom',
            'similar',
            'related',
            'toppro',
            'cross',
            'quick',
            'upsell',
            'suggest',
            'cobuy'
        ]
        if any(s in all_attrs for s in relstrs)\
        or any(s in all_attrs for s in ['strike','del'])\
        or any(s in all_attrs for s in ['display:none','hidden'])\
        or all(s in [x.name for x in el.parents] for s in ['select','option']):
            score -= 10
        if all(s in all_attrs for s in ['people','also','view'])\
        or all(s in all_attrs for s in ['best','sell'])\
        or any(s in all_attrs for s in ['rate','rating','star','size'])\
        or any(s in all_attrs for s in ['carousel','slider'])\
        or any(s in all_attrs for s in ['save','saving','shipping']):
            score -= 5
        return score

    def get_tag_attrs(self, el, attr_keys=None, baned_keys=None, parent=False, parent_depth=None, parent_sibling=False):
        attrs = []
        els = [el]
        baned_tags = ['html','body','head','script','style']
        if parent:
            parents = [el]
            for p in el.parents:
                parents.append(p)
            depth = parent_depth if parent_depth and parent_depth < len(parents) else len(parents)
            for i in range(0,depth):
                if parents[i] and parents[i].name not in baned_tags:
                    els.append(parents[i])
                    # get parent header string
                    for parent_content in parents[i].contents:
                        if parent_content.name and re.match(r"h[1-6]", parent_content.name, re.IGNORECASE):
                            if parent_content.string:
                                attrs.append(parent_content.string)
                    # get sibling string
                    if parent_sibling:
                        presibs = [s for s in parents[i].previous_siblings if s.encode('utf-8').strip() != '']
                        if presibs:
                            sib_strs = []
                            for sib in presibs:
                                sibstr = None
                                if not sib.name:
                                    sibstr = sib
                                else:
                                    if sib.name not in baned_tags and sib.string:
                                        sibstr = sib.string

                                if sibstr and not re.search(r"[\{\}\:\"\/\<\>\s+]", sibstr):
                                    sstr = re.sub(r"\s+",' ',sibstr).strip() if sibstr else None
                                    elstr = re.sub(r"\s+",' ',el.string).strip() if el.string else None
                                    if sibstr and sstr != elstr:
                                        if isinstance(sstr, str):
                                            sstr = sstr.decode("utf-8") # str to unicode
                                        sib_strs.append(sstr)
                            attrs += sib_strs

        for e in els:
            for key,val in e.attrs.iteritems():
                if val:
                    if attr_keys and key in attr_keys\
                    or not attr_keys and (baned_keys and key not in baned_keys):
                        if isinstance(val, list):
                            attrs += val
                        else:
                            if not re.search(r"[\{\}\:\"\/\<\>]", val):
                                attrs.append(val.lower())
        return attrs

    def find_add_to_cart_button(self, soup):
        regex = re.compile(r"(add(?:[\s+\-_])?(?:to)?(?:[\s+\-_])?(?:shopping)?(?:[\s+\-_])?(?:cart|bag|basket|pack|tote))|((buy$)|(buy\s(?:it)?\s+?now))", re.IGNORECASE)
        candidates = []

        btn_tags = ['input','button','a','span']
        rs = []
        for t in btn_tags:
            rs += soup.find_all(t)

        if not rs:
            return None
        for r in rs:
            is_ok = True
            attrs = ['id','class','name','title','value','alt','src']
            if r.name == 'input':
                inp_type = r.attrs.get('type', None)
                if inp_type == 'hidden':
                    is_ok = False
                if any(s in self.get_tag_attrs(el=r, attr_keys=attrs, parent=False) for s in ['quantity','qty']):
                    is_ok = False
            if is_ok:
                kw = [d.encode('utf-8').strip() for d in r.descendants if not d.name]
                kw += self.get_tag_attrs(el=r, attr_keys=attrs, parent=False)
                kw += self.get_tag_attrs(el=r.parent, attr_keys=attrs, parent=False)
                if regex.search(str(kw)):
                    if not any(d['el'] == r for d in candidates):
                        candidates.append({'el':r,'score':1})

        scores = []
        if len(candidates) > 1:
            for can in candidates:
                score = 1

                # attrs of current tag
                attrs = ['id','class','title','name','value','type']
                t_attrs = self.get_tag_attrs(el=can['el'], attr_keys=attrs, parent=True, parent_depth=7, parent_sibling=False)
                t_attrs_str = ','.join(t_attrs).lower()
                style_attrs_str = ','.join(self.get_tag_attrs(el=can['el'], attr_keys=['style']))

                kw01 = [
                    'recommend',
                    'related',
                    'slider',
                    'topProd',
                    'carousel',
                    'upsell',
                    'cross',
                    'modal',
                    'productlist',
                    'bestseller',
                    'highlighted',
                    'special',
                    'items'
                ]
                if any(x in t_attrs_str for x in kw01):
                    score -= 5

                if all(x in style_attrs_str for x in ['display','none']):
                    score -= 5

                if any(x in t_attrs_str for x in ['hidden']):
                    score -= 5

                kw02 = [
                    'primary',
                    'main'
                ]
                if any(x in t_attrs_str for x in kw02):
                    score += 1

                title = can['el'].attrs.get('title', None)
                if title and regex.search(title):
                    score += 1

                value = can['el'].attrs.get('value', None)
                if value and regex.search(value):
                    score += 1

                decs_str = ','.join([d for d in can['el'].descendants if not d.name])
                if regex.search(decs_str):
                    score += 1

                cols = ['left','right']
                for k in cols:
                    if all(s in t_attrs_str for s in ['col',k]):
                        score -= 5

                cols = ['seller','product','additional','category']
                for k in cols:
                    if all(s in t_attrs_str for s in ['list',k]):
                        score -= 5

                if any(x in t_attrs_str for x in ['nav','menu']):
                    score -= 5

                can['score'] = score
                scores.append(score)

            candidates = [max(candidates, key=lambda x:x['score'])]
        return candidates[0]['el'] if candidates else None

    def get_product_price(self, addtocart_button):
        candidates = []
        parent_els = [p for p in addtocart_button.parents]
        for d in parent_els[len(parent_els)-2].descendants:
            if self.is_valid_price_el(d):
                val = re.sub(r"\s+",' ',d)
                d_clean = val.replace(u'\xa0', u' ').strip().encode('utf-8')
                if re.match(r'^(\d+(?:[.,]\d+)?(?:[.,]\d+)?(?:[.,]\d+)?)$',d_clean):
                    ddec = [x for x in d.parent.parent.descendants if not x.name and not re.search(r'\d',x)]
                    ddec = [x for x in ddec if self.is_valid_currency_el(x)]
                    ddec = list(set(ddec)) if len(ddec)>1 else ddec
                    d_clean = ddec[0] +" "+d_clean if ddec else None
                if d_clean and not any(x['val'] == d_clean and x['el'] == d.parent for x in candidates):
                    score = self.get_price_el_score(d.parent)
                    depth = self.get_el_distance(addtocart_button, d.parent)
                    candidates.append({'el':d.parent,'val':d_clean,'depth':depth,'score':score})

        for can in candidates:
            print "*** can : " + can['val'] + " d:" + str(can['depth']) +" s:" + str(can['score'])
            # print str(can['el'])

        if len(candidates) > 1:
            candidates = [c for c in candidates if c.get('depth') == min([x.get('depth') for x in candidates])]

        if len(candidates) > 1:
            candidates = [max(candidates, key=lambda x:x['score'])]

        if candidates:
            price = self.price_data_from_string(candidates[0]['val'])
            price['currency'] = 'USD' if price['code'] == '$' else price['currency']
            if price['code'] and not price['currency']:
                with open(os.path.join(os.path.dirname(__file__), 'data', 'world_currencies.json'), 'r') as f:
                    wc = json.load(f)
                    price['code'] = price['code'].decode("utf-8") if isinstance(price['code'], str) else price['code']
                    price['currency'] = next((x['currency'] for x in wc if x['code'] == price['code']), None)
            return (price['currency'], price['amount'])
        return None

    def get_el_distance(self, el1, el2):
        distance = 0
        for p in el1.parents:
            distance += 1
            if p and p.name not in ['html','body','script','style']:
                if el2 in p.descendants:
                    return distance
        return distance

    def is_valid_el(self, el):
        isvalid = True
        baned_tags = ['script','strike','s','del','style','option','a','input']
        if el.name or any(tag in baned_tags for tag in [el.name, el.parent.name, el.parent.parent.name]):
            isvalid = False
        else:
            val = re.sub(r"\s+",' ',el)
            val = val.replace(u'\xa0', u' ').strip()
            special_chars = ['[',']','<','>','{','}','(',')','/','=','-','_','~','%',':','#','@','&','!']

            if val == ''\
            or val.count(' ') > 2\
            or val.count('.') > 1\
            or any(x in el for x in special_chars)\
            or re.match("^[a-zA-Z]+\d+[a-zA-Z]+$", val):
                isvalid = False

            if isvalid:
                tag_attrs = self.get_tag_attrs(
                    el=el.parent,
                    baned_keys=['onClick','scr','href'],
                    parent=True, parent_depth=9,
                    parent_sibling=False
                    )
                relstrs = [
                    'featured',
                    'recommend',
                    'related',
                    'similar',
                    'slider',
                    'topprod',
                    'carousel',
                    'upsell',
                    'cross-sell',
                    'productlist',
                    'bestseller',
                    'highlighted',
                    'suggest',
                    'cobuy'
                ]
                relstrs += ['size','count','shipping']
                if any(re.search("\W"+x,','.join(tag_attrs).lower()) for x in relstrs):
                    isvalid = False
        return isvalid

    def is_valid_price_el(self,el):
        isvalid = self.is_valid_el(el)
        if isvalid:
            val = re.sub(r"\s+",' ',el)
            val = val.replace(u'\xa0', u' ').strip()
            if int(re.sub(r'\D', '0', val)) == 0\
            or val.isdigit() and int(val) > 9999:
                isvalid=False
            if isvalid and re.match(r'^(\d+(?:[.,]\d+)?(?:[.,]\d+)?(?:[.,]\d+)?)$',val):
                ddec = [x for x in el.parent.parent.descendants if not x.name and not re.search(r'\d',x)]
                ddec = [x for x in ddec if self.is_valid_currency_el(x)]
                ddec = list(set(ddec)) if len(ddec)>1 else ddec
                if not ddec:
                    isvalid=False
                else:
                    val = ddec[0].decode("utf-8")+val if isinstance(ddec[0], str) else ddec[0]+val
            if isvalid:
                price = self.price_data_from_string(val)
                if not price['code'] and not price['currency']\
                or price['code'] and not self.is_valid_currency_code(price['code'])\
                or price['currency'] and not self.is_valid_currency(price['currency']):
                    isvalid=False
        return isvalid

    def is_valid_currency_el(self,el):
        isvalid = self.is_valid_el(el)
        if isvalid:
            val = re.sub(r"\s+",' ',el)
            val = val.replace(u'\xa0', u' ').strip()
            if re.search('\d',val)\
            or len(val)>4\
            or not (self.is_valid_currency(val) or self.is_valid_currency_code(val)):
                isvalid=False
        return isvalid

    def is_valid_currency(self, currency):
        with open(os.path.join(os.path.dirname(__file__), 'data', 'world_currencies.json'), 'r') as f:
            currencies = [c['currency'] for c in json.load(f)]
            return True if currency in currencies else False

    def is_valid_currency_code(self, code):
        with open(os.path.join(os.path.dirname(__file__), 'data', 'world_currencies.json'), 'r') as f:
            codes = [x['code'] for x in json.load(f)]
            if isinstance(code, str):
                code = code.decode("utf-8") # str to unicode
            return True if code in codes else False

    def price_data_from_string(self, price_str):
        price={'code':None,'currency':None,'amount':None}
        rxs = [
            "^(\D{1,3})?(?:\s)?(\d+(?:[.,]\d+)?(?:[.,]\d+)?(?:[.,]\d+)?)(?:\s)?(\D{1,3})?(?:\s)?([A-Z]{3})$",
            "^([A-Z]{3})(\D{1,3})?(?:\s)?(\d+(?:[.,]\d+)?(?:[.,]\d+)?(?:[.,]\d+)?)(?:\s)?(\D{1,3})?$",
            "^(\D{1,3})(?:\s)?(\d+(?:[.,]\d+)?(?:[.,]\d+)?(?:[.,]\d+)?)$",
            "^(\d+(?:[.,]\d+)?(?:[.,]\d+)?(?:[.,]\d+)?)(?:\s)?(\D{1,3})$"
        ]
        rx_match = None
        rx_i = None
        for i in range(0, len(rxs)):
            rx_i = i
            rx_match = re.match(rxs[i],price_str)
            if rx_match:
                break
        if rx_match:
            if rx_i == 0:
                price['code'] = rx_match.group(1) if rx_match.group(1) else rx_match.group(3)
                price['currency'] = rx_match.group(4)
                price['amount'] = rx_match.group(2)
            elif rx_i == 1:
                price['code'] = rx_match.group(2) if rx_match.group(2) else rx_match.group(2)
                price['currency'] = rx_match.group(1)
                price['amount'] = rx_match.group(3)
            elif rx_i == 2:
                price['code'] = rx_match.group(1)
                price['amount'] = rx_match.group(2)
            elif rx_i == 3:
                price['code'] = rx_match.group(2)
                price['amount'] = rx_match.group(1)
        for k,v in price.iteritems():
            if v:
                x = v.strip()
                if isinstance(x, str):
                    x = x.decode('utf-8')
                price[k] = x
        return price
