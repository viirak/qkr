# ui modules
import tornado.web


class AuthMenu(tornado.web.UIModule):
    def render(self, current=None):
        return self.render_string('auth/_menu.html', current=current)