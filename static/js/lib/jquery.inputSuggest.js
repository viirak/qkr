// PLUGIN
;
(function ($, window, document, undefined) {
    var pluginName = 'inputSuggest',
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };

    //constructor
    function Plugin(ele, options) {
        var noop = function () {
        };
        var defaults = {
            serviceUrl: null, // remote data url for suggesting
            lookupLocal: null, // local data for testing ...
            appendTo: 'body',
            isRelative: false, // if container obj has position:relative
            minChars: 0, // min char before suggest
            noCache: false,
            delimiter: null, // separate query
            maxHeight: 300,
            delayRequest: 0, // seconds to wait for query
            containerClass: 'inputSuggest',
            onSelect: null,
            width: 'auto',
            paramName: 'q',
            type: 'GET',
            dataType: 'text',
            ignoreParams: false,
            isAllowAddNew: false,
            preventBadQueries: true,
            triggerSelectOnValidInput: true,
            onAddNew: null,
            onSearchStart: noop,
            onSearchComplete: noop,
            onSearchError: noop,
            params: {}, // params to send together with query
            lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                return suggestion.value.toLowerCase().indexOf(queryLowerCase) !== -1;
            },
            transformResult: function (response) {
                return typeof response === 'string' ? $.parseJSON(response) : response;
            }
        };

        this.ele = ele;
        this._name = pluginName;
        this.suggestContainer = null;
        this.selectedIndex = -1;
        this.intervalId = 0;
        this.onChangeInterval = null;
        this.currentValue = null;
        this.isLocal = false;
        this.suggestions = [];
        this.badQueries = [];
        this.cachedResponse = {};
        this.options = $.extend({}, defaults, options);
        this.classes = {suggestion: 'suggestion', selected: 'selected'};
        this.selection = null;

        this.init();
        this.setOptions(this.options);
    }

    Plugin.prototype = {
        killerFn: null,
        init: function () {
            var self = this,
                container,
                sgItemClass = '.' + this.classes.suggestion;

            // prevent browser to suggest
            this.ele.setAttribute('autocomplete', 'off');

            this.killerFn = function (e) {
                if ($(e.target).closest('.' + this.options.containerClass).length === 0) {
                    this.killSuggestions();
                    this.disableKillerFn();
                }
            };

            // setup suggestion container
            this.suggestContainer = this.setupContainer();
            container = $(this.suggestContainer);

            // Only set width if it was provided:
            if (this.options.width !== 'auto') {
                container.width(this.options.width);
            }

            // container events
            container.on('mouseleave', sgItemClass, function (e) {
                self.selectedIndex = -1;
                container.children('.' + self.classes.selected).removeClass(self.classes.selected);
            });
            container.on('mouseover', sgItemClass, function (e) {
                self.activate($(this).data('index'));
            });
            container.on('click', sgItemClass, function (e) {
                self.select($(this).data('index'));
            });

            // element events
            $(this.ele).on('focus', function (e) {
                self.onFocus(e);
            });
            $(this.ele).on('blur', function (e) {
                self.onBlur(e);
            });
            $(this.ele).on('keyup', function (e) {
                self.onKeyUp(e);
            });
            $(this.ele).on('keydown', function (e) {
                self.onKeyPress(e);
            });

            this.fixPosition();
            this.fixPositionCapture = function () {
                if (self.visible) self.fixPosition();
            };
            $(window).on('resize', this.fixPositionCapture);

        },
        setOptions: function () {
            this.isLocal = $.isArray(this.options.lookupLocal);
            if (this.isLocal) this.options.lookupLocal = this.verifySuggestionsFormat(this.options.lookupLocal);
        },
        setupContainer: function () {
            var div = document.createElement('div');
            var offset = this.options.isRelative?$(this.ele).position():$(this.ele).offset();
            div.className = this.options.containerClass;
            div.style.position = 'absolute';
            div.style.display = 'none';
            $(div).css({'top': offset.top + $(this.ele).outerHeight() + 'px',
                'left': offset.left,
                'width': ($(this.ele).outerWidth() - 2) + 'px',
                'border': '1px solid #ddd',
//                'border-top': 'none',
                'background-color': '#FFF',
                'z-index': '999'});
            $(div).appendTo(this.options.appendTo);
            return div;
        },
        fixPosition: function () {
            var self = this, offset, styles;
            // Don't adjsut position if custom container has been specified:
            if (this.options.appendTo !== 'body') return;
            offset = $(this.ele).offset();
            styles = {
                top: (offset.top + $(this.ele).outerHeight() + 1) + 'px',
                left: offset.left + 'px'
            };
            if (this.options.width === 'auto') styles.width = ($(this.ele).outerWidth() - 2) + 'px';
            $(this.suggestContainer).css(styles);
        },

        onFocus: function (e) {
            this.fixPosition();
        },
        onBlur: function (e) {
            // this.hide();
        },
        onKeyUp: function (e) {
            switch (e.which) {
                case keys.UP:
                case keys.DOWN:
                    return;
            }
            clearInterval(this.onChangeInterval);
            if (this.currentValue !== $(this.ele).val()) {
                if (this.options.delayRequest > 0) {
                    var self = this;
                    this.onChangeInterval = setInterval(function () {
                        self.onValueChange();
                    }, self.options.delayRequest);
                } else {
                    this.onValueChange();
                }
            }
        },
        onKeyPress: function (e) {
            switch (e.which) {
                case keys.ESC:
                    $(this.ele).val(this.currentValue);
                    this.hide();
                    break;
                case keys.UP:
                    this.moveUp();
                    break;
                case keys.DOWN:
                    this.moveDown();
                    break;
                case keys.RETURN:
                    if (this.selectedIndex === -1) {
                        return;
                    }
                    this.select(this.selectedIndex);
                    break;
                case keys.RIGHT:
                    return;
                default:
                    return;
            }
            // Cancel event if function did not return:
            e.stopImmediatePropagation();
            e.preventDefault();
        },
        onValueChange: function () {

            var query = this.getQuery($(this.ele).val());

            clearInterval(this.onChangeInterval);
            this.currentValue = $(this.ele).val();
            this.selectedIndex = -1;

            // get suggestion
            if (query.length > this.options.minChars) this.getSuggestions(query);
            else this.hide();

        },
        getQuery: function (value) {
            var delimiter = this.options.delimiter, parts;
            if (!delimiter) return value;
            parts = value.split(delimiter);
            return $.trim(parts[parts.length - 1]);
        },
        getSuggestionLocal: function (query) {
            var data = { suggestions: null},
                limit = parseInt(this.options.lookupLimit, 10),
                self = this;
            data.suggestions = $.grep(this.options.lookupLocal, function (suggestions) {
                return self.options.lookupFilter(suggestions, query, query.toLowerCase());
            });
            if (limit && data.suggestions.length > limit) data.suggestions = data.suggestions.slice(0, limit);
            return data;
        },
        getSuggestions: function (q) {
            var response, params, cachekey,
                serviceUrl = this.options.serviceUrl,
                self = this;
            this.options.params[this.options.paramName] = q;
            params = this.options.ignoreParams ? null : this.options.params;
            if (this.isLocal) {
                response = this.getSuggestionLocal(q);
            } else {
                if ($.isFunction(serviceUrl)) serviceUrl = serviceUrl.call(this.ele, q);
                cacheKey = serviceUrl + '?' + $.param(params || {});
                response = this.cachedResponse[cacheKey];
            }
            if (response && $.isArray(response.suggestions)) {
                this.suggestions = response.suggestions;
                this.suggest(q);
            } else if (!this.isBadQuery(q)) {
                if (this.options.onSearchStart.call(this.ele, this.options.params) === false) return;
                if (this.currentRequest) this.currentRequest.abort();
                this.currentRequest = $.ajax({
                    url: serviceUrl,
                    data: params,
                    type: self.options.type,
                    dataType: self.options.dataType
                }).done(function (data) {
                    var result;
                    self.currentRequest = null;
                    result = self.options.transformResult(data);
                    self.processResponse(result, q, cacheKey);
                    self.options.onSearchComplete.call(self.ele, q, result.suggestions);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    self.options.onSearchError.call(self.ele, q, jqXHR, textStatus, errorThrown);
                });
            } else {
                // suggest add bad query
                this.suggest(q);
            }
        },
        processResponse: function (result, q, cacheKey) {
            result.suggestions = this.verifySuggestionsFormat(result.suggestions);
            // Cache results if cache is not disabled:
            if (!this.options.noCache) {
                this.cachedResponse[cacheKey] = result;
                if (this.options.preventBadQueries && result.suggestions.length === 0) {
                    this.badQueries.push(q);
                }
            }
            // Return if originalQuery is not matching current query:
            if (q !== this.getQuery(this.currentValue)) {
                return;
            }
            this.suggestions = result.suggestions;
            this.suggest(q);
        },
        suggest: function (q) {
            var html = '',
                container = $(this.suggestContainer),
                sgt = this.suggestions;

            $.each(sgt, function (i, sg) {
                if (sg.data === null) sgt.splice(i, 1);
            });

            if (this.options.isAllowAddNew && q.length > 2) sgt.push({'value': q, 'data': null});

            // if no suggestion
            if (sgt.length === 0) {
                this.hide();
                return;
            } else {
                // build suggest inner html
                var self = this;
                $.each(sgt, function (i, suggestion) {
                    if (self.options.isAllowAddNew && q.length > 2 && i === sgt.length - 1) {
                        if (!(sgt.length === 2 && q.toLowerCase() == sgt[0].value.toLowerCase())) {
                            html += '<div class="' + self.classes.suggestion + ' addNew" data-index="' + i + '" data-act="addNew">+add "' + self.formatResult(suggestion, self.getQuery(self.currentValue)) + '"</div>';
                        }
                    } else {
                        html += '<div class="' + self.classes.suggestion + '" data-index="' + i + '">' + self.formatResult(suggestion, self.getQuery(self.currentValue)) + '</div>';
                    }
                });
            }
            container.html(html);
            container.slideDown(100);
        },
        hide: function () {
            this.selectionIndex = -1;
            $(this.suggestContainer).slideUp(50);
        },
        activate: function (index) {
            var container = $(this.suggestContainer),
                selected = this.classes.selected;

            this.selectedIndex = index;
            container.children('.' + selected).removeClass(selected);
            if (this.selectedIndex !== -1 && container.children().length > this.selectedIndex) {
                var activeItem = container.children().get(this.selectedIndex);
                $(activeItem).addClass(selected);
                return activeItem;
            }
            return null;
        },
        // when arrow key up is pressed
        moveUp: function () {
            if (this.selectedIndex === -1) return;
            if (this.selectedIndex === 0) {
                $(this.suggestContainer).children().first().removeClass(this.classes.selected);
                this.selectedIndex = -1;
                $(this.ele).val(this.currentValue);
                return;
            }
            this.adjustScroll(this.selectedIndex - 1);
        },
        moveDown: function () {
            if (this.selectedIndex === this.suggestions.length - 1) return;
            this.adjustScroll(this.selectedIndex + 1);
        },
        select: function (index) {
            this.hide();
            this.onSelect(index);
        },
        onSelect: function (index) {
            var onSelectCallBack = this.options.onSelect,
                suggestion = this.suggestions[index];
            onAddNewCallBack = this.options.onAddNew,
                activeItem = $(this.suggestContainer).children().get(index);

            this.currentValue = this.getValue(suggestion.value);
            if (this.currentValue !== $(this.ele).val()) $(this.ele).val(this.currentValue);
            this.suggestions = [];
            this.selection = suggestion;
            if ($(activeItem).data('act') === 'addNew') {
                if ($.isFunction(onAddNewCallBack)) onAddNewCallBack.call(this.ele, suggestion);
            } else {
                if ($.isFunction(onSelectCallBack)) onSelectCallBack.call(this.ele, suggestion);
            }
        },
        adjustScroll: function (index) {
            var activeItem = this.activate(index);
            if (!activeItem) return;
            var offsetTop = activeItem.offsetTop,
                upperBound = $(this.suggestContainer).scrollTop(),
                lowerBound = upperBound + this.options.maxHeight - 25;
            if (offsetTop < upperBound) {
                $(this.suggestContainer).scrollTop(offsetTop);
            } else if (offsetTop > lowerBound) {
                $(this.suggestContainer).scrollTop(offsetTop - this.options.maxHeight + heightDelta);
            }
            $(this.ele).val(this.getValue(this.suggestions[index].value));
        },
        // verify suggestion format
        verifySuggestionsFormat: function (suggestions) {
            // If suggestions is string array, convert them to supported format:
            if (suggestions.length && typeof suggestions[0] === 'string') {
                return $.map(suggestions, function (value) {
                    return { value: value, data: null };
                });
            }
            return suggestions;
        },
        escapeRegExChars: function (value) {
            return value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        },
        formatResult: function (suggestion, currentValue) {
            var pattern = '(' + this.escapeRegExChars(currentValue) + ')';
            return suggestion.value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
        },
        getValue: function (value) {
            if (!this.options.delimiter) return value;
            var parts = this.currentValue.split(delimiter);
            if (parts.length === 1) return value;
            return this.currentValue.substr(0, this.currentValue.length - parts[parts.length - 1]) + value;
        },
        findSuggestionIndex: function (query) {
            var index = -1;
            $.each(this.suggestions, function (i, suggestion) {
                if (suggestion.value.toLowerCase() === query.toLowerCase()) {
                    index = i;
                    return false;
                }
            });
            return index;
        },
        isBadQuery: function (q) {
            if (!this.options.preventBadQueries) return false;
            var i = this.badQueries.length;
            while (i--) {
                if (q.indexOf(this.badQueries[i]) === 0) return true;
            }
            return false;
        },
        killSuggestions: function () {
            var self = this;
            this.stopKillSuggestions();
            this.intervalId = window.setInterval(function () {
                self.hide();
                self.stopKillSuggestions();
            }, 50);
        },
        stopKillSuggestions: function () {
            window.clearInterval(this.intervalId);
        },
        enableKillerFn: function () {
            $(document).on('click', this.killerFn);
        },
        disableKillerFn: function () {
            $(document).off('click', this.killerFn);
        }
    };
    $.fn[pluginName] = function (options, args) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);