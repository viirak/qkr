require.config({

    paths: {
        jquery: 'lib/jquery.min', // //cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js
        truncate: 'lib/jquery.truncate',
        underscore: 'lib/underscore-min', // //cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js
        text: 'lib/text.min', // //cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.js
        backbone: 'lib/backbone-min', // //cdnjs.cloudflare.com/ajax/libs/backbone.js/1.2.0/backbone-min.js
        stickit: 'lib/backbone.stickit.min', // //cdnjs.cloudflare.com/ajax/libs/backbone.stickit/0.8.0/backbone.stickit.min.js
        nprogress: 'lib/nprogress.min', // //cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js
        unveil: 'lib/jquery.unveil.min', // //cdnjs.cloudflare.com/ajax/libs/unveil/1.3.0/jquery.unveil.min.js
        moment: 'lib/moment.min', // //cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js
        popmodal: 'lib/popModal.min', // https://github.com/vadimsva/popModal
        layout_manager: 'lib/backbone.layoutmanager.min', // //cdnjs.cloudflare.com/ajax/libs/backbone.layoutmanager/0.9.7/backbone.layoutmanager.min.js
        perfect_scrollbar: 'lib/perfect-scrollbar.jquery.min', // https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.2/js/min/perfect-scrollbar.jquery.min.js
        dotimeout: 'lib/jquery.dotimeout.min',
        iframe_transport: 'lib/jquery.iframe-transport',
        input_suggest: 'lib/jquery.inputSuggest',
        velocity: 'lib/velocity.min',
        magnificpopup: 'lib/jquery.magnific-popup.min'
    },
    shim: {
        underscore: {
            exports: "_"
        },
        jquery:{
            exports: "$"
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'layout_manager': {
            deps: ['backbone']
        },
        'stickit':{
            deps: ['backbone']
        },
        'unveil': {
            deps: ['jquery'],
            exports: 'unveil'
        },
        'moment': {
            deps: ['jquery']
        },
        'popmodal': {
            deps: ['jquery']
        },
        'truncate': {
            deps: ['jquery']
        },
        'perfect_scrollbar': {
            deps: ['jquery']
        },
        'dotimeout': {
            deps: ['jquery']
        },
        'iframe_transport': {
            deps: ['jquery']
        },
        'input_suggest': {
            deps: ['jquery']
        },
        'velocity': {
            deps: ['jquery']
        },
        'magnificpopup': {
            deps: ['jquery']
        }
    }
});

requirejs(['bb/__init__', 'bb/data'], function (App, Data) {
    // window.quser = null;
    $.ajax({
        url: '/bb/auth/user',
        global: false,
        dataType: 'json',
        success: function(authuser) {
            // if (authuser) window.quser = authuser;
            user = authuser ? new Data.User(authuser): null;
            App.initialize(user);
        }
    });
});
