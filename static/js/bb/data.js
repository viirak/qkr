define(['backbone'], function(Backbone){
    "use strict";

    // User Model
    var User = Backbone.Model.extend({
     initialize: function(user){
       this.defaults = user;
     }
    });
    // User Collection
    var Users = Backbone.Collection.extend({
      model : User,
      url : '/bb/users',
      initialize: function(){
        this.state = { pageNum: 1, pageSize: 5, hasMore: false };
      },
      parse: function(data){
        this.state.hasMore = data.has_more;
        return data.users;
      }
    });

    // Activity
    var Activity = Backbone.Model.extend({
      initialize: function(){
        this.set('post', new Post(this.get('posts')[0]));
      }
    });
    var Activities = Backbone.Collection.extend({
      model: Activity,
      url: '/bb/activities',
      initialize: function(){
        this.state = {
          pageNum: 1,
          pageSize: 25,
          hasMore: false
        };
      },
      parse: function(data){
        this.state.hasMore = data.has_more;
        return data.activities;
      }
    });

    // Post Model
    var Post = Backbone.Model.extend({
      initialize: function(post){
        this.defaults = post;
      }
    });
    // Post Collection
    var Posts = Backbone.Collection.extend({
      model : Post,
      url : '/bb/posts',
      initialize: function(){
        this.state = { pageNum: 1, pageSize: 25, hasMore: false };
      },
      parse: function(data){
        this.state.meta = data.meta;
        return data.posts;
      }
    });

    // Comment Model
    var Comment = Backbone.Model.extend({});
    // Comment Collection
    var Comments = Backbone.Collection.extend({
      model: Comment,
      initialize: function(models, options){
        this.pid = options.pid;
        this.state = {
          offset: 0,
          pageNum: 1,
          pageSize: 3,
          nextPageSize: 10,
          length: this.length,
          sort: 'pop',
          isNext: true,
          hasMore: false,
          isPrepend: false
        };
      },
      url : function(){
        return '/bb/posts/'+this.pid+'/comments';
      },
      parse: function(data){
        this.state.hasMore = data.meta.has_more;
        this.state.total = data.meta.total;
        return data.comments;
      }
    });

    // Notification
    var Notification = Backbone.Model.extend({});
    var Notifications = Backbone.Collection.extend({
      model: Notification,
      url: '/bb/notifications',
      state: {},
      initialize: function(){
        this.state = {
          pageNum: 1,
          pageSize: 5,
          hasMore: false
        };
      },
      parse: function(data){
        this.state.hasMore = data.has_more;
        return data.notifications;
      }
    });

    // Topic
    var Topic = Backbone.Model.extend({
      initialize: function(topic){
        this.defaults = topic;
      }
    });
    var Topics = Backbone.Collection.extend({
      model: Topic,
      url: '/bb/topics',
      initialize: function(){
        this.state = {
          pageNum: 1,
          pageSize: 5,
          hasMore: false
        };
      },
      parse: function(data){
        this.state.hasMore = data.has_more;
        return data.topics;
      }
    });
    // Tag
    var Tag = Backbone.Model.extend({
      initialize: function(tag){
        this.defaults = tag;
      }
    });
    var Tags = Backbone.Collection.extend({
      model: Tag,
      url: '/bb/tags',
      initialize: function(){
        this.state = {
          pageNum: 1,
          pageSize: 5,
          hasMore: false
        };
      },
      parse: function(data){
        this.state.hasMore = data.has_more;
        return data.tags;
      }
    });

    // PostCollection
    var Collection = Backbone.Model.extend({});
    var Collections = Backbone.Collection.extend({
      model: Collection,
      state: {},
      initialize: function(options){
        this.url = '/bb/'+options.user.get('username')+'/collections'
        this.state = {
          pageNum: 1,
          pageSize: 25,
          hasMore: false
        };
      },
      parse: function(data){
        this.state.hasMore = data.has_more;
        return data.collections;
      }
    });

    return {
      User : User,
      Users : Users,
      Post : Post,
      Posts : Posts,
      Comment : Comment,
      Comments : Comments,
      Activity : Activity,
      Activities : Activities,
      Notification: Notification,
      Notifications: Notifications,
      Collection: Collection,
      Collections: Collections,
      Topic: Topic,
      Topics: Topics,
      Tag: Tag,
      Tags: Tags
    }
});
