define(['moment', 'nprogress','bb/router'], function (Moment, NProgress, Router) {
    "use strict";

    var initialize = function (user) {

        $(document)
            .on('ajaxSend', function () {
                $('#page').css({'opacity': '0.7'});
                NProgress.inc();
            })
            .on('ajaxComplete', function () {
                $('#page').css({'opacity': '1'});
                NProgress.done();
            });

//        $(document).ready(function(){
//            var spos = $(window).scrollTop();
//            $(window).scroll(function () {
//                var cpos = $(this).scrollTop(), tm = $("#topmenu");
//                if (cpos < spos) { //down
//                    if (!tm.hasClass('on')) tm.addClass('on').animate({top: 0}, 375);
//                } else if (cpos > spos) { //up
//                    if (tm.hasClass('on') && cpos > 0) tm.removeClass('on').animate({top: -50}, 375);
//                }
//                spos = cpos;
//            });

            //STICKY NAV
//            $('.stickyNav').each(function () {
//            var that = $('.stickyNav');
//            var top = that.offset().top - parseFloat(that.css('marginTop').replace(/auto/, 100));
//            $(window).scroll(function () {
//                // what the y position of the scroll is
//                var y = $(this).scrollTop();
//                console.log("y is ..." + y);
//                // whether that's below the form
//                if (y >= top) {
//                  // if so, ad the fixed class
//                  that.addClass('fixed');
//                } else {
//                  // otherwise remove it
//                  that.removeClass('fixed');
//                }
//            });
//            });
//        });

//        Moment.lang('en', {
//            relativeTime : {
//                future: "in %s",
//                past:   "%s ago",
//                s:  "seconds",
//                m:  "1m",
//                mm: "%dm",
//                h:  "1h",
//                hh: "%h",
//                d:  "1d",
//                dd: "%dd",
//                M:  "1m",
//                MM: "%dm",
//                y:  "1y",
//                yy: "%dy"
//            }
//        });

        Router.initialize(user); // init bb routes
    };

    return {
        initialize: initialize
    }
});
