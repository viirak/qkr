define([
  'backbone',
  'bb/utils',
  'bb/data',
  'moment',
  'text!bb/templates/post.html',
  'text!bb/templates/new_post.html',
  'text!bb/templates/form_topic_tag.html',
  'text!bb/templates/new_text_form.html',
  'text!bb/templates/settings.html',
  'text!bb/templates/profile.html',
  'text!bb/templates/comment.html',
  'text!bb/templates/comment_form.html',
  'text!bb/templates/topic.html',
  'text!bb/templates/user.html',
  'text!bb/templates/pop_notification.html',
  'text!bb/templates/pop_userinfo.html',
  'text!bb/templates/modal.html',
  'text!bb/templates/modal_share.html',
  'text!bb/templates/modal_report.html',
  'text!bb/templates/modal_editemail.html',
  'text!bb/templates/modal_editusername.html',
  'text!bb/templates/modal_changep8.html',
  'text!bb/templates/modal_editname.html',
  'text!bb/templates/yourpost_menu.html',
  'text!bb/templates/collection.html',
  'text!bb/templates/contentMenu.html',
  'layout_manager',
  'stickit',
  'unveil',
  'popmodal',
  'perfect_scrollbar',
  'dotimeout',
  'iframe_transport',
  'truncate',
  'input_suggest',
  'velocity',
  'magnificpopup'
], function(
  Backbone,
  Utils,
  Data,
  Moment,
  postHtml,
  newPostHtml,
  formTopicTagHtml,
  newTextFormHtml,
  settingsHtml,
  profileHtml,
  commentHtml,
  commentFormHtml,
  topicHtml,
  userHtml,
  popNotiHtml,
  popInfoHtml,
  modalHtml,
  shareModalHtml,
  reportHtml,
  editEmailHtml,
  editUsernameHtml,
  changeP8Html,
  editNameHtml,
  yourPostMenuHtml,
  collHtml,
  contentMenuHtml){
  "use strict";

  var _xsrf = Utils.getCookie('_xsrf');
  var _user = null, _layout = null;

  var initialize = function(options){
    _user = options.user;
    _layout = options.layout;
    new BodyView();
    new FlashBarView();
  }

  // Capitalize string
  String.prototype.capitalize = function(){
    return this.charAt(0).toUpperCase() + this.slice(1);
  };

  // add subviews to every backbone view
  var BaseView = Backbone.View.extend({
    _subViews : null,
    regSub: function(view){
      this._subViews = this._subViews || [];
      this._subViews.push(view);
    },
    remove: function(){
      _.invoke(this._subViews, 'remove');
      Backbone.View.prototype.remove.call(this);
    }
  });

  // Layout Manager
  var Layout = Backbone.Layout.extend({
    template: "#section-layout",
    className: 'layout',
    displayView: function(areaSel, View){
      var currentView = this.getView(areaSel);
      if(currentView) currentView.remove();
      this.setView(areaSel, View).render();
    },
    removeView: function(sel){
      var v = this.getView(sel);
      if(v) v.remove();
    }
  });

  var BodyView = BaseView.extend({
    manage: true,
    el: $('body'),
    initialize: function(options){
      var spos = $(window).scrollTop();
      $(window).scroll(function(){
        var cpos = $(this).scrollTop(),
            topbar = $("#topbar"),
            flashbar = $("#flashbar"),
            cm = $('#content-menu');

        // flashbar
        var bar_top = 0;
        if (cpos >= bar_top) flashbar.addClass('fixedtop');
        else flashbar.removeClass('fixedtop');

        // sticky content-menu
        var cm_top = 80;
        if (cpos >= cm_top) cm.addClass('fixed');
        else cm.removeClass('fixed');

        spos = cpos;
      });
    }
  });

  var FlashBarView = BaseView.extend({
    manage: true,
    el: '#flashbar',
    events: {
      'click .xbt': 'onClose'
    },
    onClose: function(e){
      e.preventDefault();
      $('#topbar').removeClass('hasflash');
      this.remove();
    }
  });

  // Object contains menus
  var Menu = {
    Top: BaseView.extend({
      manage: true,
      el: '#topbar',
      setActive: function(selector){
        this.$el.find('.active').removeClass('active');
        this.$el.find(selector).addClass('active');
      },
      events: {
        'click #uimglnk': 'onUImg',
        'click #unotiflnk': 'onUNotif',
        'click #upluslnk': 'onUPlus',
        'click .popmenu a': 'onPopmenuLink'
      },
      onUImg: function(e){
        e.preventDefault();
        $(e.currentTarget).popModal({
          placement : 'bottomLeft',
          html: $('#proIcoMenu').html(),
          showCloseBut : false
        });
      },
      onUNotif: function(e){
        e.preventDefault();
        var self = this, target = $(e.currentTarget), li = target.closest('li'), count = li.find('.count');
        self.notifDialog = new Notification.Dialog();
        var coll = self.notifDialog.streamView.collection;
        coll.fetch({ reset: true, success: function () {
          $(e.currentTarget).popModal({
            placement: 'bottomLeft',
            showCloseBut: false,
            html: function (cb) {
              cb(self.notifDialog.render().el);
            },
            onClose: function(){
              self.notifDialog.remove();
              target.removeClass('on');
            }
          });
          count.html('0').fadeOut('slow');
          target.addClass('on');
        }});
      },
      onPopmenuLink: function(){
        $('html').popModal("hide");
      },
      onUPlus: function(e){
        e.preventDefault();
        $(e.currentTarget).popModal({
          placement : 'bottomLeft',
          html: $('#plusMenu').html(),
          showCloseBut : false
        });
      }
    }),

    ContentMenu: BaseView.extend({
      manage: true,
      className: 'content-menu',
      template: _.template(contentMenuHtml),
    })
  };

  // View contains a link to load more of stream view
  var moreBtnView = BaseView.extend({
    manage: true,
    className: 'hasmore',
    uiClassNames: null,
    template: _.template('<a href="#" class="uiMore <%=classes%>" title="More">&#8226; &#8226; &#8226;</a>'),
    serialize: function(){
      return {'classes':this.uiClassNames}
    }
  });

  var PopInfoView = BaseView.extend({
    manage: true,
    template: _.template(popInfoHtml),
    serialize: function(){
      return {'user': this.model.toJSON(), '_user': _user.toJSON()}
    },
    events: {
      'click .btnFollow': 'onFollow'
    },
    onFollow: function(e){
      e.preventDefault();
      var self = this;
      if(!self.model.get('is_follow')) {
        $.ajax({
          url: '/bb/users/'+self.model.get('username')+'/follow',
          data: {_xsrf: _xsrf, follow: true},
          method: 'PUT',
          dataType: 'json',
          success: function(){
            self.model.set('is_follow', true);
            $(e.currentTarget).addClass('uiDisabled').html('Following');
          },
          error: function(){
            alert('Error following user.');
          }
        });
      }
    }
  });

  var User = {
    // Model View
    ModelView : BaseView.extend({
      manage: true,
      tagName: 'li',
      className: 'item user',
      template: _.template(userHtml),
      initialize: function(options){
        this.model = options.model;
      },
      serialize: function(){
        return {user: this.model.toJSON()}
      },
      renderTemplate: function(){
        this.$el.html(_.template(userHtml)(this.serialize()));
        this.stickit();
      },
      bindings: {
        '.uiFollowerCount': { observe: 'follower_count' }
      },
      events: {
        'click .btnFollow': 'onFollow'
      },
      onFollow: function(e){
        e.preventDefault();
        var self = this, target = $(e.currentTarget), follow = target.hasClass('follow');
        if(follow !== self.model.get('is_follow')){
          $.ajax({
            url: '/bb/users/'+self.model.get('username')+'/follow',
            data: {_xsrf: _xsrf, follow: follow},
            method: 'PUT',
            success: function(){
              if(follow){
                self.model.set('follower_count', self.model.get('follower_count')+1);
                target.html('Following').addClass('uiDisabled').removeClass('follow');
              }
            },
            error: function(){
              alert('Error following user.');
            }
          });
        }
      }
    }),

    // Collection View
    CollectionView : BaseView.extend({
      manage: true,
      tagName: 'ul',
      isSub: false,
      className: 'list users',
      initialize: function (options) {
        this.collection = options.collection;
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'add', this.addItemView);
      },
      beforeRender: function () {
        this.collection.each(function (model) {
          this.addItemView(model, false);
        }, this);
      },
      addItemView: function (model, render) {
        var view = this.insertView(new User.ModelView({model: model}));
        this.regSub(view);
        if (render !== false) view.render();
      }
    }),

    // Stream View
    StreamView : BaseView.extend({
      manage: true,
      className: 'userStream stream',
      params: {},
      initialize: function(options){
        var self = this;
        if (!this.users){
          this.users = new Data.Users([]);
        }
        if(options.params) this.params = options.params;
        this.users.fetch({ reset: true, data:this.params, success: function(){
          if (self.users.state.hasMore) {
              self.$el.append('<a class="uiMore hasMore" href="#" title="More">&#8226; &#8226; &#8226;</a>');
          }
        }});
      },
      beforeRender: function(){
        this.regSub(this.insertView(new User.CollectionView({collection: this.users})));
      },
      events: {
        'click .hasMore': 'fetchMore'
      },
      fetchMore: function(e){
        e.preventDefault();
        var self = this;
        this.users.state.pageNum += 1;
        this.params['page_num'] = this.users.state.pageNum;
        this.users.fetch({
          add: true,
          data: this.params,
          success: function(){
            if (!self.users.state.hasMore) {
              self.$el.find('.hasMore').remove();
            }
          }
        });
      }
    })
  };

  var Comment = {
    // Model View
    ModelView : BaseView.extend({
      manage : true,
      tagName : 'li',
      className : 'item comment',
      template : _.template(commentHtml),
      post: null,
      initialize: function(options){
        this.model = options.model;
        this.post = options.post;
        this.isSub = options.isSub;
      },
      serialize : function(){
        return {
          comment: this.model.toJSON(),
          post: this.post.toJSON(),
          isSub: this.isSub,
          _user: _user.toJSON(),
          mm: Moment,
        };
      },
      events: function(){
        var ce, cre;
        ce = {
          'click .commentLike': 'onLike',
          'click .uiCommentReply' : 'onReply',
          'click .commentReport': 'onReport',
          'click .commentHide': 'onHide',
          'click .uiUndoCommentHide': 'onUndoHide',
          'click .commentDelete': 'onDelete',
          'click .commentEdit': 'onEdit',
          'click .uiCommentOptions': 'onCommentOptions',
          'keydown .uiCommentTextEdit': 'onEditKeydown',
          'mouseover a.commentAuthorLnk': 'onAuthorHover'
        };
        cre = {
          'click .replyLike': 'onLike',
          'click .replyReport': 'onReport',
          'click .replyHide': 'onHide',
          'click .uiUndoReplyHide': 'onUndoHide',
          'click .replyDelete': 'onDelete',
          'click .replyEdit': 'onEdit',
          'keydown .uiReplyTextEdit': 'onEditKeydown',
          'mouseover a.replyAuthorLnk': 'onAuthorHover'
        };
        return this.model.get('parent') === null?ce:cre;
      },
      renderTemplate: function(){
        this.$el.html(this.template(this.serialize()));
        this.stickit();
      },
      beforeRender: function(){
        if (this.model.get('children')){
          if (!this.replyStream){
            this.replyStream = new Comment.ReplyStreamView({model:this.model, post:this.post});
          }
          this.regSub(this.insertView(this.replyStream));
        }
      },
      bindings: {
        '.uiCommentLikeCount': {
          observe: 'like_count',
          updateMethod: 'html',
          onGet: 'formatVal'
        },
        '.uiCommentReplyCount': {
          observe: 'child_count',
          updateMethod: 'html',
          onGet: 'formatVal'
        },
        '.commentText': {
          observe: 'text'
        }
      },
      formatVal: function(val){
        return val === 0 ? '' : ' &#8226; ' + val;
      },
      onReply: function(e){
        e.preventDefault();
        if(this.model.get('child_count') === 0 && !this.replyStream){
          var self = this;
          this.replyStream = this.insertView(new Comment.ReplyStreamView({model:this.model, post:this.post}));
          this.replyStream.regSub(this.replyStream.insertView(new Comment.Form({model: this.model, collection: this.replyStream.collection, parent:this.model.get('id')})));
          this.regSub(this.insertView(this.replyStream));
          this.render().then(function(){
            self.$el.find('.uiCommentTextarea').focus();
          });
        }else{
          this.$el.find('.uiCommentTextarea')[0].focus();
        }
      },
      onLike: function(e){
        e.preventDefault();
        var self = this,
          ico = $(e.currentTarget).find('.ico'),
          text = $(e.currentTarget).find('.text'),
          like = ico.hasClass('off'),
          like_count = like ? this.model.get('like_count') + 1 : this.model.get('like_count') - 1;
        if(self.model.get('is_liked') !== like){ //incase s.o modifies ui
          $.post('/bb/posts/'+self.post.get('id')+'/comments/'+this.model.get('id')+'/like', {'_xsrf':_xsrf, 'like': like})
          .done(function(){
            self.model.set('like_count', like_count);
            self.model.set('is_liked', like);
            if(like){
              ico.removeClass('off').addClass('on');
              text.html('Liked');
            }else{
              ico.removeClass('on').addClass('off');
              text.html('Like');
            }
          });
        }
      },
      onReport: function(e){
        e.preventDefault();
        var self = this;
        $(_.template(modalHtml)({title: 'Report', okButText: 'Submit', isCancelBut:true})).dialogModal({
          onLoad: function(el){
            $.get('/bb/reports/categories', {'type':'comment'}).done(function(data){
              var html = _.template('<ul id="reportcats"><% for (var i = 0; i < categories.length; i++) { %><li><label><input class="uiReportOpt" <% if(i === 0) { %> checked="checked" <% } %> type="radio" name="category" value="<%=categories[i].id%>"> <span class="uiLabelText"><%=categories[i].title%></span></label></li><% } %></ul>')({'categories':data.categories});
              el.find('.content').html(html);
            });
          },
          onOkBut: function(){
            var catId = $('body').find('.uiReportOpt:checked').val();
            if(self.model.get('is_report') !== true){
              $.post('/bb/posts/'+self.post.get('id')+'/comments/'+self.model.get('id')+'/report',{
                '_xsrf':_xsrf,
                'opt': catId
              }).done( function(){
                $(e.currentTarget).addClass('disabled');
                self.model.set('is_report', true);
                var notifyHtml = '<p><strong>Thank you!</strong><br />Your report has been submitted.</p>';
                $(notifyHtml).notifyModal({
                  duration : 2500,
                  placement : 'center',
                  overlay : true,
                  // type : 'dark'
                });
              });
            }
          }
        });
      },
      onHide: function(e){
        e.preventDefault();
        var self = this;
        $.post('/bb/posts/'+self.post.get('id')+'/comments/'+self.model.get('id')+'/hide', {
          '_xsrf':_xsrf,
          'hide':true
        }).done(function(){
          var str = self.model.get('parent')?'Reply':'Comment';
          self.$el.wrapInner('<div class="hiddenComment"></div>');
          self.$el.find('.hiddenComment').slideUp('fast',function(){
            var nel = '<div class="hiddenNotice">You will no longer see this comment. &mdash; <a href="#" class="uiUndo'+str+'Hide" title="This comment has been hidden.">Undo</a></div>';
            self.$el.append($(nel).fadeIn());
          });
        });
      },
      onUndoHide: function(e){
        e.preventDefault();
        var self = this;
        $.post('/bb/posts/'+self.post.get('id')+'/comments/'+self.model.get('id')+'/hide', {
          '_xsrf':_xsrf,
          'hide':false
        }).done(function(){
          self.render();
        });
      },
      onDelete: function(e){
        e.preventDefault();
        var self = this;
        $(_.template(modalHtml)({title: 'Confirmation', okButText: 'Delete', isCancelBut:true})).dialogModal({
          onLoad: function(el){
            el.find('.content').html('<p>Are you sure you want to delete?</p>');
          },
          onOkBut: function(){
            $.ajax({
              url: '/bb/posts/'+self.post.get('id')+'/comments/'+self.model.get('id'),
              method: 'DELETE',
              data: {'_xsrf':_xsrf},
              success: function(){
                self.$el.fadeOut('slow');
                self.post.set('comment_count', self.post.get('comment_count') - 1);
              }
            });
          }
        });
      },
      onEdit: function(e){
        var target = $(e.currentTarget), self = this;
        target.fadeOut('fast',function(){
          var str = self.model.get('parent') === 0?'Comment':'Reply', ac = '.'+str.toLowerCase()+'Actions';
          target.parent().append('<div class="editForm"><textarea class="uiTextInp uiTextarea editArea ui'+str+'TextEdit">'+target.text()+'</textarea><div class="hint"><strong>Enter</strong> to submit. <strong>Esc</strong> to quit</div></div>');
          self.$el.find('.editArea').autogrow().focus();
          self.$el.find(ac).remove();
          target.hide();
        });
      },
      onEditKeydown: function(e){
        var self = this, target = $(e.currentTarget),
          val = $.trim(target.val());
        if(e.keyCode === 13 && !e.shiftKey){ // Enter key
          e.preventDefault();
          if(val && val !== self.model.get('html')){
            // put to server
            $.ajax({
              url: '/bb/posts/'+self.post.get('id')+'/comments/'+self.model.get('id'),
              method: 'PUT',
              data: {'_xsrf':_xsrf, html: val},
              dataType: 'json', // expect json back
              success: function(){
                self.model.set('html', val);
                self.model.set('modified_at', Moment.format());
                _.invoke(self._subViews, 'remove');
                self.render();
              }
            });
          }else{
            target.shake(2, 5, 180); // shake
          }
        }else if(e.keyCode === 27){ //escape key
          _.invoke(this._subViews, 'remove');
          this.render();
        }
      },
      onAuthorHover: function(e){
        e.preventDefault();
        var self = this, target = $(e.currentTarget), popView;
        target.popModal({
          html : function(cb){
            popView = new PopInfoView({model: new Backbone.Model(self.model.get('owner'))});
            cb(popView.render().el);
          },
          placement : 'bottomCenter',
          showCloseBut : false,
          onClose: function(){
            popView.remove();
          }
        });
      },
      onCommentOptions: function(e){
        e.preventDefault();
        var target = $(e.currentTarget);
        target.popModal({
          placement : 'bottomCenter',
          html: target.parent().find('.commentOptions').html(),
          showCloseBut : false
        });
      }
    }),

    // Collection View
    CollectionView : BaseView.extend({
      manage: true,
      tagName: 'ul',
      isSub: false,
      className: function(){
        if(this.isSub){
          return 'sublist subcomments';
        }else{
          return 'list comments';
        }
      },
      subViews: [],
      initialize: function(options){
        this.post = options.post;
        this.collection = options.collection;
        this.isSub = options.isSub;
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'add', this.addItemView);
      },
      beforeRender : function(){
        this.collection.each(function (model) {
          this.addItemView(model, false);
        }, this);
      },
      addItemView : function(model, render){
        var view = this.insertView(new Comment.ModelView({
          model: model,
          post: this.post,
          isSub: this.isSub
        }));
        this.regSub(view);
        if (render !== false) view.render();
      },
      // overwrite
      insert: function($root, $el){
        if (this.collection.state.isPrepend) {
          $root.prepend(Utils.fadeNew($el));
        }else{
          $root.append(Utils.fadeNew($el));
        }
      }
    }),

    // Stream View
    StreamView : BaseView.extend({
      manage: true,
      className: 'comment-stream',
      events: {
        'click .hasmoreComments': 'fetchMore'
      },
      initialize: function (options) {
        this.post = options.post;
        if (!this.collection) {
          this.collection = new Data.Comments(this.post.get('comments'), {pid: this.post.get('id')});
        }
      },
      beforeRender : function(){
        this.regSub(this.insertView(new Comment.CollectionView({
          collection: this.collection,
          post: this.post,
          isSub: false})));
        if (this.post.get('comment_count') > 3) {
          this.regSub(this.insertView(new moreBtnView({uiClassNames: 'hasmoreComments'})));
        }
      },
      fetchMore: function(e){
        e.preventDefault();
        var self = this;
        var isNext = this.collection.state.isNext;
        var pageSize = this.collection.state.pageSize;
        var offset = this.collection.state.offset;
        var nextPageSize = this.collection.state.nextPageSize;
        if(!isNext && nextPageSize > 0) pageSize = nextPageSize; //previous more comments

        if(!isNext && offset < pageSize){
          pageSize = offset;
          offset = 0;
        }

        if(offset > 0) offset = isNext ? offset + pageSize : offset - pageSize;
        if(offset == 0 && isNext) offset = offset + pageSize;
        if(isNext && nextPageSize > 0) pageSize = nextPageSize; //next more comments

        this.collection.state.offset = offset;
        this.collection.state.pageSize = pageSize;
        this.collection.state.isPrepend = false;

        this.collection.fetch({
          add: true,
          data: {
            page_size: pageSize,
            offset: offset
          },
          success: function(){
            if (!self.collection.state.hasMore) {
              var v = self.getView(function(view){
                if(view.className === 'hasmore') return view;
              });
              if (v) v.remove();
            }
          }
        });
      }
    }),

    ReplyStreamView : BaseView.extend({
      manage: true,
      className: 'replies',
      tagName: 'div',
      model: null,
      post: null,
      initialize: function () {
        if (!this.collection){
          var children = this.model.get('children');
          this.collection = new Data.Comments(typeof(children)!='undefined'?children:[], {pid: this.post.get('id')});
        }
        this.collection.state.parent = this.model.get('id');
        this.collection.state.pageSize = this.model.get('child_count') > 0?this.model.get('children').length:0;
        this.collection.state.nextPageSize = 5;
        this.collection.state.offset = 1;
        this.collection.state.sort = 'pop';
      },
      events: {
        'click .hasmoreReplies': 'loadMore'
      },
      beforeRender : function(){
        this.regSub(this.insertView(new Comment.CollectionView({collection:this.collection, post:this.post, isSub:true})));
        if(this.model.get('child_count') > this.collection.length){
          this.regSub(this.insertView(new moreBtnView({uiClassNames:'hasmoreReplies'})));
        }
        this.regSub(this.insertView(new Comment.Form({post: this.post, model: this.model, collection: this.collection, parent: this.collection.state.parent})));
      },
      loadMore: function(e){
        e.preventDefault();

        var pageSize = this.collection.state.pageSize;
        var offset = this.collection.state.offset;
        var nextPageSize = this.collection.state.nextPageSize;
        if(offset > 0) offset = offset + pageSize;
        if(nextPageSize > 0) pageSize = nextPageSize; //next more comments

        this.collection.state.offset = offset;
        this.collection.state.isPrepend = false;

        var self = this,
          data = {
            parent: this.collection.state.parent,
            page_size: pageSize,
            offset: offset,
          };
        if(this.collection.state.sort !== 'pop') data['sort'] = this.collection.state.sort;
        this.collection.fetch({
          add: true,
          data: data,
          success: function(){
            if (!self.collection.state.hasMore) {
              var v = self.getView(function(view){
                if(view.className === 'hasmore') return view;
              });
              if (v) v.remove();
            }
          }
        });
      }
    }),

    Form : BaseView.extend({
      manage : true,
      tagName : 'div',
      className : 'commentForm',
      collection: null,
      model: null,
      parent: null,
      post: null,
      initialize : function(){
        this.template = _.template(commentFormHtml)({'_user': _user.toJSON()});
        this.collection.post = this.post;
      },
      afterRender: function(){
        this.$el.find('.uiCommentTextarea').autogrow();
      },
      events: {
        'keypress textarea': 'newComment'
      },
      newComment: function(e){
        var target = $(e.currentTarget);
        if(e.keyCode === 13 && !e.shiftKey) { // Enter key
          e.preventDefault();
          if ($.trim(target.val())) {
            var self = this,
              data = {'_xsrf':_xsrf, 'text':target.val()};
            if (self.parent) data['parent'] = self.parent;
            $.post(this.collection.url(), data,'json').done( function(data){
              if(!self.parent) self.collection.state.isPrepend = true;
              self.collection.add(data.comment);
              var str = !self.parent ? 'comment_count':'child_count';
              self.model.set(str, self.model.get(str)+1);
              target.val("").css({'height':'18px'});
            });
          }else{
            target.shake(2, 5, 180); // shake
          }
        }
      }
    })
  };

  var Post = {
    //Model View
    ModelView: BaseView.extend({
      manage: true,
      className: 'postView post',
      tagName: 'li',
      isSolo: false,
      template: _.template(postHtml),
      initialize: function(options){
        this.model = options.model;
        this.aa = options.aa?options.aa:null;
      },
      serialize: function(){
        return {
          'post': this.model.toJSON(),
          'aa':this.aa,
          'Moment':Moment,
          '_user':_user,
          'isSolo':this.isSolo
        };
      },
      beforeRender: function(){
        if (this.model.get('comments') && this.model.get('comments').length > 0) {
          var cmtStream = new Comment.StreamView({post: this.model});
          var cmtForm = cmtStream.insertView(new Comment.Form({model: this.model, collection: cmtStream.collection}));
          cmtStream.regSub(cmtForm);
          this.regSub(this.insertView(cmtStream));
        }
      },
      renderTemplate: function(){
        this.$el.html(this.template(this.serialize()));
        this.stickit();
      },
      afterRender: function(){
        this.$el.find('img').unveil();
        $('body').titleModal();
        if(this.model.get('photos') && this.model.get('photos').length > 1) {
          $('.pic'+this.model.get('id')).magnificPopup({
            delegate: 'a.uiPostPhoto', // the selector for gallery item
            type: 'image',
//                        mainClass: 'mfp-with-fade',
            showCloseBtn: false,
            removalDelay: 500,
            gallery: {
                enabled: true
            },
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 150 // don't foget to change the duration also in CSS
            }
          });
        }
      },
      bindings: {
        '.uiPostLikeCount': {
          observe: 'like_count',
          updateMethod: 'html',
          onGet: 'formatVal'
        },
        'a.uiPostLike': {
          observe: 'is_liked',
          updateMethod: 'html',
          onGet: 'formatPostLike'
        },
        '.uiPostCommentCount': {
          observe: 'comment_count',
          updateMethod: 'html',
          onGet: 'formatVal'
        },
        'a.uiPostComment': {
          observe: 'comment_count',
          updateMethod: 'html',
          onGet: 'formatPostComment'
        },
        '.uiPostShare': {
          observe: 'is_shared',
          updateMethod: 'html',
          onGet: 'formatPostShare'
        },
        '.uiPostShareCount': {
          observe: 'share_count',
          updateMethod: 'html',
          onGet: 'formatVal'
        },
        'a.uiPostSave': {
          observe: 'is_saved',
          updateMethod: 'html',
          onGet: 'formatPostSave'
        },
        '.uiPostSaveCount':{
          observe: 'save_count',
          updateMethod: 'html',
          onGet: 'formatVal'
        },
        'a.uiPostSubscribe': {
          observe: 'is_subscribed',
          updateMethod: 'html',
          onGet: 'formatPostSubscribe'
        },
      },
      formatVal: function(val){
        var html = ' &#8226; ' + val;
        return val === 0 ? '' : html;
      },
      formatPostLike: function(val){
        var html = '<i class="ico';
        html += val === true ? ' on' : ' off';
        html += '"></i><span class="text">';
        html += val === true ? 'Liked' : 'Like';
        html += '</span>';
        return html;
      },
      formatPostComment: function(val){
        var html = '<i class="ico';
        html += val > 0 ? ' on' : ' off';
        html += '"></i><span class="text">Comment';
        html += '</span>';
        return html;
      },
      formatPostShare: function(val){
        var html = '<i class="ico';
        html += val === true ? ' on' : ' off';
        html += '"></i><span class="text">';
        html += val === true ? 'Shared' : 'Share';
        html += '</span>';
        return html;
      },
      formatPostSave: function(val){
        var html = '<i class="ico';
        html += val === true ? ' on' : ' off';
        html += '"></i><span class="text">';
        html += val === true ? 'Saved' : 'Save';
        html += '</span>';
        return html;
      },
      formatPostSubscribe: function(val){
        var html = '<i class="ico';
        html += val === true ? ' on' : ' off';
        html += '"></i><span class="text">';
        html += val === true ? 'Unsubscribed' : 'Subscribe';
        html += '</span>';
        return html;
      },
      events: {
        'click a.uiPostComment': 'onComment',
        'click a.uiPostLike': 'onLike',
        'click a.uiPostSave': 'onSave',
        'click a.uiPostShare': 'onShare',
        'click a.uiPostReport': 'onReport',
        'click a.uiPostHide': 'onHide',
        'click a.uiUndoHide': 'onUndoHide',
        'click a.uiPostDelete': 'onDelete',
        'click a.uiPostOptions': 'onPostOptions',
        'click a.uiShareToFollowers': 'onShareToFollowers',
        'click a.uiPostSubscribe': 'onPostSubscribe',
        'click a.permalink': 'onPermalink',
//        'click a.uiPostPhoto': 'onPostPhotoClicked',
        'mouseover a.actProfileLink': 'onProfileLinkHover',
      },
      onComment: function(e){
        e.preventDefault();
        if(this.model.get('comment_count') <= 0){
          var self = this;
          var cmtStream = new Comment.StreamView({post: this.model});
          var cmtForm = cmtStream.insertView(new Comment.Form({model: this.model, collection: cmtStream.collection}));
          cmtStream.regSub(cmtForm);
          this.regSub(this.insertView(cmtStream));
          this.render().then(function(){
            self.$el.find('.uiCommentTextarea').focus();
          });
        }else{
          this.$el.find('.uiCommentTextarea')[0].focus();
        }
      },
      onLike: function(e){
        e.preventDefault();
        var self=this,
          like = this.model.get('is_liked')?false:true;
        $.post('/bb/posts/' + self.model.get('id') + '/like',
        {'_xsrf': _xsrf, 'like': like}, 'json').done(function () {
          var like_count = like? self.model.get('like_count')+1: self.model.get('like_count')-1;
          self.model.set('like_count', like_count);
          self.model.set('is_liked', like);
        });
      },
      onSave: function(e){
        e.preventDefault();
        var self = this, is_saved = this.model.get('is_saved');
        if (is_saved){
          $.post('/bb/posts/'+this.model.get('id')+'/save', {
            _xsrf:_xsrf,
            save:false
          }).done(function(data){
            if(data.status == 'Ok'){
              self.model.set('is_saved', false);
              self.model.set('save_count', self.model.get('save_count')-1);
            }
          }).fail(function(){
            alert('Server Error.');
          });
        }else{
          this.collectionDialog = new Collection.Dialog({user: _user, post: this.model});
          var coll = self.collectionDialog.streamView.collection;
          coll.fetch({ reset: true, data: {}, success: function () {
            $(e.currentTarget).popModal({
              id: 'sth',
              placement: 'bottomCenter',
              showCloseBut: false,
              html: function (cb) {
                cb(self.collectionDialog.render().el);
              },
              onClose: function(){
                self.collectionDialog.remove();
              }
            });
          }});
        }
      },

      onShare: function(e){
        e.preventDefault();
        var target = $(e.currentTarget);
        var target = $(e.currentTarget);
        target.popModal({
          placement : 'bottomCenter',
          html: target.parent().find('.shareOptions').html(),
          showCloseBut : false
        });
      },
      onShareToFollowers: function(e){
        e.preventDefault();
        var p = this.model, parent=this.model.get('parent'),
          x = parent ? parent : p.toJSON();
        $(_.template(shareModalHtml)({post: x, via: p.get('owner')})).dialogModal({
          onLoad: function(){
            $('body').find('#uiShareComment').autogrow().focus();
          },
          onOkBut: function(){
            var comment = $('#uiShareComment').val(),
              data = {'_xsrf':_xsrf};
            if (comment !== '') data['comment'] = comment;
            // if(p.get('is_shared') !== true) {
            // todo: need to find way to prevent spam share (non stop)
              $.post('/bb/posts/' + p.get('id') + '/share', data)
              .done(function (data) {
                if(data.status === 'Ok'){
                  p.set('share_count', p.get('share_count') + 1);
                  p.set('is_shared', true);
                  // $(e.currentTarget).closest('.footer').find('.uiPostShare').addClass('disabled');
                  var notifyHtml = '<p>Post has been shared to your followers.</p>';
                  $(notifyHtml).notifyModal({
                    duration: 2500,
                    placement: 'center',
                    overlay: true,
                    // type: 'dark'
                  });
                }
              }).fail(function(){ alert("Server Error."); });
            // }
          }
        });
      },
      onPostOptions: function(e){
        e.preventDefault();
        var target = $(e.currentTarget);
        target.popModal({
          placement : 'bottomCenter',
          html: target.parent().find('.postOptions').html(),
          showCloseBut : false
        });
      },
      onReport: function(e){
        e.preventDefault();
        var self = this, p = this.model;
        $(_.template(modalHtml)({ title: 'Report', okButText: 'Submit', isCancelBut:true})).dialogModal({
          onLoad: function(el){
            $.get('/bb/reports/categories', {'type':'post'}).done(function(data){
              var c = '<ul id="reportcats">' +
                '<% for (var i = 0; i < categories.length; i++){ %>'+
                '<li><label>'+
                '<input class="uiReportOpt" <% if(i === 0) { %> checked="checked" <% } %> type="radio" name="category" value="<%=categories[i].id%>"> '+
                '<span class="uiLabelText"><%=categories[i].title%></span>'+
                '</label></li>'+
                '<% } %></ul>';
              var html = _.template(c)({'categories':data.categories});
              el.find('.content').html(html);
            });
          },
          onOkBut: function(){
            var opt = $('body').find('.uiReportOpt:checked').val();
            if(p.get('is_report') !== true) {
              $.post('/bb/posts/' + self.model.get('id') + '/report', {
                '_xsrf': _xsrf,
                'opt': opt
              }).done(function () {
                $(e.currentTarget).addClass('disabled');
                p.set('is_report', true);
                $(e.currentTarget).find('.ico').removeClass('off').addClass('on');
                $(e.currentTarget).find('.text').html('Reported');
                var notifyHtml = '<p><strong>Thank you!</strong><br />Your report has been submitted.</p>';
                $(notifyHtml).notifyModal({
                  duration: 2500,
                  placement: 'center',
                  overlay: true,
                  // type: 'dark'
                });
              });
            }
          }
        });
      },
      onHide: function(e){
        e.preventDefault();
        var self = this;
        $.post('/bb/posts/'+this.model.get('id')+'/hide', {'_xsrf':_xsrf, 'hide':true}).done(function(){
          self.$el.wrapInner('<div class="hiddenComment"></div>');
          self.$el.find('.hiddenComment').slideUp(function(){
            var nel = '<div class="hiddenNotice">You will no longer see this post. &mdash; <a href="#" class="uiUndoHide" title="This comment has been hidden.">Undo</a></div>';
            self.$el.append($(nel).fadeIn());
          });
        });
      },
      onUndoHide: function(e){
        e.preventDefault();
        var self = this;
        $.post('/bb/posts/'+this.model.get('id')+'/hide', {'_xsrf':_xsrf, 'hide':false}).done(function(){
          self.render();
        });
      },
      onDelete: function(e){
        e.preventDefault();
        var self = this;
        $(_.template(modalHtml)({title: 'Confirmation', okButText: 'Delete', isCancelBut:true})).dialogModal({
          onLoad: function(el){
            el.find('.content').html('<span>Are you sure you want to delete?</span>');
          },
          onOkBut: function(){
            $.ajax({
              url: '/bb/posts/'+self.model.get('id'),
              method: 'DELETE',
              data: {'_xsrf':_xsrf},
              success: function(){
                self.$el.fadeOut('slow');
              }
            });
          }
        });
      },
      onPostSubscribe: function(e){
        e.preventDefault();
        var self = this,
          subscribe=self.model.get('is_subscribed')?false:true;
        $.post('/bb/posts/'+this.model.get('id')+'/subscribe',
          {'_xsrf':_xsrf, 'subscribe':subscribe}).done(function(data){
            if(data.status === 'Ok'){
              self.model.set('is_subscribed', subscribe);
              var notifyHtml = subscribe?'<p>Your are subscribed.</p>': '<p>Your are unsubscribed.</p>';
              $(notifyHtml).notifyModal({
                duration: 2500,
                placement: 'center',
                overlay: true,
                // type: 'dark'
              });
              $('html').popModal("hide"); // close all popModal
            }
        });
      },
      onProfileLinkHover: function(e){
        e.preventDefault();
        var popView, popUser=null, target = $(e.currentTarget), ref = target.data("ref");
        if(ref === "actor"){
          _.each(this.aa['actors'], function(actor){
            if(actor.username === target.data('username')) popUser=actor;
          });
        }else if(ref === "post-owner"){
          popUser = this.model.get('owner');
        }else if(ref === "parent-post-owner"){
          popUser = this.model.get('parent').author;
        }else if(ref === "inpost-actor"){
          _.each(this.aa['share_in_posts'], function(share){
            if (share.in_post.author.username === target.data('username')) popUser=share.in_post.author;
          });
        }
        if(popUser){
          target.popModal({
            html : function(cb){
              popView = new PopInfoView({model: new Backbone.Model(popUser)});
              cb(popView.render().el);
            },
            placement : 'bottomCenter',
            showCloseBut : false,
            onClose: function(){
              popView.remove();
            }
          });
        }
      },
      onPostPhotoClicked: function(e){
        e.preventDefault();
        var curEl = $(e.currentTarget), href = $(e.currentTarget).attr('href');
        console.log(this.galleryItems);
        if(this.galleryItems.length > 0){
          $.magnificPopup.open({
            items: this.galleryItems,
            type: 'image',
            gallery: {
              enabled: true
            },
            removalDelay: 500, //delay removal by X to allow out-animation
            mainClass: 'mfp-fade',
            callbacks: {
              beforeOpen: function() {
                // just a hack that adds mfp-anim class to markup
               this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
               this.st.mainClass = curEl.attr('data-effect');
              }
            },
            closeOnContentClick: true
          });
        }
      },
      onPermalink: function(e){
        e.preventDefault();
        var self = this, p = this.model, pm;
        if (p.get('parent')) pm = new Data.Post(p.get('parent'));
        else pm = p;
        var soloPost = new Post.ModelView({'model':pm, 'className':'postSoloView', 'tagName': 'div', 'id': 'postSolo', 'isSolo':true});
        $(_.template(modalHtml)({ title: 'Post', okButText: 'Close', isCancelBut:false})).dialogModal({
          onLoad: function(el){
              soloPost.render().then(function(){
                  el.find('.content').html(this.el);
              });
          },
          onClose: function(){
              soloPost.remove();
          }
        });

      }
    }),

    ItemGroupLabel: BaseView.extend({
      manage: true,
      className: 'groupLabel',
      tagName: 'li',
      initialize: function(options){
        this.label = options.label;
        this.template = _.template('<div><h3 class="glabel"><%=label%></h3></div>')(this.serialize());
      },
      serialize: function(){
        return {'label': this.label}
      }
    }),

    CollectionView : BaseView.extend({
      manage: true,
      tagName: 'ul',
      className: 'list postList',
      initialize: function(){
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'add', this.addItemView);
      },
      beforeRender: function() {
        var g = [], sort=this.collection.params.sort;
        this.collection.each(function(model) {
          if(this.collection.length > 1) {
            if (sort && sort !== 'pop') {
              if (model.get('created_at')) {
                var gname = Moment(model.get('created_at'), 'X').format('MMM YYYY');
                if (g.indexOf(gname) === -1) {
                  this.insertView(new Post.ItemGroupLabel({label: gname}));
                  g.push(gname);
                }
              }
            }
          }
          this.addItemView(model, false);
        }, this);
      },
      addItemView : function(model, render){
        var view = this.insertView(new Post.ModelView({model: model}));
        if (render !== false) view.render();
        this.regSub(view);
      },
      // overwrite
      insert: function($root, $el){
        $root.append(Utils.fadeNew($el));
      }
    }),

    // Stream View
    StreamView : BaseView.extend({
        manage: true,
        className: 'stream',
        params: {},
        initialize: function(options){
            var self = this;
            this.btnMoreHtm = '<a class="uiMore hasMore" href="#" title="More">&#8226; &#8226; &#8226;</a>';
            if (!this.posts){
                this.posts = new Data.Posts({});
            }
            if(options.params){
              this.params = options.params;
              this.posts.params = this.params;
            }
            this.posts.fetch({ reset: true, data:this.params, success: function(){
                var meta = self.posts.state.meta;
                var tc = '', pc = '';
//                    if(meta.types.knowledge === 0 || meta.types.question === 0) tc = 'uiDisabled';
                if(meta.total > 1) {
                    var html = "";
                    html += '<ul class="streamFilter">';
//                    html += 'streamFilter<li><label>Type:</label><select id="stFtType" class="'+tc+'">';
//                    if(meta.types.knowledge > 0 && meta.types.question > 0) html += '<option value="all">All</option>';
//                    if(meta.types.knowledge > 0) html += '<option value="knowledge">Knowledge ('+meta.types.knowledge+')</option>';
//                    if(meta.types.question > 0) html += '<option value="question">Question ('+meta.types.question+')</option>';
//                    html += '</select></li>';

                    html += '<li><label>Sort:</label><select id="stSort">';
                    html += '<option value="pop"';
                    if (!self.params.sort || self.params.sort && self.params.sort === 'pop') html += 'selected=selected';
                    html += '>Popular</option>';
                    html += '<option value="newest"';
                    if (self.params.sort && self.params.sort === 'newest') html += 'selected=selected';
                    html += '>Newest</option>';
                    html += '</select></li>';
                    html += '</ul>';
                    self.$el.prepend(html);
                }
                if (self.posts.state.meta.has_more) {
                    self.$el.append(self.btnMoreHtm);
                }
            }});
        },
        beforeRender: function(){
            this.regSub(this.insertView(new Post.CollectionView({collection: this.posts})));
        },
        events: {
            'click .hasMore': 'fetchMore',
            'change #stFtType': 'onFtTypeChanged',
            'change #stSort': 'onFtSortChanged'
        },
        fetchMore: function(e){
            e.preventDefault();
            var self = this;
            this.posts.state.pageNum += 1;
            this.params['page_num'] = this.posts.state.pageNum;
            this.posts.fetch({
                add: true,
                data: this.params,
                success: function(){
                    if (!self.posts.state.meta.has_more) {
                        self.$el.find('.hasMore').remove();
                    }
                }
            });
        },
        onFtTypeChanged: function(e){
            var self = this, val = $(e.currentTarget).val(), sort=$('#stSort').val();
            this.params['type'] = val;
            this.params['sort'] = sort;
            this.params['page_num'] = 1;
            this.posts.state.pageNum = 1;
            this.posts.fetch({
                reset: true,
                data: this.params,
                success: function(){
                    self.$el.find('.hasMore').remove();
                    if (self.posts.state.meta.has_more) {
                        self.$el.append(self.btnMoreHtm);
                    }
                }
            });
        },
        onFtSortChanged: function(e){
            var self = this, val = $(e.currentTarget).val(), ty=$('#stFtType').val();
            this.params['type'] = ty;
            this.params['sort'] = val;
            this.params['page_num'] = 1;
            this.posts.state.pageNum = 1;
            this.posts.fetch({
                reset: true,
                data: this.params,
                success: function(){
                    self.$el.find('.hasMore').remove();
                    if (self.posts.state.meta.has_more) {
                        self.$el.append(self.btnMoreHtm);
                    }
                }
            });
        }
    }),

    SoloView: BaseView.extend({
      manage: true,
      className: 'soloView postSoloView',
      tagName: 'div',
      beforeRender: function(){
        this.regSub(this.insertView(new Post.ModelView({model: this.model, tagName: 'div'})));
      }
    })
  };


    // Object contains activity related views
    var Activity = {

        // Model view
        ModelView : BaseView.extend({
            manage: true,
            tagName: 'li',
            className: 'item aa',
            beforeRender : function(){
                this.regSub(this.insertView(new Post.ModelView({
                    model: new Data.Post(this.model.get('posts')[0]),
                    aa: {
                        actors: this.model.get('actors'),
                        verb: this.model.get('verb'),
                        share_in_posts: this.model.get('share_in_posts')
                    },
                    tagName: 'div'
                })));
            }
        }),

        // Collection view
        CollectionView : BaseView.extend({
            manage: true,
            tagName: 'ul',
            className: 'list',
            initialize: function(){
                this.listenTo(this.collection, 'reset', this.render);
                this.listenTo(this.collection, 'add', this.addItemView);
            },
            beforeRender: function() {
                this.collection.each(function(model) {
                    this.addItemView(model, false);
                }, this);
            },
            addItemView : function(model, render){
                var view = this.insertView(new Activity.ModelView({model: model}));
                if (render !== false) view.render();
                this.regSub(view);
            },
            // overwrite
            insert: function($root, $el){
                $root.append(Utils.fadeNew($el));
            }
        }),

        // Stream View
        StreamView : BaseView.extend({
            manage: true,
            className: 'stream',
            events: {
                'click .hasmorePosts': 'fetchMore',
            },
            initialize: function(){
                var self = this;
                if (!this.activities){
                    this.activities = new Data.Activities();
                }
                this.activities.fetch({ reset: true, success: function(){
                    if (self.activities.state.hasMore) {
                        self.$el.append('<a class="uiMore hasmorePosts" href="#" title="More" data-titlemodal="init" data-placement="top">&#8226; &#8226; &#8226;</a>');
                    }
                }});
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Activity.CollectionView({collection: this.activities})));
            },
            fetchMore: function(e){
                e.preventDefault();
                var self = this;
                this.activities.state.pageNum += 1;
                this.activities.fetch({
                    add: true,
                    data: {
                        page_num:this.activities.state.pageNum,
                        page_size: this.activities.state.pageSize
                    },
                    success: function(){
                        if (!self.activities.state.hasMore) {
                            self.$el.find('.hasmorePosts').remove();
                        }
                    }
                });
            }
        })
    };

    // Object contains notification views
    var Notification = {
        // Model view
        ModelView : BaseView.extend({
            manage: true,
            tagName: 'li',
            className: 'item',
            template: _.template(popNotiHtml),
            serialize: function(){
                return {'_user':_user.toJSON(), 'notification': this.model.toJSON()}
            },
            events: {
                'click .check': 'onMark'
            },
            onMark: function(e){
                e.preventDefault();
                var self = this;
                $.ajax({
                    url: '/bb/notifications/'+this.model.get('serialization_id'),
                    method: 'PUT',
                    data: {'_xsrf': _xsrf},
                    success: function(res){
                        if(res.status === 'Ok'){
                            $(e.currentTarget).fadeOut();
                            self.$el.find('.notifItem').removeClass('unread unseen');
                        }
                    }
                });
            }
        }),
        // Collection view
        CollectionView : BaseView.extend({
            manage: true,
            tagName: 'ul',
            className: 'list',
            initialize: function(){
                this.listenTo(this.collection, 'reset', this.render);
                this.listenTo(this.collection, 'add', this.addItemView);
            },
            beforeRender: function() {
                this.collection.each(function(model) {
                    this.addItemView(model, false);
                }, this);
            },
            addItemView : function(model, render){
                var view = this.insertView(new Notification.ModelView({model: model}));
                if (render !== false) view.render();
                this.regSub(view);
            },
            // overwrite
            insert: function($root, $el){
                $root.append(Utils.fadeNew($el));
            }
        }),
        // Stream View
        StreamView : BaseView.extend({
            manage: true,
            className: 'stream',
            events: {
                'click .hasMore': 'fetchMore'
            },
            initialize: function(){
                if (!this.collection){
                    this.collection = new Data.Notifications();
                }
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Notification.CollectionView({collection: this.collection})));
            },
            afterRender: function(){
                this.$el.perfectScrollbar();
                if (this.collection.state.hasMore) {
                    this.$el.append('<a class="uiMore hasMore" href="#" title="More">&#8226; &#8226; &#8226;</a>');
                }
            },
            fetchMore: function(e){
                e.preventDefault();
                var self = this;
                this.collection.state.pageNum += 1;
                this.collection.fetch({
                    add: true,
                    data: {
                        page_num:this.collection.state.pageNum,
                        page_size: this.collection.state.pageSize
                    },
                    success: function(){
                        if (!self.collection.state.hasMore) {
                            self.$el.find('.hasMore').remove();
                            self.$el.perfectScrollbar('update');
                        }
                    }
                });
            }
        }),
        Dialog: BaseView.extend({
            manage: true,
            className: 'dialog',
            id: 'ntDlg',
            template: _.template('<h3 id="dlgTitle">Notifications</h3>'),
            initialize: function(){
                if (!this.streamView) this.streamView = new Notification.StreamView();
                this.regSub(this.insertView(this.streamView));
            }
        })
    };

    // Post Collection
    var Collection = {
        ModelView: BaseView.extend({
            manage: true,
            tagName: 'li',
            className: 'item',
            template: _.template(collHtml),
            serialize: function(){
                return {'_user':_user.toJSON(), 'collection': this.model.toJSON()}
            }
        }),
        CollectionView: BaseView.extend({
            manage: true,
            tagName: 'ul',
            className: 'list collist',
            initialize: function(){
                this.listenTo(this.collection, 'reset', this.render);
                this.listenTo(this.collection, 'add', this.addItemView);
            },
            beforeRender: function() {
                this.collection.each(function(model) {
                    this.addItemView(model, false);
                }, this);
            },
            addItemView : function(model, render){
                var view = this.insertView(new Collection.ModelView({model: model}));
                if (render !== false) view.render();
                this.regSub(view);
            },
            // overwrite
            insert: function($root, $el){
                $root.append(Utils.fadeNew($el));
            }
        }),
        StreamView: BaseView.extend({
            manage: true,
            className: 'stream',
            events: {
                'click .hasMore': 'fetchMore',
                // 'click #addNew': 'onAddNew'
            },
            initialize: function(options){
                if (!this.collection){
                    this.collection = new Data.Collections({user:options.user});
                }
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Collection.CollectionView({collection: this.collection})));
            },
            afterRender: function(){
                this.$el.perfectScrollbar();
//                this.$el.before('<a id="addNew" title="Add new" href="#">+New</a>');
                if (this.collection.state.hasMore) {
                    this.$el.append('<a class="uiMore hasMore" href="#" title="More">&#8226; &#8226; &#8226;</a>');
                }
            },
            fetchMore: function(e){
                e.preventDefault();
                var self = this;
                this.collection.state.pageNum += 1;
                this.collection.fetch({
                    add: true,
                    data: {
                        page_num:this.collection.state.pageNum,
                        page_size: this.collection.state.pageSize
                    },
                    success: function(){
                        if (!self.collection.state.hasMore) {
                            self.$el.find('.hasMore').remove();
                            self.$el.perfectScrollbar('update');
                        }
                    }
                });
            },

        }),
        Dialog: BaseView.extend({
            manage: true,
            className: 'dialog',
            id: 'collDlg',
            template: _.template('<h3 id="dlgTitle">Collections</h3><a id="addNew" title="Add new" href="#">+Add New</a>'),
            initialize: function(options){
                this.post = options.post;
                this.user = options.user;
                if (!this.streamView) this.streamView = new Collection.StreamView({'user':options.user});
                this.regSub(this.insertView(this.streamView));
            },
            events: {
                'click #addNew': 'onAddNew',
                'click #newCollSubmit': 'onNewCollSubmit',
                'focus #inpName': 'onInpNameFocus',
                'change .rdColl': 'onCollectionChoose'
            },
            onAddNew: function(e){
                e.preventDefault();
                var curEl = $(e.currentTarget),
                    collForm = $('#newCollForm');
                if (collForm.length === 0) {
                    var html = '<div id="newCollForm"><div class="inp">' +
                        '<div class="inpBdr"><input id="inpName" class="uiTextInp uiTextField" type="text" name="name" value="" maxlength="30" placeholder="Name" autocomplete="off" /></div>' +
                        '<input id="newCollSubmit" type="submit" class="uiBtn" value="Save" /> <br />' +
                        '<div class="rowOpt"><label><input id="inpPrivate" type="checkbox" name="private" /> Private</label></div>' +
                        '</div><div class="errStatus error hide">Input not valid</div>' +
                        '</div>';
                    this.$el.append($(html).slideDown(function(){
                        $('#inpName').focus();
                    }));
                    curEl.html('Cancel');
                }else{
                    collForm.remove();
                    curEl.html('+Add New');
                }
            },
            onInpNameFocus: function(){
                $('.errStatus').html('').hide();
            },
            onNewCollSubmit: function(e){
                e.preventDefault();
                var self = this,
                    curEl = $(e.currentTarget),
                    collName = $('#inpName').val(),
                    collForm = $('#newCollForm'),
                    collPrivate = $('#inpPrivate').prop('checked'),
                    err = $('.errStatus');

                if (collName){
                    $.post('/bb/'+this.user.get('username')+'/collections',{
                        _xsrf: _xsrf,
                        name: collName,
                        private:collPrivate
                    },'json').done(function(data){
                        if(data.status === 'Ok'){
                            // self.post.set('collect_count', self.post.get('collect_count')+1);
                            // data.collection.has_current_post = true;
                            // self.streamView.collection.add(data.collection);
                            // $('#addNew').html('+Add New');
                            // collForm.remove();
                            // new request to add post to this collection
                            self.addToCollection(data.collection.id);
                        }else{
                            err.html(data.msg).show();
                        }
                    });
                }else{
                    err.html('Name required.').show();
                }
            },
            onCollectionChoose: function(e){
              e.preventDefault();
              this.addToCollection($(e.currentTarget).val());
            },
            addToCollection: function(collection_id){
              var self = this;
              $.post('/bb/posts/'+self.post.get('id')+'/save', {
                _xsrf: _xsrf,
                save: true,
                collection_id: collection_id
              }, 'json').done(function(data){
                if(data.status == 'Ok'){
                  self.post.set('save_count', self.post.get('save_count')+1);
                  self.post.set('is_saved', true);
                  var notifyHtml = '<p>Post has been saved.</p>';
                  $(notifyHtml).notifyModal({
                    duration: 2500,
                    placement: 'center',
                    overlay: true,
                    // type: 'dark'
                    onClose: function(){
                      $('html').popModal("hide"); // close all popModal
                    }
                  });
                }
              }).fail(function(){
                alert('Server Error.');
              });
            }

        })
    };

    // Settings
    var Settings = {
        dlgEmail: BaseView.extend({
            manage: true,
            id: 'dlgEm',
            className: 'dlg',
            email: null,
            password: null,
            initialize: function(){
                this.template = _.template(editEmailHtml)(this.serialize());
            },
            serialize: function(){
                return this.model.toJSON();
            },
            events: {
                'keyup #uiEmailField': 'onEmailField',
                'keyup #rqPwd': 'onRequirePassword'
            },
            onEmailField: function(e){
                var self = this, target = $(e.currentTarget);
                target.doTimeout('typing', 500, function(){
                    var email = target.val(),
                        st = target.closest('.field').find('.state'),
                        btnSmt = $('#butOk'),
                        rqP8 = $('#rqPwd');
                    if(email){
                        if(email !== self.model.get('email')) {
                            if (Utils.validateEmail(email) === false) {
                                st.html("Not valid!").show();
                                btnSmt.addClass('uiBtnDisabled');
                                rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
                                self.email = null;
                            } else {
                                $.post('/check/unique/email', {_xsrf: _xsrf, 'email': email}, 'json').done(function (data) {
                                    if (data.status !== 'Ok') {
                                        st.html("Not available!").show();
                                        btnSmt.addClass('uiBtnDisabled');
                                        rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
                                        self.email = null;
                                    } else {
                                        st.html("").show();
                                        self.email = email;
                                        rqP8.closest('.pwdbox').removeClass('uiDisabled');
                                    }
                                });
                            }
                        }else{
                            st.html('').hide();
                            btnSmt.addClass('uiBtnDisabled');
                            rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
                            self.email = null;
                        }
                    }else{
                        st.html("Required!").show();
                        btnSmt.addClass('uiBtnDisabled');
                        rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
                        self.email = null;
                    }
                });
            },
            onRequirePassword: function(e){
                var p8 = $(e.currentTarget).val(), btnSmt = $('#butOk'), self = this, st = $('#p8Er');
                $(e.currentTarget).doTimeout('typing', 500, function(){
                    if(p8 && self.email){
                        $.post('/check/correct/password',{_xsrf: _xsrf, 'password':p8},'json').done(function(data) {
                            if(data.status === 'Ok'){
                                btnSmt.removeClass('uiBtnDisabled');
                                self.password = p8;
                                st.html('').hide();
                            }else{
                                st.html('incorrect').show();
                                btnSmt.addClass('uiBtnDisabled');
                                self.password = null;
                            }
                        });
                    }else{
                        btnSmt.addClass('uiBtnDisabled');
                        self.password = null;
                        st.html('').hide();
                    }
                });
            }
        }),
        // dlgUsername: BaseView.extend({
        //     manage: true,
        //     id: 'dlgUn',
        //     className: 'dlg',
        //     username: null,
        //     password: null,
        //     initialize: function () {
        //         this.template = _.template(editUsernameHtml)(this.serialize());
        //     },
        //     serialize: function () {
        //         return this.model.toJSON();
        //     },
        //     events: {
        //         'keyup #uiUsernameField': 'onUsernameField',
        //         'keyup #rqPwd': 'onRequirePassword'
        //     },
        //     onUsernameField: function(e){
        //         var self = this, target = $(e.currentTarget);
        //         target.doTimeout('typing', 500, function(){
        //             var val = target.val(),
        //                 st = target.closest('.field').find('.state'),
        //                 btnSmt = $('#butOk'),
        //                 rqP8 = $('#rqPwd');
        //             if(val){
        //                 if(val !== self.model.get('username')) {
        //                     if (!val.match(/^[a-zA-Z0-9]+$/)) {
        //                         st.html("Not valid!").show();
        //                         btnSmt.addClass('uiBtnDisabled');
        //                         rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
        //                         self.username = null;
        //                     } else {
        //                         $.get('/bb/auth/vuniu8', {'val': val}, 'json').done(function (data) {
        //                             if (!data.valid) {
        //                                 st.html(data.msg).show();
        //                                 btnSmt.addClass('uiBtnDisabled');
        //                                 rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
        //                                 self.username = null;
        //                             } else {
        //                                 st.html("").show();
        //                                 self.username = val;
        //                                 rqP8.closest('.pwdbox').removeClass('uiDisabled');
        //                             }
        //                         });
        //                     }
        //                 }else{
        //                     st.html('').hide();
        //                     btnSmt.addClass('uiBtnDisabled');
        //                     rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
        //                     self.username = null;
        //                 }
        //             }else{
        //                 st.html("Required!").show();
        //                 btnSmt.addClass('uiBtnDisabled');
        //                 rqP8.val('').closest('.pwdbox').addClass('uiDisabled').find('#p8Er').html('');
        //                 self.username = null;
        //             }
        //         });
        //     },
        //     onRequirePassword: function(e){
        //         var p8 = $(e.currentTarget).val(), btnSmt = $('#butOk'), self = this, st = $('#p8Er');
        //         $(e.currentTarget).doTimeout('typing', 500, function(){
        //             if(p8 && self.username){
        //                 $.post('/bb/auth/vp8',{'p8':p8},'json').done(function(data) {
        //                     if(data.valid){
        //                         btnSmt.removeClass('uiBtnDisabled');
        //                         self.password = p8;
        //                         st.html('').hide();
        //                     }else{
        //                         st.html('incorrect').show();
        //                         btnSmt.addClass('uiBtnDisabled');
        //                         self.password = null;
        //                     }
        //                 });
        //             }else{
        //                 btnSmt.addClass('uiBtnDisabled');
        //                 self.password = null;
        //                 st.html('').hide();
        //             }
        //         });
        //     }
        // }),
        dlgP8: BaseView.extend({
          manage: true,
          id: 'dlgP8',
          className: 'dlg',
          oldP8: null,
          newP8: null,
          newP8C: null,
          initialize: function () {
            this.template = _.template(changeP8Html)(this.serialize());
          },
          serialize: function () {
            return this.model.toJSON();
          },
          events: {
            'keyup #uiOldP8': 'onOldP8',
            'keyup #uiNewP8': 'onNewP8',
            'keyup #uiNewP8C': 'onNewP8C'
          },
          onOldP8: function(e){
            var target = $(e.currentTarget),
              btnSmt = $('#butOk'),
              newP8Fields = target.closest('.dlg').find('.newP8Fields'),
              self = this,
              st = target.closest('.field').find('.state');
            target.doTimeout('typing', 500, function(){
              if(target.val()){
                $.post('/check/correct/password',{_xsrf: _xsrf, 'password':target.val()},'json').done(function(data) {
                  if(data.status == 'Ok'){
                    newP8Fields.removeClass('uiDisabled');
                    self.oldP8 = target.val();
                    st.html('').hide();
                  }else{
                    st.html('incorrect').show();
                    btnSmt.addClass('uiBtnDisabled');
                    newP8Fields.addClass('uiDisabled');
                    self.oldP8 = null;
                  }
                });
              }else{
                btnSmt.addClass('uiBtnDisabled');
                newP8Fields.addClass('uiDisabled');
                self.oldP8 = null;
                st.html('').hide();
              }
              $('#uiNewP8').val('').closest('.field').find('.state').html('').hide();
              $('#uiNewP8C').val('').closest('.field').find('.state').html('').hide();
            });
          },
          onNewP8: function(e){
            var self = this,
              target = $(e.currentTarget),
              btnSmt = $('#butOk'),
              st = target.closest('.field').find('.state');
            target.doTimeout('typing', 500, function(){
              if(target.val() && self.oldP8){
                if (target.val().length < 8) {
                  self.newP8 = null;
                  btnSmt.addClass('uiBtnDisabled');
                  st.html('Password should be at least 8 characters').show();
                }else if(target.val() === self.oldP8){
                  self.newP8 = null;
                  btnSmt.addClass('uiBtnDisabled');
                  st.html('Password should be diffrent from old password').show();
                }
                else{
                  st.html('').hide();
                  self.newP8 = target.val();
                }
              }else{
                st.html('').hide();
                self.newP8 = null;
                btnSmt.addClass('uiBtnDisabled');
              }
              $('#uiNewP8C').val('').closest('.field').find('.state').html('').hide();
            });
          },
          onNewP8C: function(e){
              var self = this,
                btnSmt = $('#butOk'),
                target = $(e.currentTarget),
                st = target.closest('.field').find('.state');
              target.doTimeout('typing', 500, function(){
                if(target.val() && self.newP8){
                  if (target.val() !== self.newP8) {
                    self.newP8C = null;
                    btnSmt.addClass('uiBtnDisabled');
                    st.html('Password not match.').show();
                  }else{
                    st.html('').hide();
                    self.newP8C = target.val();
                    btnSmt.removeClass('uiBtnDisabled');
                  }
                }else{
                  st.html('').hide();
                  self.newP8C = null;
                  btnSmt.addClass('uiBtnDisabled');
                }
              });
          }
        }),
        // dlgEditName: BaseView.extend({
        //     manage: true,
        //     id: 'dlgN4',
        //     className: 'dlg',
        //     firstName: null,
        //     lastName: null,
        //     middleName: null,
        //     password: null,
        //     initialize: function () {
        //         this.template = _.template(editNameHtml)(this.serialize());
        //         this.firstName = this.model.get('first_name');
        //         this.middleName = this.model.get('middle_name');
        //         this.lastName = this.model.get('last_name');
        //     },
        //     serialize: function () {
        //         return this.model.toJSON();
        //     },
        //     events: {
        //         'keyup #uiFirstName': 'onFirstName',
        //         'keyup #uiLastName': 'onLastName',
        //         'keyup #uiMiddleName': 'onMiddleName',
        //         'keyup #uiP8': 'onP8'
        //     },
        //     onFirstName: function(e){
        //         var self = this,
        //             target = $(e.currentTarget),
        //             p8field = $('.p8field'),
        //             st = target.closest('.field').find('.state'),
        //             lastName = $('#uiLastName').val(),
        //             middleName = $('#uiMiddleName').val();
        //         target.doTimeout('typing', 500, function(){
        //             if(target.val()){
        //                 if(target.val().length < 2){
        //                     st.html('Name should be at least 2 characters.').show();
        //                     p8field.addClass('uiDisabled');
        //                 }else if (!target.val().match(/^[a-zA-Z]+$/)) {
        //                     st.html('Special characters are not allowed.').show();
        //                     p8field.addClass('uiDisabled');
        //                 }else if (target.val() === self.firstName){
        //                     if(lastName === self.lastName && middleName === self.middleName) p8field.addClass('uiDisabled');
        //                 }else{
        //                     if (lastName) p8field.removeClass('uiDisabled');
        //                     st.html('').hide();
        //                 }
        //             }else{
        //                 st.html('').hide();
        //                 p8field.addClass('uiDisabled');
        //             }
        //             $('#uiP8').val('');
        //         });
        //     },
        //     onLastName: function(e){
        //         var self = this,
        //             p8field = $('.p8field'),
        //             target = $(e.currentTarget),
        //             st = target.closest('.field').find('.state'),
        //             firstName = $('#uiFirstName').val(),
        //             middleName = $('#uiMiddleName').val();
        //         target.doTimeout('typing', 500, function(){
        //             if(target.val()){
        //                 if(target.val().length < 2){
        //                     st.html('Name should be at least 2 characters.').show();
        //                     p8field.addClass('uiDisabled');
        //                 }else if (!target.val().match(/^[a-zA-Z]+$/)) {
        //                     st.html('Special characters are not allowed.').show();
        //                     p8field.addClass('uiDisabled');
        //                 }else if(target.val() === self.lastName){
        //                     if(firstName === self.firstName && middleName === self.middleName) p8field.addClass('uiDisabled');
        //                 }else{
        //                     if (firstName) p8field.removeClass('uiDisabled');
        //                     st.html('').hide();
        //                 }
        //             }else{
        //                 st.html('').hide();
        //                 p8field.addClass('uiDisabled');
        //             }
        //             $('#uiP8').val('');
        //         });
        //     },
        //     onMiddleName: function(e){
        //         var self = this,
        //             p8field = $('.p8field'),
        //             target = $(e.currentTarget),
        //             st = target.closest('.field').find('.state'),
        //             firstName = $('#uiFirstName').val(),
        //             lastName = $('#uiLastName').val();
        //         target.doTimeout('typing', 500, function(){
        //             if(target.val()){
        //                 if(target.val().length < 2){
        //                     st.html('Name should be at least 2 characters.').show();
        //                     p8field.addClass('uiDisabled');
        //                 }else if (!target.val().match(/^[a-zA-Z]+$/)) {
        //                     st.html('Special characters are not allowed.').show();
        //                     p8field.addClass('uiDisabled');
        //                 }else if(target.val() === self.middleName){
        //                     if(firstName === self.firstName && lastName === self.lastName) p8field.addClass('uiDisabled');
        //                 }else{
        //                     if (firstName && lastName) p8field.removeClass('uiDisabled');
        //                     st.html('').hide();
        //                 }
        //             }else{
        //                 st.html('').hide();
        //                 if(firstName === self.firstName && lastName === self.lastName) p8field.addClass('uiDisabled');
        //             }
        //             $('#uiP8').val('');
        //         });
        //     },
        //     onP8: function(e){
        //         var self = this,
        //             target = $(e.currentTarget),
        //             p8 = target.val(), btnSmt = $('#butOk'),
        //             st = target.closest('.field').find('.state');
        //         $(e.currentTarget).doTimeout('typing', 500, function(){
        //             if(p8 && self.firstName && self.lastName){
        //                 $.post('/bb/auth/vp8',{'p8':p8},'json').done(function(data) {
        //                     if(data.valid){
        //                         btnSmt.removeClass('uiBtnDisabled');
        //                         self.password = p8;
        //                         st.html('').hide();
        //                     }else{
        //                         st.html('incorrect').show();
        //                         btnSmt.addClass('uiBtnDisabled');
        //                         self.password = null;
        //                     }
        //                 });
        //             }else{
        //                 btnSmt.addClass('uiBtnDisabled');
        //                 self.password = null;
        //                 st.html('').hide();
        //             }
        //         });
        //     }
        // }),
        Page : BaseView.extend({
            manage: true,
            id: 'settingsHtml',
            template: _.template(settingsHtml),
            serialize: function(){ return {'user': this.model.toJSON(), 'Moment':Moment} },
            events: {
                'click #uiBtnEditEmail': 'onEditEmail',
                'click #btnResendECC': 'onResendCC',
                'click #btnCancelEC': 'onCancelCC',
                'click #uiBtnEditUsername': 'onEditUsername',
                'click #uiBtnEditPassword': 'onEditPassword',
                'click #uiBtnSaveProfile' : 'onProfileSaved',
                'change #inpProfilePic': 'onChangePicture'
                // 'click #uiBtnEditName': 'onEditName',
                // 'click #uiBtnEditBio': 'onEditBio',
                // 'change input[name="sex"]': 'onSex'
            },
            onEditEmail: function(e){
                e.preventDefault();
                var self = this;
                var dlgEmail = new Settings.dlgEmail({model: self.model});
                $(_.template(modalHtml)({title:'Change Email', okButText: 'Submit', isCancelBut:true})).dialogModal({
                    onLoad: function(el){
                        el.find('#butOk').addClass('uiBtnDisabled');
                        dlgEmail.render().then(function(){
                            el.find('.content').html(this.el);
                            this.$el.find('#uiEmailField').focus();
                        });
                    },
                    onClose: function(){
                        dlgEmail.remove();
                    },
                    onOkBut: function(){
                        if(dlgEmail.email && dlgEmail.password){
                          self.postConfirmEmail(self, dlgEmail.email);
                        }
                    }
                });
            },
            postConfirmEmail: function(self, confirm_email){
              $.post(
                '/bb/auth/user/email',
                {_xsrf: _xsrf, 'email': confirm_email},
                'json').done(function(data){
                  if(data.status == 'Ok'){
                    var notifyHtml = '<p><strong>Please confirm your email</strong> <br />A ' +
                        'confirmation email was sent to <strong>' + confirm_email + '</strong>. ' +
                        'Your email will not be changed until you complete this step!</p>';
                    $(notifyHtml).notifyModal({
                        duration : 3000,
                        placement : 'center',
                        overlay : true,
                        // type : 'dark',
                        onClose: function(){
                          self.model.set('confirm_email', data.token);
                          self.render();
                        }
                    });
                  }
                }).fail(function(){
                  alert('Internal Server Error.');
                });
            },
            onResendCC: function(e){
                e.preventDefault();
                this.postConfirmEmail(this, this.model.get('confirm_email').email);
            },
            onCancelCC: function(e){
                e.preventDefault();
                var self = this, tar = $(e.currentTarget);
                $.ajax({
                    url: '/bb/auth/user/email',
                    method: 'DELETE',
                    data: {_xsrf: _xsrf, email: self.model.get('confirm_email').email},
                    success: function(data, textStatus, jqXHR){
                        if(jqXHR.status === 200) {
                          self.model.set('confirm_email', null);
                          self.render();
                        }
                    },
                    error: function(){
                      alert('Internal Server Error.');
                    }
                });
            },
            // onEditUsername: function(e){
            //     e.preventDefault();
            //     var self = this,
            //         dlgUsername = new Settings.dlgUsername({model: self.model});
            //     $(_.template(modalHtml)({title:'Change Username', okButText: 'Submit', isCancelBut:true})).dialogModal({
            //         onLoad: function(el){
            //             el.find('#butOk').addClass('uiBtnDisabled');
            //             dlgUsername.render().then(function(){
            //                 el.find('.content').html(this.el);
            //                 this.$el.find('#uiUsernameField').focus();
            //             });
            //         },
            //         onClose: function(){
            //             dlgUsername.remove();
            //         },
            //         onOkBut: function(){
            //             if(dlgUsername.username && dlgUsername.password){
            //                 $.ajax({
            //                     url: '/bb/auth/username',
            //                     method: 'PUT',
            //                     data: {_xsrf: _xsrf, 'username': dlgUsername.username, 'password': dlgUsername.password},
            //                     success: function(data, textStatus, jqXHR){
            //                         if(jqXHR.status === 200){
            //                             var notifyHtml = '<p><strong>Congratulation</strong> <br />Your ' +
            //                                 'Username has been changed.</p>';
            //                             $(notifyHtml).notifyModal({
            //                                 duration : -1,
            //                                 placement : 'center',
            //                                 overlay : true,
            //                                 type : 'dark',
            //                                 onClose: function(){
            //                                     self.model.set('username', dlgUsername.username);
            //                                     self.render();
            //                                 }
            //                             });
            //                         }
            //                     }
            //                 });
            //             }
            //         }
            //     });
            // },
            onEditPassword: function(e){
              e.preventDefault();
              var self = this,
                target = $(e.currentTarget),
                dlg = new Settings.dlgP8({model: self.model});
              $(_.template(modalHtml)({title:'Change Password', okButText: 'Submit', isCancelBut:true})).dialogModal({
                onLoad: function(el){
                  el.find('#butOk').addClass('uiBtnDisabled');
                  dlg.render().then(function(){
                    el.find('.content').html(this.el);
                    this.$el.find('#uiOldP8').focus();
                  });
                },
                onClose: function(){
                    dlg.remove();
                },
                onOkBut: function(){
                  if(dlg.oldP8 && dlg.newP8 && dlg.newP8C){
                    $.post(
                      '/bb/auth/user/password',
                      {
                        _xsrf: _xsrf,
                        'password_old': dlg.oldP8,
                        'password': dlg.newP8
                      },
                      'json'
                    ).done(function(data){
                      if(data.status == 'Ok'){
                        var notifyHtml = '<p><strong>Congratulation</strong> <br />Your ' +
                          'Password has been changed.</p>';
                        $(notifyHtml).notifyModal({
                          duration : -1,
                          placement : 'center',
                          overlay : true,
                          // type : 'dark',
                          onClose: function(){
                            self.model.set('password_changed_at', Moment.utc());
                            self.render();
                          }
                        });
                      }
                    }).fail(function(){
                      alert('Internal Server Error');
                    });
                  }
                }
              });
            },
            onChangePicture: function(e){
              e.preventDefault();
              var self=this, curEl = $(e.currentTarget);
              $.ajax({
                url: '/bb/auth/user/picture',
                method: 'POST',
                data: {'_xsrf': _xsrf},
                files: curEl,
                iframe: true // jquery-iframe-transport plugin
              }).complete(function(data) {
                if(data.statusText == 'OK'){
                  // if (self.model.get('detail') != null){
                  //   self.model.get('detail').picture_url_s = data.responseJSON.picture_url_s;
                  //   self.model.get('detail').picture_url_m = data.responseJSON.picture_url_m;
                  // }
                  self.model.set('detail', data.responseJSON.user_detail)
                  $('#profileImg').attr('src', self.model.get('detail').picture_url_m).fadeOut().fadeIn();
                  $('.authImg').attr('src', self.model.get('detail').picture_url_s).fadeOut().fadeIn();
                }
              });
            },
            onProfileSaved: function(e){
              e.preventDefault();
              var self = this;
              var errmsg = $('#errmsg'),
                full_name = $('input[name=full_name]').val(),
                gender = $('input[name=gender]:checked').val(),
                bio = $('textarea[name=bio]').val(),
                birthday_year = $('#birthday_year').val(),
                birthday_month = $('#birthday_month').val(),
                birthday_day = $('#birthday_day').val(),
                birthday = birthday_year + '-' + birthday_month + '-' + birthday_day;

              if(!full_name) errmsg.html('full name is required.').show();
              else if(typeof(gender) === 'undefined') errmsg.html('gender is required').show();
              else if(birthday_day == 0 || birthday_month == 0 | birthday_year == 0) errmsg.html('birthday is required.').show();
              else if(!bio) errmsg.html('bio is required.').show();
              else {
                errmsg.html("").hide();
                var d = {
                  '_xsrf': _xsrf,
                  'full_name': full_name,
                  'gender': gender,
                  'birthday': birthday,
                  'bio': bio
                }
                $.post('/bb/auth/user/detail', d, 'json').done(function(data){
                  if(data.status === 'Ok'){
                    self.model.set('detail', data.user_detail);
                    var notifyHtml = '<p>Profile saved.</p>';
                    $(notifyHtml).notifyModal({
                      duration : 2000,
                      placement : 'center',
                      overlay : false,
                      // type : 'dark',
                    });
                  }
                }).fail(function(){
                  console.log('something has gone wrong on the server side.');
                });
              }
            }
            // onEditName: function(e){
            //     e.preventDefault();
            //     var self = this,
            //         dlg = new Settings.dlgEditName({model: self.model});
            //     $(_.template(modalHtml)({title:'Change Name', okButText: 'Submit', isCancelBut:true})).dialogModal({
            //         onLoad: function (el) {
            //             el.find('#butOk').addClass('uiBtnDisabled');
            //             dlg.render().then(function () {
            //                 el.find('.content').html(this.el);
            //                 this.$el.find('#uiFirstName').focus();
            //             });
            //         },
            //         onClose: function () {
            //             dlg.remove();
            //         },
            //         onOkBut: function(){
            //             dlg.firstName = $('#uiFirstName').val().capitalize();
            //             dlg.lastName = $('#uiLastName').val().capitalize();
            //             dlg.middleName = $('#uiMiddleName').val().capitalize();
            //             if(dlg.firstName && dlg.lastName && dlg.password){
            //                 $.ajax({
            //                     url: '/bb/auth/fullname',
            //                     method: 'PUT',
            //                     data: {
            //                         _xsrf: _xsrf,
            //                         'first_name': dlg.firstName,
            //                         'last_name': dlg.lastName,
            //                         'password': dlg.password,
            //                         'middleName': dlg.middleName ? dlg.middleName : ''
            //                     },
            //                     success: function(data, textStatus, jqXHR){
            //                         if(jqXHR.status === 200){
            //                             var notifyHtml = '<p><strong>Congratulation</strong> <br />Your ' +
            //                                 'Name has been changed.</p>';
            //                             $(notifyHtml).notifyModal({
            //                                 duration : -1,
            //                                 placement : 'center',
            //                                 overlay : true,
            //                                 type : 'dark',
            //                                 onClose: function(){
            //                                     self.model.set('first_name', dlg.firstName);
            //                                     self.model.set('last_name', dlg.lastName);
            //                                     if (dlg.middleName) self.model.set('middle_name', dlg.middleName.capitalize());
            //                                     self.model.set('name_change_at', Moment.utc());
            //                                     self.render();
            //                                 }
            //                             });
            //                         }
            //                     }
            //                 });
            //             }
            //         }
            //     });
            // },
            // onEditBio: function(e) {
            //     e.preventDefault();
            //     var self = this, uiBio = null;
            //     $(_.template(modalHtml)({title: 'Change Bio', okButText: 'Submit', isCancelBut:true})).dialogModal({
            //         onLoad: function (el) {
            //             var t = self.model.get('title')?self.model.get('title'):'';
            //             el.find('.content').html('<div class="inpBdr"><textarea name="bio" placeholder="Summary" autocomplete="off" id="uiBio" class="uiTextInp uiTextarea uiTextareaNoResize">'+t+'</textarea></div>');
            //             uiBio = el.find('#uiBio');
            //             uiBio.autogrow().focus();
            //         },
            //         onOkBut: function(){
            //             if(uiBio.val() !== self.model.get('title')){
            //                 $.ajax({
            //                     url: '/bb/auth/title',
            //                     method: 'PUT',
            //                     data: {
            //                         _xsrf: _xsrf,
            //                         'title': uiBio.val()
            //                     },
            //                     success: function(data, textStatus, jqXHR){
            //                         if(jqXHR.status === 200){
            //                             var notifyHtml = '<p><strong>Congratulation</strong> <br />Your ' +
            //                                 'Bio has been changed.</p>';
            //                             $(notifyHtml).notifyModal({
            //                                 duration : -1,
            //                                 placement : 'center',
            //                                 overlay : true,
            //                                 type : 'dark',
            //                                 onClose: function(){
            //                                     self.model.set('title', uiBio.val());
            //                                     self.render();
            //                                 }
            //                             });
            //                         }
            //                     }
            //                 });
            //             }
            //         }
            //     });
            // },
            // onSex: function(e){
            //     var self = this, target = $(e.currentTarget);
            //     $.ajax({
            //         url: '/bb/auth/gender',
            //         method: 'PUT',
            //         data: { _xsrf: _xsrf, 'gender': parseInt(target.val()) },
            //         success: function (data, textStatus, jqXHR) {
            //             if (jqXHR.status === 200) {
            //                 self.model.set('gender', parseInt(target.val()));
            //                 self.render();
            //             }
            //         }
            //     });
            // }
        })
    };

    var Profile = {
        Content : BaseView.extend({
            manage: true,
            className: 'profileView view',
            id: 'profileView',
            template: _.template(profileHtml),
            serialize: function(){
                return {'_user': _user.toJSON(), 'user': this.model.toJSON()}
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Post.StreamView({params: {'owner': this.model.get('username')}})));
            },
            renderTemplate: function(){
                this.$el.html(_.template(profileHtml)(this.serialize()));
                this.stickit();
            },
            bindings: {
              '.uiPostCount':{
                observe: 'post_count'
              },
              '.uiFollowerCount': {
                observe: 'follower_count'
              },
              // '#followRow': {
              //   observe: 'is_followed',
              //   updateMethod: 'html',
              //   onGet: 'formatFollowRow'
              // }
            },
            // formatFollowRow: function(val){
            //   var html = '', lbl = val? 'Following':'Follow';
            //   html += '<a id="btnFollow" class="uiBtn uiBtnDark" href="#">'+lbl+'</a>'+
            //   return html;
            // },
            events: {
                'click #btnFollow': 'onFollow',
                'change #uiGetNotif': 'onGetNotif',
                'change #inpProfilePic': 'onProfilePicChanged',
                // 'click #profileImg': 'onProfileImgClick'
            },
            onFollow: function(e){
              e.preventDefault();
              var self = this, target = $(e.currentTarget),
                follow = self.model.get('is_followed')?false:true;
              // if(isfollow !== self.model.get('is_follow')){
                $.ajax({
                  url: '/bb/users/'+self.model.get('username')+'/follow',
                  data: {_xsrf: _xsrf, follow: follow},
                  method: 'POST',
                  success: function(){
                    var count = follow?self.model.get('follower_count')+1:self.model.get('follower_count')-1;
                    self.model.set('follower_count', count);
                    self.model.set('is_followed', follow);
                    if(follow){
                      target.html('Following').removeClass('uiBtnDark').addClass('uiDisabled');
                      var notifHtml = '<div id="getNotifRow" class="row"><label>'+
                      '<input type="checkbox" id="uiGetNotif" name="getNotif" value="0">'+
                      ' Get Notification</label></div>';
                      target.closest('.profileActionGroup').append(notifHtml);
                    }else{
                      target.html('Follow').addClass('uiBtnDark');
                      target.closest('.profileActionGroup').find('#getNotifRow').remove();
                    }
                  },
                  error: function(){
                    alert('Error following user.');
                  }
                });
              // }
            },
            onGetNotif: function(e){
              var self = this, notif = this.model.get('is_notified')?false:true;
              $.post('/bb/users/'+self.model.get('username')+'/notif',
                {_xsrf: _xsrf, notif: notif},'json').done(function(data){
                  self.model.set('is_notified', notif);
                }).fail(function(data){alert('Server Error.')});
            },
            // onProfilePicChanged: function(e){
            //     e.preventDefault();
            //     var self = this, target = $(e.currentTarget),
            //         form = target.closest('form');
            //     $.ajax({
            //         url: form.attr('action'),
            //         method: 'POST',
            //         data: {_xsrf: _xsrf},
            //         files: $('#inpProfilePic'),
            //         iframe: true // jquery-iframe-transport plugin
            //     }).complete(function(data) {
            //         if(data.statusText == 'OK'){
            //             self.model.set('picture_url_s', data.responseJSON.picture_url_48);
            //             self.model.set('picture_url_m', data.responseJSON.picture_url_250);
            //             $('#profileImg').attr('src', data.responseJSON.picture_url_250).fadeOut().fadeIn();
            //             $('.authImg').attr('src', data.responseJSON.picture_url_48).fadeOut().fadeIn();
            //         }
            //     });
            // },
            // onProfileImgClick: function(e){
            //     e.preventDefault();
            //     var displayName = this.model.get('first_name') && this.model.get('last_name')? this.model.get('first_name') +' '+ this.model.get('last_name'): '@'+this.model.get('username');
            //     $(_.template(modalHtml)({title:displayName, okButText: '', isCancelBut:true, imgUrl:this.model.get('picture_url_m')})).dialogModal({});
            // }
        })
    };

    var Topic = {
        // Model View
        ModelView : BaseView.extend({
            manage: true
        }),
        PageView : BaseView.extend({
            manage: true,
            className: 'pageTopic',
            template: _.template(topicHtml),
            serialize: function(){
                return {topic: this.model.toJSON()}
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Post.StreamView({params: {'topic': this.model.get('slug')}})));
            },
            renderTemplate: function(){
                this.$el.html(this.template(this.serialize()));
                this.stickit();
            },
            bindings: {
                '.uiFollowerCount': {
                    observe: 'follower_count',
                    updateMethod: 'html',
                    onGet: 'formatVal'
                }
            },
            formatVal: function(val){
                var html = ' &#8226; ' + val;
                return html;
            },
            events: {
                'click .btnFollow': 'onFollow'
            },
            onFollow: function(e){
                e.preventDefault();
                var self = this, target = $(e.currentTarget), follow = target.hasClass('follow');
                if(self.model.get('is_follow') !== follow) {
                    $.ajax({
                        url: '/bb/topics/'+self.model.get('slug')+'/follow',
                        data: {_xsrf: _xsrf, follow: follow},
                        method: 'PUT',
                        success: function(){
                            if (follow) {
                                self.model.set('follower_count', self.model.get('follower_count') + 1);
                                self.model.set('is_follow', true);
                                target.removeClass('follow').addClass('uiDisabled').html('Following');
                                target.before('<span class="stText text gray">You are following. </span>');
                            } else {
                                self.model.set('follower_count', self.model.get('follower_count') - 1);
                                target.addClass('follow').html('Follow');
                                target.parent().find('.stText').remove();
                            }
                        }
                    });
                }
            }
        })
    };

    var Tag = {
        TagView: BaseView.extend({
            manage: true,
            className: 'pageTopic pageTag',
            template: _.template(topicHtml),
            initialize: function(options){
                this.topic = options.topic;
                this.tag = options.tag
            },
            serialize: function(){
                return {topic: this.topic.toJSON(), tag: this.tag.toJSON()}
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Post.StreamView({params: {'topic': this.topic.get('slug'), 'tag': this.tag.get('slug')}})));
            }
        })
    };

    var MyPostView = {

        initialize: function(filters, titleVar){
            _layout.displayView('#centerCol', new MyPostView.MainView({filters:filters, titleVar: titleVar}));
            _layout.displayView('#rightCol', new MyPostView.PostMenuView());
            // layout.removeView('#leftCol');
            _layout.displayView('.left', new Menu.ContentMenu());
        },

        MainView: BaseView.extend({
            manage: true,
            className: 'pagePosts',
            initialize: function(options){
                this.filters = options.filters;
                this.template = _.template('<div><h1 class="pageTitle">Posts <span class="titleVar"><%-titleVar%></span></h1></div>')({titleVar: options.titleVar});
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Post.StreamView({params: this.filters})));
            }
        }),
        PostMenuView: BaseView.extend({
            manage: true,
            className: 'menu postMenu',
            initialize: function(){
                var self = this;
                $.get('/bb/posts/menu', {}, 'json').done(function(data){
                    self.template = _.template(yourPostMenuHtml)(data);
                    self.render();
                });
            }
        })
    };

    var SavedView = {

        initialize: function(options){
            _layout.displayView('.center', new SavedView.MainView({user:_user, c8nid: options.c8nid}));
            _layout.displayView('#rightCol', new SavedView.MenuView());
            _layout.removeView('#leftCol');
        },

        MainView: BaseView.extend({
            manage: true,
            className: 'pagePosts savedPosts',
            template: _.template('<h1 class="pageTitle">Saves</h1>'),
            initialize: function(options){
                this.user = options.user;
                this.c8nid = options.c8nid; // c8n (collection)
            },
            serialize: function(){
                return {user: this.user, c8nid:this.c8nid};
            },
            beforeRender: function(){
                this.regSub(this.insertView(new Post.StreamView({params: {
                    'saved_by': this.user.id,
                    'c8nid': this.c8nid
                }})));
            }
        }),
        MenuView: BaseView.extend({
            manage: true,
            className: 'collectionMenu block',
            initialize: function(){
                var self = this;
                $.get('/bb/collections', {'owner': _user.get('id')}, 'json').done(function(data){
                    var tpl = '<div><h2 class="block-title">Collections</h2><ul class="topiclist menu">' +
                        '<li class="item"><a href="#saves">All</a></li>' +
                        '<%for(var i=0; i < collections.length; i++){%>' +
                        '<%if(collections[i].post_count > 0){%>'+
                        '<li class="item"><a href="#saves/collection/<%-collections[i].id%>"><%-collections[i].name%></a> <span class="count"><%-collections[i].post_count%></span></li>'+
                        '<%}%>' +
                        '<%}%>' +
                        '</ul></div>';
                    self.template = _.template(tpl)(data);
                    self.render();
                });
            }
        })
    };

  var NewPostViews = {

    NPView: BaseView.extend({
      manage: true,
      className: 'newPost',
      id: 'pf',
      initialize: function(options){
        var tp = '<select id="selType">' +
            '<option value="text">Text</option>' +
            '<option value="product">Product</option>' +
            '</select> <br /><br />' +
            '<div class="typeFormContainer"></div>';
        this.template = _.template(tp);
        this.model = options.model;
      },
      serialize: function(){
        return {'user': this.model.toJSON()}
      },
      beforeRender: function(){
        this.regSub(this.setView('.typeFormContainer', new NewPostViews.TextPostView({model: this.model})));
      },
      events: {
        'change #selType': 'onTypeChanged'
      },
      onTypeChanged: function(e){
        e.preventDefault();
        var val = $(e.currentTarget).val();

        var v = this.getView(function(view){
          if(view.className === 'typeView') return view;
        });
        if (v) v.remove();

        var tv;
        if (val === 'photo') {
          tv = new NewPostViews.PhotoPostView();
        }else if(val == 'text'){
          tv = new NewPostViews.TextPostView({model: this.model});
        }else if(val == 'product'){
          tv = new NewPostViews.ProductPostView({model: this.model});
        }
        this.regSub(this.setView('.typeFormContainer', tv).render());
      }
    }),

    TextInputView: BaseView.extend({
      manage: true,
      id: 'textInputRow',
      className: 'row field',
      initialize: function(options){
        var vHtml = '<textarea id="pf_inp04" class="uiTextarea" placeholder="What have you found?" tabindex="1"></textarea>'+
        '<div id="charRemain">500</div><div id="inp04Error" class="hide error"></div>';
        this.parentView = options.parentView;
        this.template = _.template(vHtml);
        this.maxChar = options.maxChar;
      },
      events: {
        'keyup #pf_inp04': 'onKeyUp'
      },
      afterRender: function(){
        var self = this;
        $("#pf_inp04").autogrow().focus();
      },
      onKeyUp: function(e){
        var inp = $(e.currentTarget),
          tlength = inp.val().length;
        var remain = this.maxChar - parseInt(tlength);
        if (remain < 0){
          $('#pf_inp04').addClass('error');
          $('#inp04Error').html('Your text is too long.').show();
        }else{
          $('#pf_inp04').removeClass('error');
          $('#inp04Error').html('').hide();
        }
        $('#charRemain').text(remain);
        this.parentView.textLength = tlength;
      }
    }),

    TopicTagView: BaseView.extend({
      manage: true,
      template: _.template(formTopicTagHtml),
      initialize: function(options){
        this.parentView = options.parentView;
      },
      events: {
        'click .rmTopic': 'onRmTopic',
        'click .rmTag': 'onRmTag'
      },
      afterRender: function(){
        var self = this;
        $("#pf_inp04").autogrow().focus();
        // Make topic box auto suggest
        var inp05 = $('#pf_inp05');
        inp05.inputSuggest({
          serviceUrl: '/bb/topics/suggest',
          width: 199,
          appendTo: inp05.parent(),
          isRelative:true,
          isAllowAddNew: true,
          delayRequest: 300,
          onAddNew: function (sgt) {
            var inp = $(this);
            $.post('/bb/topics', {'name': inp.val(), '_xsrf': _xsrf},'json')
            .done( function (data) {
              $("#pf_ld").html("");
              if (data.status == 'Ok') {
                self.selectSggTopic(inp, data.topic.id, data.topic.name, "new");
              } else alert(data.message);
            });
          },
          onSelect: function (sgt) {
            self.selectSggTopic($(this), sgt.data, sgt.value, "");
          }
        });
      },
      selectSggTopic: function(ele, topicId, topicName, classes) {
        var selectedHtml = '<span class="selectedTopic topic item ' + classes + '"><span class="text">' + topicName + '</span><a href="#" class="xbt rmTopic">&times;</a></span>';
        var inp07 = $("#pf_inp07");
        this.parentView.postTopic = topicId;
        ele.before(selectedHtml).val('').hide();
        inp07.removeAttr('disabled').focus();
        this.initTagSuggest(inp07, topicId);
      },
      initTagSuggest: function(el, topicId){
        var self = this;
        el.inputSuggest({
          serviceUrl: '/bb/tags/suggest',
          width: 148,
          appendTo:el.parent(),
          isRelative:true ,
          isAllowAddNew: true,
          onAddNew: function (sgt) {
            var inp = $(this);
            $.post('/bb/tags', {'name': inp.val(), 'topic': topicId, '_xsrf': _xsrf}, 'json')
            .done(function (data) {
              $("#pf_ld").html("");
              if (data.status == 'Ok') {
                self.selectSggTag(inp, topicId, data.tag.id, data.tag.name, "new");
              } else alert(data.msg);
            });
          }, onSelect: function (sgt) {
            self.selectSggTag($(this), topicId, sgt.data, sgt.value, "");
          }
        });
      },
      selectSggTag: function(el, topicId, tagId, tagName, classes) {
        if (_.contains(this.postTags, tagId)){ el.val(''); return; }
        var selectedHtml = '<span class="selectedTag tag item ' + classes + '"><span class="text">' + tagName + '</span><a href="#" data-topic="'+topicId+'" class="xbt rmTag">&times;</a></span>';
        el.parent().before(selectedHtml);
        this.parentView.postTags.push(tagId);
        var selectedTags = el.closest('.tag-group').find('.selectedTag');
        if(selectedTags.length <= 2) el.val('').focus();
        else el.val('').attr('disabled','disabled').hide();
      },
      onRmTopic: function(e){
        e.preventDefault();
        this.postTopic = null;
        $('.selectedTag').remove();
        $("#pf_inp05").show().focus();
        $("#pf_inp07").attr('disabled', 'disabled').show();
        $(e.currentTarget).parent().remove();
      },
      onRmTag: function(e){
        e.preventDefault();
        $(e.currentTarget).parent().remove();
        $("#pf_inp07").removeAttr('disabled').show().focus();
        this.postTags = _.without(this.postTags, $(e.currentTarget).data('topic'));
      }
    }),

    TagView: BaseView.extend({
      manage: true,
      id: 'tagRow',
      className: 'row field',
      initialize: function(options){
        var html = '<div id="pf_f05" class="field tag-group">'+
        '<span class="inp-wpr"><input type="text" name="post_subtopic" id="pf_inp07" class="uiTextField uiTagInput input_field inst_search_field" autocomplete="off" maxlength="11" tabindex="3" placeholder="Tag" /></span>'+
        '</div>';
        this.parentView = options.parentView;
        this.template = _.template(html);
      },
      events: {
        'click .rmTag': 'onRmTag'
      },
      afterRender: function(){
        var self = this;
        this.initTagSuggest($("#pf_inp07"));
      },
      initTagSuggest: function(el){
        var self = this;
        el.inputSuggest({
          serviceUrl: '/bb/tags/suggest',
          width: 148,
          appendTo:el.parent(),
          isRelative:true ,
          isAllowAddNew: true,
          onAddNew: function (sgt) {
            var inp = $(this);
            $.post('/bb/tags', {'name': inp.val(), '_xsrf': _xsrf}, 'json')
            .done(function (data) {
              $("#pf_ld").html("");
              if (data.status == 'Ok') {
                self.selectSggTag(inp, data.tag.id, data.tag.name, "new");
              } else alert(data.msg);
            });
          }, onSelect: function (sgt) {
            self.selectSggTag($(this), sgt.data, sgt.value, "");
          }
        });
      },
      selectSggTag: function(el, tagId, tagName, classes) {
        if (_.contains(this.parentView.postTags, tagId)){ el.val(''); return; }
        var selectedHtml = '<span class="selectedTag tag item ' + classes + '"><span class="text">' + tagName + '</span><a href="#" data-tag="'+tagId+'" class="xbt rmTag">&times;</a></span>';
        el.parent().before(selectedHtml);
        this.parentView.postTags.push(tagId);
        var selectedTags = el.closest('.tag-group').find('.selectedTag');
        if(selectedTags.length <= 2) el.val('').focus();
        else el.val('').attr('disabled','disabled').hide();
      },
      onRmTag: function(e){
        e.preventDefault();
        $(e.currentTarget).parent().remove();
        $("#pf_inp07").removeAttr('disabled').show().focus();
        this.postTags = _.without(this.postTags, $(e.currentTarget).data('tag'));
      }
    }),

    TextPostView: BaseView.extend({
      manage: true,
      className: 'typeView',
      initialize: function(options){
        this.template = _.template(newTextFormHtml);
        this.model = options.model;
        this.postTopic = null;
        this.postTags = [];
        this.textLength = 0;
        this.maxTextLength = 500;
      },
      serialize: function(){
        return {'user': this.model.toJSON()}
      },
      events: {
        'click #pf_inp11': 'onSubmit',
      },
      beforeRender: function(){
        this.regSub(this.insertView('#pf_wpr01', new NewPostViews.TextInputView({'parentView':this, 'maxChar': this.maxTextLength})));
        this.regSub(this.insertView('#pf_wpr01', new NewPostViews.TagView({'parentView':this})));
      },
      onSubmit: function(e){
        e.preventDefault();
        var postText = $('#pf_inp04').val();
        if(!postText || this.maxTextLength < this.textLength){ this.$el.shake(2, 15, 180); return; }
        var postPrivacy = $('#pf_inp09').is(':checked') ? 'private' : 'public',
          postSource = $('#pf_inp_src').val(),
          self = this,
          data = {
            '_xsrf': _xsrf,
            'text': postText,
            'source': postSource,
            'privacy': postPrivacy
          };
        if (this.postTopic) data['topic'] = this.postTopic;
        if (this.postTags.length > 0) data['tags'] = JSON.stringify(this.postTags);

        $.ajax({
          url: '/bb/posts/text',
          method: 'POST',
//                    contentType: 'application/json',
          traditional: true,
          data: data,
          dataType: 'json',
          success: function (data) {
            $("#pf_ld").html("");
            if (data.status == 'Ok') {
              var notifyHtml = '<p>Your knowledge has been posted.</p>';
              $(notifyHtml).notifyModal({
                duration: 1500,
                placement: 'center',
                overlay: true,
                type: 'dark',
                onClose: function () {
                  //update counter
                  self.model.set('post_count', self.model.get('post_count')+1);
                  self.postPrivacy = 'public';
                  self.postTopic = null;
                  window.location.replace('/#posts');
                }
              });
            } else alert(data.msg)
          }
        });
      }
    }),

    ProductPostView: BaseView.extend({
      manage: true,
      className: 'typeView',
      initialize: function(options){
        var tp = '<div id="pf"><form id="pf_form" action="/bb/product/posts" method="POST">'+
            '<div id="pf_wpr01" class="pf_wpr">'+
            '<div class="row filed"><div class="inpBdr"><input id="inpURL" type="text" name="producturl". value="" class="uiTextInp uiTextField" placeholder="Product URL" autocomplete="off" /></div></div>'+
            '</div>'+
            '<div id="pf_opt" class="pf_fg">'+
              '<div class="fieldPostSubmit aRight">'+
                  // '<label class="uiPostPrivacy"><input type="checkbox" name="post_privacy" value="0" id="pf_inp09" tabindex="4" /> Private</label>'+
                  '<input type="submit" name="submit" value="Post" id="pf_inp11" class="submit uiBtn uiBtnOrange postSubmit" tabindex="6" />'+
              '</div>'+
            '</div>'+
        '</form></div>';
        this.template = _.template(tp);
        this.model = options.model;
      },
      beforeRender: function(){

      },
      afterRender: function(){
        $('#inpURL').focus();
      },
      events: {
        'click #pf_inp11': 'onSubmit',
      },
      onSubmit: function(e){
        e.preventDefault();
        var url = $('#inpURL').val();
        if (!url || !Utils.validateUrl(url)) { this.$el.shake(2, 15, 180); return; }
        $.get('/bb/posts/product', {'url':url}, 'json').done(function(data){
          console.log(data);
          if(data.status == 'Ok'){

          }
        });
      }
    }),

    PhotoPostView: BaseView.extend({
      manage: true,
      className: 'typeView',
      initialize: function(){
        var tp = '<form>' +
          '<p>Photo Post</p>' +
          '</form>';
        this.template = _.template(tp);
      },
      beforeRender: function(){

      }
    }),




      attPhotoPreview: BaseView.extend({
          manage: true,
          onClose: function(){},
          initialize: function(options){
              this.model = options.model;
              this.isSingle = options.isSingle;
//                var imgsrc = this.isSingle ? this.model.get('img') : this.model.get('img_150');
              var imgsrc = this.model.get('datauri');
              var photoHtml = '<a href="#" class="xbt xPhotoThumb">&times;</a> <img src="' + imgsrc + '" alt="" width="100%" />';
              this.template = _.template(photoHtml);
          },
          className: function(){
              var classes = 'view photoThumb';
              if (this.isSingle) classes += ' single';
              return classes;
          },
          events: {
              'click .xPhotoThumb': 'onXPhotoThumb'
          },
          onXPhotoThumb: function(e){
              e.preventDefault();
              this.onClose();
              this.remove();
          }
      }),

      NewPostView: BaseView.extend({
          manage: true,
          className: 'newPost',
          template: _.template(newPostHtml),
          initialize: function(){
              this.postQ = false;
              this.postTitle = null;
              this.postText = "";
              this.postPrivacy = 'public';
              this.postTopic = null;
              this.postTags = [];
              this.postPhotos = [];
              this.postLink = null;

              this.disAddTitle = false;
              this.disAddPhotos = false;
              this.disAddLink = false;

              this.MAX_PHOTOS = 4;
              this.rmTitle = true;

          },
          serialize: function(){
              return {user:this.model.toJSON()};
          },
          afterRender: function(){
              var self = this;
              $("#pf_inp04").autogrow().focus();
              // Make topic box auto suggest
              $('#pf_inp05').inputSuggest({
                  serviceUrl: '/bb/topics/suggest',
                  width: 199,
                  appendTo: $('#pf_inp05').parent(),
                  isRelative:true,
                  isAllowAddNew: true,
                  delayRequest: 300,
                  onAddNew: function (sgt) {
                      var inp = $(this);
                      $.post('/bb/topics', {'name': inp.val(), '_xsrf': _xsrf},'json')
                          .done( function (data) {
                              $("#pf_ld").html("");
                              if (data.status == 'Ok') {
                                  self.selectSggTopic(inp, data.topic.slug, data.topic.name, "new");
                              } else alert(data.message);
                          });
                  },
                  onSelect: function (sgt) {
                      self.selectSggTopic($(this), sgt.data, sgt.value, "");
                  }
              });
          },
          selectSggTopic: function(ele, topicSlug, topicName, classes) {
              var selectedHtml = '<span class="selectedTopic topic item ' + classes + '"><span class="text">' + topicName + '</span><a href="#" class="xbt rmTopic">&times;</a></span>';
              this.postTopic = topicSlug;
              ele.before(selectedHtml).val('').hide();
              $("#pf_inp07").removeAttr('disabled').focus();
              this.initTagSuggest($("#pf_inp07"), topicSlug);
          },
          initTagSuggest: function(el, topicSlug){
              var self = this;
              el.inputSuggest({
                  serviceUrl: '/bb/tags/suggest',
                  width: 148,
                  appendTo:el.parent(),
                  isRelative:true ,
                  isAllowAddNew: true,
                  onAddNew: function (sgt) {
                      var inp = $(this);
                      $.post('/bb/tags', {'name': inp.val(), 'topic': topicSlug, '_xsrf': _xsrf}, 'json')
                          .done(function (data) {
                              $("#pf_ld").html("");
                              if (data.status == 'Ok') {
                                  self.selectSggTag(inp, topicSlug, data.tag.slug, data.tag.name, "new");
                              } else alert(data.msg);
                          });
                  }, onSelect: function (sgt) {
                      self.selectSggTag($(this), topicSlug, sgt.data, sgt.value, "");
                  }
              });
          },
          selectSggTag: function(el, tid, tagSlug, tagName, classes) {
              if (_.contains(this.postTags, tagSlug)){ el.val(''); return; }
              var selectedHtml = '<span class="selectedTag tag item ' + classes + '"><span class="text">' + tagName + '</span><a href="#" data-topic="'+tagSlug+'" class="xbt rmTag">&times;</a></span>';
              el.parent().before(selectedHtml);
              this.postTags.push(tagSlug);
              var selectedTags = el.closest('.tag-group').find('.selectedTag');
              if(selectedTags.length <= 2) el.val('').focus();
              else el.val('').attr('disabled','disabled').hide();
          },
          events: {
              'change #pf_inp04': 'onPostTextChanged',
              'change #pf_inp09': 'onPostPrivacyChanged',
              'click #pf_inp11': 'onSubmit',
              'click .rmTopic': 'onRmTopic',
              'click .rmTag': 'onRmTag',
              'change #pf_inp13': 'onAttPhoto',
              'click #attLink': 'onAttLink',
              'click #addTitle': 'onAddTitleUI',
              'click #pfTitle .xbt': 'onRmTitleUI',
              'change #inpTitle': 'onTitleChanged',
              'click #addPhotos': 'onAddPhotoUI',
              'click #xPhotoUI': 'onRmPhotoUI',
              'change #inpPhoto': 'onPhotoUpload',
              'click #lnkDownloadFromWeb': 'onPhotoDownloadFromWeb',
              'click #addPhotoFromWebUI #xDl': 'onRmDlFromWeb',
              'click #addLink': 'onAddLinkUI',
              'click #pfLink #xAttLink': 'onRmLinkUI',
              'click #pfLink #ximg': 'onRmAttLinkImg',
              'change #inpLink': 'onLinkChanged',
              'change #inpPhotoUrl': 'onPhotoUrlChanged',
              'change #isPostQ': 'onPostQ',
              'change #pf_inp_src': 'onSrcChanged'
          },
          onPostTextChanged: function(e){
              this.postText = $(e.currentTarget).val();
          },
          onPostPrivacyChanged: function(e){
              this.postPrivacy = $(e.currentTarget).is(':checked') ? 'private' : 'public';
          },
          onRmTopic: function(e){
              e.preventDefault();
              this.postTopic = null;
              $('.selectedTag').remove();
              $("#pf_inp05").show().focus();
              $("#pf_inp07").attr('disabled', 'disabled').show();
              $(e.currentTarget).parent().remove();
          },
          onRmTag: function(e){
              e.preventDefault();
              $(e.currentTarget).parent().remove();
              $("#pf_inp07").removeAttr('disabled').show().focus();
              this.postTags = _.without(this.postTags, $(e.currentTarget).data('topic'));
          },
          onAddTitleUI: function(e){
              e.preventDefault();
              var curEl = $(e.currentTarget);
              if(!this.disAddTitle) { // prevent UI changed
                  this.disAddTitle = true;
                  curEl.addClass('uiDisabled');
                  $('#addLink').addClass('uiDisabled');
                  var html = '<div id="pfTitle" class="removable"><input id="inpTitle" type="text" name="title" value="" class="uiTextField input_field" placeholder="Title" autocomplete="off" /><a href="#" class="xbt">&times;</a></div>';
                  $('#pf_wpr01').prepend($(html).velocity("fadeIn", {duration: 300, complete: function(){
                      $('#inpTitle').focus();
                  }}));
                  this.MAX_PHOTOS = 1;
              }
          },
          onRmTitleUI: function(e){
              e.preventDefault();
              var curEl = $(e.currentTarget);
              this.disAddTitle = false;
              $('#addTitle').removeClass('uiDisabled');
              if(!this.disAddPhotos) $('#addLink').removeClass('uiDisabled');
              curEl.parent().remove();
              this.MAX_PHOTOS = 4;
          },
          onTitleChanged: function(e){
              this.postTitle = $(e.currentTarget).val();
          },
          onSrcChanged: function(e){
              this.postSrc = $(e.currentTarget).val();
          },
          onAddPhotoUI: function(e){
              e.preventDefault();
              var curEl = $(e.currentTarget);
              if(!this.disAddPhotos){
                  this.disAddPhotos = true;
                  this.disAddLink = true;
                  this.disAddTitle = true;
                  curEl.addClass('uiDisabled');
                  $('#addTitle').addClass('uiDisabled');
                  $('#addLink').addClass('uiDisabled');
                  var atthtml = '<div id="attPhotoUI"><div id="photoCont" class="hide"></div><div id="pfPhotoToolPane"><ul id="addPhotoOptions"><li><span class="Text">Upload</span><input id="inpPhoto" class="inputFile" name="picture" type="file" title="Add Picture" accept="image/*" /></li><li><a id="lnkDownloadFromWeb" class="lnkOpt" href="" title="Download from web">Add from web</a></li></ul></div><a href="#" id="xPhotoUI" class="xbt">&times;</a></div>';
                  if($('#pfTitle').length === 1){
                      $('#pfTitle').find('.xbt').hide();
                      this.rmTitle = false;
                      $('#pf_f02').before(atthtml);
                  }else{
                      $('#pf_f02').after(atthtml);
                  }
              }
          },
          onRmPhotoUI: function(e){
              e.preventDefault();
              var self = this, curEl = $(e.currentTarget);
              if(self.postPhotos.length > 0){
                  $.ajax({
                      url: '/bb/photos/tmp/delete',
                      data: {'files': JSON.stringify(self.postPhotos)},
                      method: 'PUT',
                      dataType: 'json',
                      success: function(data){
                          if (data.status == 'Ok'){
                              self.postPhotos = [];
                          }
                      }
                  });
              }
              $('#addPhotos').removeClass('uiDisabled');
              this.disAddPhotos = false;
              this.disAddLink = false;
              if($('#pfTitle').length === 0){
                  $('#addTitle').removeClass('uiDisabled');
                  $('#addLink').removeClass('uiDisabled');
                  this.disAddTitle = false;
              }else{
                  this.rmTitle = true;
                  $('#pfTitle').find('.xbt').show();
              }
              curEl.parent().remove();
          },
          onPhotoUpload: function(e){
              e.preventDefault();
              var self = this;
              $.each(e.target.files, function(index, file) {
                  var reader = new FileReader();
                  reader.onload = function(e) {
                      var object = {};
                      object.filename = file.name;
                      object.datauri = e.target.result;
                      self.newPhotoPreview(object);
                  };
                  reader.readAsDataURL(file);
              });
          },
          newPhotoPreview: function(object){
              // object = {'filename': '', 'datauri': ''}
              var self = this;
              var photoPreviewModel = Backbone.Model.extend({
                  initialize: function (preview) {
                      this.defaults = preview;
                  }
              });
              var isSingle = self.MAX_PHOTOS === 1;
              self.regSub(self.insertView('#photoCont', new NewPostViews.attPhotoPreview({
                  model: new photoPreviewModel(object),
                  isSingle: isSingle,
                  onClose: function(){
                      self.postPhotos = _.without(self.postPhotos, _.findWhere(self.postPhotos, {filename: this.model.get('filename')}));
                      if(self.postPhotos.length < self.MAX_PHOTOS){
                          $('#pfPhotoToolPane').show();
                      }
                      if(self.postPhotos.length === 0){
                          $('#photoCont').hide();
                          $('#pfPhotoToolPane').removeClass('hasTop');
                      }
                  }
              })).render());
              if (!$('#pfPhotoToolPane').hasClass('hasTop')) $('#pfPhotoToolPane').addClass('hasTop');
              if(self.postPhotos.length + 1 === self.MAX_PHOTOS){
                  $('#pfPhotoToolPane').hide();
              }
              if(self.postPhotos.length === 0) $('#photoCont').show();
              self.postPhotos.push(object);
              $("#pf_inp04").focus();
          },
          onPhotoDownloadFromWeb: function(e){
              e.preventDefault();
              $('#addPhotoOptions').hide();
              var html = '<div id="addPhotoFromWebUI"><input id="inpPhotoUrl" type="text" name="Photo Url" value="" class="uiTextField input_field" placeholder="Paste image url here ..." autocomplete="off" /><a href="#" id="xDl" class="xbt">&times;</a></div>';
              $('#pfPhotoToolPane').append($(html).velocity("fadeIn", {duration: 300, complete: function(){
                  $('#inpPhotoUrl').focus();
              }}));
          },
          onRmDlFromWeb: function(e){
              e.preventDefault();
              var curEl = $(e.currentTarget);
              $('#addPhotoOptions').show();
              curEl.parent().remove();
          },
          onPhotoUrlChanged: function(e){
              e.preventDefault();
              var self = this, val = $(e.currentTarget).val();
              // check if val is url
              if (val && Utils.validateUrl(val)){
                  // submit url to server and expect datauri back
                  $.get('/bb/photos/download?url='+val, {}, 'json').done(function(data){
                      self.newPhotoPreview(data);
                      $('#addPhotoOptions').show();
                      $('#addPhotoFromWebUI').remove();
                  });
              }else{
                  console.log("not valid");
              }
          },

          onAddLinkUI: function(e){
              e.preventDefault();
              var curEl = $(e.currentTarget);
              if(!this.disAddLink){
                  this.disAddLink = true;
                  this.disAddPhotos = true;
                  this.disAddTitle = true;
                  curEl.addClass('uiDisabled');
                  $('#addTitle').addClass('uiDisabled');
                  $('#addPhotos').addClass('uiDisabled');
                  var html = '<div id="pfLink" class="removable"><div class="inp"><input id="inpLink" type="text" name="title" value="" class="uiTextField input_field" placeholder="http://..." autocomplete="off" /><a href="#" id="xAttLink" class="xbt">&times;</a></div></div>';
                  $('#pf_f02').after($(html).velocity("fadeIn", {duration: 300, complete: function(){
                      $('#inpLink').focus();
                  }}));
              }
          },
          onRmLinkUI: function(e){
              e.preventDefault();
              this.disAddLink = false;
              this.disAddPhotos = false;
              this.disAddTitle = false;
              $('#addLink').removeClass('uiDisabled');
              $('#addTitle').removeClass('uiDisabled');
              $('#addPhotos').removeClass('uiDisabled');
              $('#pfLink').remove();
              this.postLink = null;
          },
          onLinkChanged: function(e){
              var self = this, val = $(e.currentTarget).val();
              if (val && Utils.validateUrl(val)) {
                  $.get('/bb/links/submit?url=' + val, {}, 'json').done(function (data) {
                      var html = '<div id="attLinkUi">';
                      html += '<a href="#" id="xAttLink" class="xbt">&times;</a>';
                      html += '<div class="image row"><a href="#" id="ximg" class="xbt">&times;</a><img src="'+data.image+'" alt="" width="100%" /></div>';
                      html += '<h3 class="title row">'+data.title+'</h3>';
                      html += '<div class="desc row">'+data.description+'</div>';
                      html += '<div class="source row"><span class="domain">'+data.domain+'</span></div>';
                      html += '</div>';
                      $('#pfLink').prepend($(html).velocity("fadeIn", {duration: 300, complete: function(){
                          $('#pf_inp04').focus();
                      }}));
                      $(e.currentTarget).parent().remove();
                      self.postLink = data;
                  });
              }
          },
          onRmAttLinkImg: function(e){
              e.preventDefault();
              var el = $(e.currentTarget);
              el.parent().remove();
              this.postLink.image = null;
          },
          onPostQ: function(e){
              this.postQ = $(e.currentTarget).prop('checked');
              if(this.postQ) {
                  $('#pf_inp04').attr('placeholder', 'What problem?');
                  $('.uiPostPrivacy').addClass('uiDisabled');
                  $('#pf_wpr04').hide();
              }else{
                  $('#pf_inp04').attr('placeholder', 'What have you learnt?');
                  $('.uiPostPrivacy').removeClass('uiDisabled');
                  $('#pf_wpr04').show();
              }
              $('#pf_inp04').focus();
          },
          onSubmit: function(e){
              e.preventDefault();
              var self = this;
              if(!(this.postText && this.postTopic)){ this.$el.shake(2, 15, 180); return; }
              var data = {
                      '_xsrf': _xsrf,
                      'text': this.postText,
                      'src': this.postSrc,
                      'privacy': this.postPrivacy,
                      'anonymous': false,
                      'topic': this.postTopic,
                      'isquestion': this.postQ
                  };
              if (this.postTitle) data['title'] = this.postTitle;
              if (this.postTags.length > 0) data['tags'] = JSON.stringify(this.postTags);
              if (this.postPhotos.length > 0) data['photos'] = JSON.stringify(this.postPhotos);
              if (this.postLink) data['link'] = JSON.stringify(this.postLink);
              $.ajax({
                  url: '/bb/posts/create',
                  method: 'POST',
                  traditional: true,
                  data: data,
                  success: function (data) {
                      $("#pf_ld").html("");
                      if (data.status == 'Ok') {
                          var notifyHtml = '<p>Your knowledge has been posted.</p>';
                          $(notifyHtml).notifyModal({
                              duration: 1500,
                              placement: 'center',
                              overlay: true,
                              type: 'dark',
                              onClose: function () {
                                  //clear form values
                                  $('#pf form')[0].reset();
                                  self.postTitle = null;
                                  self.postText = '';
                                  self.postSrc = '';
                                  self.postPrivacy = 'public';
                                  self.postTopic = null;
                                  self.postTags = [];
                                  self.postLink = null;
                                  window.location.replace('/i#posts');
                              }
                          });
                      } else alert(data.msg)
                  }
              });
          }
      })
  };

    var AuthUserViews = {
        FollowerView: BaseView.extend({
            manage: true,
            className: 'followerView userView view',
            template: _.template('<h1 class="pageTitle">Followers</h1>'),
            serialize: function(){ return this.model.toJSON(); },
            beforeRender: function(){
                this.regSub(this.insertView(new User.StreamView({params: {followers:true}})));
            }
        }),
        FollowingView: BaseView.extend({
            manage: true,
            className: 'followingView userView view',
            template: _.template('<h1 class="pageTitle">Following</h1>'),
            serialize: function(){ return this.model.toJSON(); },
            beforeRender: function(){
                this.regSub(this.insertView(new User.StreamView({params: {following:true}})));
            }
        })
    };


// ==============================================================================================

    // Object contains views for route: Home
    var HomeView = {
        initialize : function(){
            // layout.displayView('.left', new Menu.ContentMenu());
//            layout.displayView('.center', new Activity.StreamView());
          _layout.displayView('.center', new HomeView.Content({model:_user}));
        },
        Content : BaseView.extend({
            manage: true,
            template: _.template('<h1>Welcome Home. <%-username%></h1>'),
            serialize: function(){
              return this.model.toJSON();
            }
        })
    };

    // Object contains Views for route: Manage
    var ManageView = {

        Content : BaseView.extend({
            manage: true,
            template: _.template('<h1>Manage*</h1><div id="title"/><span class="author"></span> <input id="author" type="text">'),
            bindings: {
                '#title': 'title',
                '.author': 'author'
            },
            renderTemplate: function(){
                this.$el.html(this.template);
                this.stickit();
            }
        }),
        initialize : function(){
            var book = new Backbone.Model();
            book.set({
                title:'Backbone Application',
                author: 'viirak'
            });
            _layout.displayView('.center', new this.Content({model: book}));
        }
    };

    var ExploreView = {
        Content : Backbone.View.extend({
            manage: true,
            template: _.template('<h1>Explore*</h1>')
        }),
        Menu: Backbone.View.extend({
            manage: true,
            template: _.template('<h2>Explore Menu</h2><ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>')
        }),
        initialize : function(){
            _layout.displayView('.center', new this.Content);
        }
    };

    var SearchView = {
        Content : Backbone.View.extend({
            manage: true,
            template: _.template('<h1>Search</h1>')
        }),
        Menu: Backbone.View.extend({
            manage: true,
            template: _.template('<h2>Search Menu</h2><ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>')
        }),
        initialize : function(){
            _layout.displayView('.center', new this.Content);
            _layout.removeView('#leftCol');
            _layout.removeView('#rightCol');
        }
    };

    var ProfileView = {
        initialize : function(username){
          if(_user.get('username') !== username) {
            $.get('/bb/users/'+username,{},'json').done(function(user_dict){
              _layout.displayView('.center', new Profile.Content({model:new Data.User(user_dict)}));
            }).fail(function(){ alter('Server Error.') });
          }else{
            _layout.displayView('.center', new Profile.Content({model:_user}));
          }
          _layout.removeView('#leftCol');
          _layout.removeView('#rightCol');
        }
    };

    var SettingsView = {
      initialize : function(){
        _layout.displayView('.center', new Settings.Page({model:_user}));
        _layout.removeView('#leftCol');
        _layout.removeView('#rightCol');
      }
    };

    var TopicView = {
      initialize: function(slug){
        $.get('/bb/topics/'+slug,{},'json').done(function(d){
          _layout.displayView('.center', new Topic.PageView({model: new Data.Topic(d)}));
          _layout.removeView('#leftCol');
          _layout.removeView('#rightCol');
        });
      }
    };

    var TagView = {
      initialize: function(slug1, slug2){
        $.get('/bb/topics/'+slug1+'/'+slug2,{},'json').done(function(d){
          _layout.displayView('.center', new Tag.TagView({topic: new Data.Topic(d.topic), tag: new Data.Tag(d.tag)}));
          _layout.removeView('#leftCol');
          _layout.removeView('#rightCol');
        });
      }
    };

    var SinglePostView = {
      initialize: function(uid, key){
        $.get('/bb/'+uid+'/posts/'+key,{},'json').done(function(d){
          _layout.displayView('.center', new Post.SoloView({model: new Data.Post(d)}));
          _layout.removeView('#leftCol');
          _layout.removeView('#rightCol');
        });
      }
    };

    var NewTextPostView = {
      initialize: function(){
        // layout.displayView('.center', new NewPostViews.NewPostView({model:new Data.User(quser)}));
        // _layout.displayView('.center', new NewPostViews.NPView({model:_user}));
        _layout.displayView('.center', new NewPostViews.TextPostView({model:_user}));
        _layout.removeView('#leftCol');
        _layout.removeView('#rightCol');
      }
    };

    var NewProductPostView = {
      initialize: function(){
        // layout.displayView('.center', new NewPostViews.NewPostView({model:new Data.User(quser)}));
        // _layout.displayView('.center', new NewPostViews.NPView({model:_user}));
        _layout.displayView('.center', new NewPostViews.ProductPostView({model:_user}));
        _layout.removeView('#leftCol');
        _layout.removeView('#rightCol');
      }
    };

    var FollowerView = {
      initialize: function(){
        _layout.displayView('.center', new AuthUserViews.FollowerView({model:_user}));
        _layout.removeView('#leftCol');
        _layout.removeView('#rightCol');
      }
    };

    var FollowingView = {
      initialize: function(){
        _layout.displayView('.center', new AuthUserViews.FollowingView({model:_user}));
        _layout.removeView('#leftCol');
        _layout.removeView('#rightCol');
      }
    };

    return {
      initialize: initialize,
      Layout: Layout,
      Menu: Menu,
      HomeView: HomeView,
      ManageView: ManageView,
      ExploreView: ExploreView,
      SearchView: SearchView,
      Notification: Notification,
      SettingsView: SettingsView,
      ProfileView: ProfileView,
      TopicView: TopicView,
      TagView: TagView,
      SinglePostView: SinglePostView,
      MyPostView: MyPostView,
      SavedView: SavedView,
      FollowerView: FollowerView,
      FollowingView: FollowingView,
      NewTextPostView: NewTextPostView,
      NewProductPostView: NewProductPostView,
    };
});
