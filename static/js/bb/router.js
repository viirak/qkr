define(['backbone','moment','bb/views'], function(Backbone, Moment, Views){
  "use strict";

  var Router = Backbone.Router.extend({
    routes: {
      '': 'home',
      'home': 'home',
      'settings': 'settings',
      'posts': 'posts',
      'posts/t/:slug': 'posts_filter_topic',
      'posts/y/:year': 'posts_filter_year',
      'posts/m/:year/:month': 'posts_filter_month',
      'posts/pr/:privacy': 'posts_filter_privacy',
      'posts/type/:strtype': 'posts_filter_type',
      'posts/text/new': 'new_post_text',
      'posts/product/new': 'new_post_product',
      'favorites': 'favorites',
      'saves': 'saves',
      'saves/collection/:id': 'saves_by_collection',
      'followers': 'followers',
      'following': 'following',
      't/:slug': 'topic',
      't/:slug1/:slug2': 'tag',
      ':uid/posts/:key': 'profile_post',
      ':username':'profile'
    }
  });

  var initialize = function(user){

    var _user = user;
    var router = new Router();
    var topmenu = new Views.Menu.Top();
    var layout = new Views.Layout();

    Views.initialize({'user':user, 'layout':layout});

    router.on('route:home', function(){
      topmenu.setActive('.home');
      Views.HomeView.initialize();
    });

//        router.on('route:manage', function(){
//            topmenu.setActive('.manage');
//            Views.ManageView.initialize();
//        });
//
//        router.on('route:explore', function(){
//           topmenu.setActive('.explore');
//           Views.ExploreView.initialize();
//        });
//
//        router.on('route:search', function(){
//           topmenu.setActive('.search');
//           Views.SearchView.initialize();
//        });

    router.on('route:settings', function(){
     topmenu.setActive('.settings');
     Views.SettingsView.initialize();
    });

    router.on('route:posts', function(){
      var titleVar = '';
      topmenu.setActive('.posts');
      var filters = {
        'owner': _user.get('username'),
        'parent': false,
        'sort':'newest'
      };
      Views.MyPostView.initialize(filters, titleVar);
    });

    router.on('route:posts_filter_topic', function(slug){
      var titleVar = ' on #'+slug;
      var filters = {
        'owner': _user.get('username'),
        'parent': false,
        'topic': slug
      };
      Views.MyPostView.initialize(filters, titleVar);
    });

    router.on('route:posts_filter_year', function(year){
      var titleVar = ' in '+year,
        filters = {
          'owner': _user.get('username'),
          'parent': false,
          'start_ts': Moment.utc([year, 0]).unix(),
          'end_ts': Moment.utc([year, 11]).endOf('month').unix()
        };
      Views.MyPostView.initialize(filters, titleVar);
    });

    router.on('route:posts_filter_month', function(year, month){
      var titleVar = ' in '+month +' '+ year;
      var months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ];
      var startDate = Moment.utc([year, months.indexOf(month)]);
      var endDate = Moment(startDate).endOf('month');
      var filters = {
        'owner': _user.get('username'),
        'parent': false,
        'start_ts': startDate.unix(),
        'end_ts': endDate.unix()
      };
      Views.MyPostView.initialize(filters, titleVar);
    });

    router.on('route:posts_filter_type', function(strtype){
      var titleVar = ' in '+strtype;
      var filters = {
        'owner': _user.get('username'),
        'parent': false,
        'type': strtype
      };
      Views.MyPostView.initialize(filters, titleVar);
    });

    router.on('route:posts_filter_privacy', function(privacy){
      var titleVar = ' in '+privacy;
      var filters = {
        'owner': _user.get('username'),
        'parent': false,
        'privacy': privacy
      };
      Views.MyPostView.initialize(filters, titleVar);
    });

    router.on('route:new_post_text', function(){
      Views.NewTextPostView.initialize();
    });

    router.on('route:new_post_product', function(){
      Views.NewProductPostView.initialize();
    });

    router.on('route:saves', function(){
      Views.SavedView.initialize();
    });

    router.on('route:saves_by_collection', function(id){
     Views.SavedView.initialize({c8nid:id});
    });

    router.on('route:followers', function(){
     Views.FollowerView.initialize();
    });

    router.on('route:following', function(){
     Views.FollowingView.initialize();
    });

    router.on('route:topic', function(slug){
     topmenu.setActive('.topic');
     Views.TopicView.initialize(slug);
    });

    router.on('route:tag', function(slug1, slug2){
      topmenu.setActive('.tag');
      Views.TagView.initialize(slug1, slug2);
    });

    router.on('route:profile', function(username){
      topmenu.setActive('.profile');
      Views.ProfileView.initialize(username);
    });

    router.on('route:profile_post', function(uid, key){
      topmenu.setActive('.post');
      Views.SinglePostView.initialize(uid, key);
    });

    layout.$el.appendTo("#page-section");
    layout.render();

    // Start Backbone history a necessary step for bookmarkable URL's
    Backbone.history.start();
  };

  return {
    initialize : initialize
  };

});
