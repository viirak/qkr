$(document).ready(function () {
    // get cookies
    function getCookie(name) {
        var c = document.cookie.match("\\b" + name + "=([^;]*)\\b");
        return c ? c[1] : undefined;
    }

    var ojf = $('#openJoinForm');
    if(ojf.length > 0){
        var inpEmail = ojf.find('input[name=email]'),
            oj = {email:'', username:'', password:''},
            typed_text = {email:'', username:'', password:''};

        inpEmail.focus();

        $('.uiTextInp').each(function(){
            var target_name = $(this).attr('name');
            $(this).on('focus', function(){
                var self = $(this),
                    error = self.closest('.field').find('.fieldError');
                if(oj[target_name] == '') error.hide();
                else error.html('').hide();
            });
            $(this).on('blur', function(){
                var self = $(this),
                    val = $(this).val(),
                    error = self.closest('.field').find('.fieldError'),
                    url = '',
                    inp_data = {_xsrf: getCookie('_xsrf')};

                if (target_name === 'email') {
                    url = '/check/unique/email';
                    inp_data['email'] = val;
                }
                else if(target_name === 'username') {
                    url = '/check/unique/username';
                    inp_data['username'] = val;
                }
                else {
                    url = '/check/valid/password';
                    inp_data['password'] = val;
                }

                if (val && val !== typed_text[target_name]){
                    typed_text[target_name] = val;
                    $.post(url, inp_data, 'json').done(function(data){
                        if(data.status !== 'Ok'){
                            error.html(data.msg).show();
                            oj[target_name] = '';
                        }else{
                            error.html('').hide();
                            oj[target_name] = val;
                        }
                    }).fail(function(){
                        ojf.find('.formError').html('Server Fail.');
                    });
                }else{
                    if(oj[target_name] == '') error.show();
                }
            });
        });

        $('input[type=submit]').click(function(e){
            e.preventDefault();
            var self = $(this);
            if (!(oj.email && oj.username && oj.password)){
                self.closest('form').shake(2, 5, 180);
            }else{
                var d = {
                    _xsrf: getCookie('_xsrf'),
                    email: oj.email,
                    username:oj.username,
                    password:oj.password
                };
                $.post(ojf.attr('action'), d, 'json').done(function(data){
                    if(data.status == 'Ok'){
                        var notifyHtml = '<p>An account confirmation email has been sent to your email. ' +
                            'Please follow the instruction to complete your registration.</p>';
                        $(notifyHtml).notifyModal({
                            duration : 6000,
                            placement : 'center',
                            overlay : true,
                            type : 'dark',
                            onClose : function(){
                                window.location.replace('/login');
                            }
                        });
                    }
                }).fail(function(){
                    ojf.find('.formError').html('Server Fail.');
                });
            }
        });
    }
});
