$(document).ready(function () {
    // get cookies
    function getCookie(name) {
        var c = document.cookie.match("\\b" + name + "=([^;]*)\\b");
        return c ? c[1] : undefined;
    }
    // Focus on username on load
    $('#uiLoginUsername').focus();
    // Clear error message on text field focus
    $('.uiTextField').focus(function () {
        $(this).closest('form').find('.submitError').hide();
    });
    // Login Submit
    $('#uiLoginSubmit').click(function (e) {
        e.preventDefault();
        var self = $(this);
        var uiU8 = $("#uiLoginUsername"), uiP8 = $("#uiLoginPassword"),
            rememberMe = $('#uiLoginRememberMe').is(':checked') ? true : false,
            err = self.closest('form').find('.submitError');
        if (!(uiU8.val() && uiP8.val())) {
            self.closest('form').shake(2, 5, 180);
            return;
        }
        $.post(
            '/login',
            {
              _xsrf: getCookie('_xsrf'),
              username: uiU8.val(),
              password: uiP8.val(),
              remember_me: rememberMe
            },
            'json'
        ).done(function (data, textStatus, jqXHR) {
            switch (jqXHR.status) {
                case 200:
                    document.location = '/'; //reload
                    break;
                case 204:
                    self.closest('form').shake(2, 5, 180);
                    uiP8.val('');
                    uiU8.focus();
                    err.html('<strong>ERROR:</strong> username and password you have entered is not correct. <a href="/p8">Forget password?</a>').fadeIn();
                    break;
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log("Login fail. " + errorThrown);
            self.closest('form').shake(2, 5, 180);
        });
    });

});
