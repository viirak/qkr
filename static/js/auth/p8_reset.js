$(document).ready(function(){
    // get cookies
    function getCookie(name) {
        var c = document.cookie.match("\\b" + name + "=([^;]*)\\b");
        return c ? c[1] : undefined;
    }

    var inpP8 = $('input[name="password"]'),
        inpP8C = $('input[name="password_confirm"]'),
        inpSubmit = $('input[type="submit"]'),
        err = $('#fErr'),
        p8 = '',
        p8_val = '',
        p8_err = '';

    inpP8.focus();
    inpP8.on('focus', function(){
        err.html('');
    });
    inpP8.on('keyup', function(){
        inpP8C.val('');
        p8 = '';
    });
    inpP8.on('blur', function () {
        var password = $(this).val();
        if(password && password !== p8_val) {
            p8_val = password;
            $.post('/check/valid/password', {_xsrf: getCookie('_xsrf'), 'password': password}, 'json').done(function (data) {
                if (data.status === 'Error') {
                    p8_err = data.msg;
                    err.html(p8_err);
                }else{
                    p8_err = '';
                }
            });
        }else{
            if(p8_err){
                err.html(p8_err);
            }
        }
    });
    inpP8C.on('keyup', function(){
        $(this).doTimeout('typing', 500, function(){
            var val = $(this).val();
            if (val){
                if (val !== inpP8.val()) {
                    if (!p8_err) {
                        err.html('Password not match');
                    }
                    p8 = '';
                } else {
                    err.html('');
                    p8 = val;
                    p8_err = '';
                }
            }
        });
    });
    inpSubmit.click(function(e){
        e.preventDefault();
        var self = $(this);
        if(!p8){
            self.closest('form').shake(2, 5, 180);
        }else{
            $.post(self.closest('form').attr('action'),{_xsrf: getCookie('_xsrf'), 'password': p8},'json').done(function(data){
                if(data.status === 'Ok'){
                    var notifyHtml = '<p>Password has been saved.</p>';
                    $(notifyHtml).notifyModal({
                        duration : 6000,
                        placement : 'center',
                        overlay : true,
                        type : 'dark',
                        onClose : function(){
                            window.location.replace('/login');
                        }
                    });
                }else{
                    err.html(data.msg);
                }
            }).fail(function(){
                err.html('Server has failed saving password.');
            });
        }
    });
});