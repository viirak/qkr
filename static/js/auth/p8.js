$(document).ready(function(){
    // get cookies
    function getCookie(name) {
        var c = document.cookie.match("\\b" + name + "=([^;]*)\\b");
        return c ? c[1] : undefined;
    }

    var inpEmail = $('#inpEmail');
    inpEmail.focus();
    inpEmail.focus(function(){
        $(this).closest('form').find('.submitError').hide();
    });
    $('#uiBtnSubmit').click(function(e){
        e.preventDefault();
        var self = $(this);
        var email = inpEmail.val();
        var error = self.closest('form').find('.submitError');

        if (!email){
            self.closest('form').shake(2, 5, 180);
            $('#inpEmail').focus();
            return;
        }
        $.post('/p8',{_xsrf: getCookie('_xsrf'), 'email':email,'step':1},'json').done(function(data){
            if(data.status === 'Ok'){
                var notifyHtml = '<p>Instruction to reset password has been sent.</p>';
                $(notifyHtml).notifyModal({
                    duration : 6000,
                    placement : 'center',
                    overlay : true,
                    type : 'dark',
                    onClose : function(){
                        window.location.replace('/login');
                    }
                });
            }else{
                error.html('<strong>ERROR:</strong> ' + data.msg).fadeIn();
            }
        }).fail(function(){
            error.html('<strong>ERROR:</strong> Server fail.').fadeIn();
        });
    });
});