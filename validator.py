# coding=utf-8
""" valideer custom validators """
import re
import sys
import os
import cgi
import json
import valideer as v
import slugify
import datetime
import qkr.model as m
from qkr.model import User, Collection


class Password(v.String):
    """ validate password input """
    name = "password"

    def __init__(self, min_length=8, min_lower=1, min_upper=1, min_digits=0):
        super(Password, self).__init__(min_length=min_length)
        self.min_lower = min_lower
        self.min_upper = min_upper
        self.min_digits = min_digits

    def validate(self, value, adapt=True):
        super(Password, self).validate(value)

        if len(filter(str.islower, str(value))) < self.min_lower:
            raise v.ValidationError("At least %d lowercase characters required" % self.min_lower)

        if len(filter(str.isupper, str(value))) < self.min_upper:
            raise v.ValidationError("At least %d uppercase characters required" % self.min_upper)

        if len(filter(str.isdigit, str(value))) < self.min_digits:
            raise v.ValidationError("At least %d digits required" % self.min_digits)

        return value


class Boolean(v.Validator):
    """ validate boolean string """
    name = "bool"
    true = ("y", "yes", "1", "t", "true", "on")
    false = ("n", "no", "0", "f", "false", "off")

    def validate(self, value, adapt=True):
        if type(value) is bool:
            return value
        _value = str(value).lower()
        if _value in self.true:
            return True if adapt else value
        elif _value in self.false:
            return False if adapt else value
        else:
            self.error("bool is not valid")


class Uuid(v.Pattern):
    """ valiate if string is uuid pattern """
    name = "uuid"
    regexp = re.compile(r"^[0-9a-f]{8}(-?[0-9a-f]{4}){3}-?[0-9a-f]{12}$")


class QkPrivacyString(v.String):
    """ privacy string
    """
    def validate(self, value, adapt=True):
        """ validate value """
        super(QkPrivacyString, self).validate(value)
        if value not in ['public', 'private']:
            raise v.ValidationError("%s is not valid" % value)
        return value


class ValidEmail(v.String):
    """ validate if string is email address """
    name = "email"

    def validate(self, value, adapt=True):
        super(ValidEmail, self).validate(value)
        if not re.match(".+@.+\..+", value):
            raise v.ValidationError("Email not valid")
        return value.lower() if adapt else value


class UniqueEmail(ValidEmail):
    """ validate unique email """
    name = 'unique_email'

    def validate(self, value, adapt=True):
        super(UniqueEmail, self).validate(value)
        u = User.select(User.email).where(User.email == value)
        if u.count() > 0:
            raise v.ValidationError("Email %s is already taken" % value.lower())
        return value.lower() if adapt else value


class ValidName(v.String):
    """ validate name """
    name = 'clean_name'

    def __init__(self, min_length=2, max_length=30, allow_space=False, allow_digit=False):
        super(ValidName, self).__init__(min_length=min_length, max_length=max_length)
        self.allow_space = allow_space
        self.allow_digit = allow_digit

    def validate(self, value, adapt=True):
        _value = value.strip().lower()
        super(ValidName, self).validate(_value)
        regex = "^[a-zA-Z]*$"
        msg = "Special characters are not allowed"
        if self.allow_space and self.allow_digit:
            regex = "^[ a-zA-Z0-9]*$"
        elif self.allow_space:
            regex = "^[ a-zA-Z]*$"
            msg = "Special character and number are not allowed"
        elif self.allow_digit:
            regex = "^[a-zA-Z0-9]*$"
            msg = "Special character and space are not allowed"
        if not re.match(regex, _value):
            raise v.ValidationError(msg)
        return _value.title() if adapt else value


class ValidUsername(v.String):
    """ valid username
    """
    def __init__(self, min_length=4):
        super(ValidUsername, self).__init__(min_length=min_length)

    def validate(self, value, adapt=True):
        _value = value.strip().lower()
        if not re.match("^[a-zA-Z0-9]*$", _value):
            raise v.ValidationError("Special characters are not allowed")
        return _value if adapt else value


class UniqueUsername(ValidUsername):
    """ validate username input """
    name = 'username'

    def __init__(self, min_length=3):
        super(UniqueUsername, self).__init__(min_length=min_length)
        self.min_length = min_length

    def validate(self, value, adapt=True):
        _value = value.strip().lower()
        super(UniqueUsername, self).validate(_value)
        if User.select(User.username).where(User.username == _value).count() > 0:
            raise v.ValidationError("%s is already taken" % _value)
        with open(os.path.join(os.path.dirname(__file__), 'data', 'reserved_username.json'), 'r') as f:
            names = json.load(f)
            if _value in names:
                raise v.ValidationError("%s is already taken" % _value)
        return _value if adapt else value


class UniqueCollectionName(ValidName):
    name = 'unique_collection_name'

    def __init__(self, allow_space=True, allow_digit=True):
        super(UniqueCollectionName, self).__init__(allow_space=allow_space, allow_digit=allow_digit)

    def validate(self, value, adapt=True):
        _value = value.strip().lower()
        super(UniqueCollectionName, self).validate(_value)
        if m.Collection.select(m.Collection.slug).where(m.Collection.slug == slugify.slugify(_value)).count() > 0:
            raise v.ValidationError("%s is already taken" % _value)
        return _value if adapt else value


class Gender(v.String):
    """ validate gender value
    """
    def validate(self, value, adapt=True):
        super(Gender, self).validate(value)
        if value not in ['-1', '0', '1']:
            raise v.ValidationError("%s is not valid" % value)
        return int(value) if adapt else value


class DateString(v.String):
    """ validate date string value
    """
    def __init__(self, format="%d%m%Y"):
        super(DateString, self).__init__()
        self.format = format

    def validate(self, value, adapt=True):
        super(DateString, self).validate(value)
        try:
            d = datetime.datetime.strptime(value, self.format).date()
        except ValueError:
            raise v.ValidationError("%s is not valid" % value)
        return d if adapt else value


class ImageFile(v.Validator):
    """ validate image file
    """
    def validate(self, value, adapt=True):
        if not isinstance(value, cgi.FieldStorage):
            raise v.ValidationError("%s is not valid")
        allow_extensions = ['.gif', '.jpg', '.jpeg', '.png']
        (name, extension) = os.path.splitext(value.filename)
        image_file = value.file
        image_file.seek(0, os.SEEK_END)
        filesize = int(image_file.tell())  # file size in KB
        if extension not in allow_extensions:
            raise v.ValidationError("%s is not image")
        if (filesize / 1024) > 1024:  # max 1MB
            raise v.ValidationError("%s Image file is too big (min 1MB)" % value)
        return value if adapt else value


class ImageFileDict(v.Validator):
    """ validate image file dictionary
    """
    def validate(self, value, adapt=True):
        allow_extensions = ['gif', 'jpg', 'jpeg', 'png']
        (name, extension) = value['content_type'].split('/')
        if extension not in allow_extensions:
            raise v.ValidationError("%s is not image")
        bytes = sys.getsizeof(value['body'])
        if (bytes / 1024) > 1024:  # max 1MB
            raise v.ValidationError("%s Image file is too big (min 1MB)" % value)
        return value if adapt else value


class StringListOf(v.String):
    """ validate Json string of list
    """
    def __init__(self, validator):
        super(StringListOf, self).__init__()
        self.validator = validator

    def validate(self, value, adapt=True):
        """ validate value """
        try:
            _value = json.loads(value)
        except ValueError:
            raise v.ValidationError("%s is not json stringify" % value)
        try:
            d = v.parse({'value': [self.validator]}).validate({'value': _value})
        except v.ValidationError, e:
            raise v.ValidationError(str(e))
        return d.get('value') if adapt else value


class QkPhotoStr(v.String):
    """ json string
    """
    def __init__(self):
        super(QkPhotoStr, self).__init__()

    def validate(self, value, adapt=True):
        """ validate value
        :param value: [{},{}]
        :param adapt: converted value
        """
        try:
            _value = json.loads(value)
        except ValueError:
            raise v.ValidationError("%s is not json stringify" % value)

        try:
            d = v.parse({'value': [{
                '+filename': v.String,
                '+datauri': v.String
            }]}).validate({'value': _value})
        except v.ValidationError, e:
            raise v.ValidationError(str(e))
        return d.get('value') if adapt else value


class QkLinkStr(v.String):
    """ validate Json string object
    """
    def __init__(self):
        super(QkLinkStr, self).__init__()

    def validate(self, value, adapt=True):
        """ validate value
        """
        try:
            _value = json.loads(value)
        except ValueError:
            raise v.ValidationError("%s is not json stringify" % value)
        try:
            d = v.parse({
                '+url': v.String,
                '+title': v.String,
                'image': v.String,
                '+description': v.String,
                '+domain': v.String,
                'author': v.String
            }).validate(_value)
        except v.ValidationError, e:
            raise v.ValidationError(str(e))
        return d if adapt else value


class ValidURL(v.String):
    """ """
    name = "url"

    def validate(self, value, adapt=True):
        super(ValidURL, self).validate(value)
        regex = re.compile(
            r'^(?:http|ftp)s?://' # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
            r'localhost|' #localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?' # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if not regex.match(value):
            raise v.ValidationError("URL not valid")
        return value.strip() if adapt else value


# class UniqueUserCollectionName(CleanName):
#
#     def __init__(self, user):
#         super(UniqueUserCollectionName, self).__init__(allow_space=True)
#         self.user = user
#
#     def validate(self, value, adapt=True):
#         super(UniqueUserCollectionName, self).validate(value)
#         _value = value.lower().strip()
#         coll = models.DBSession.query(models.Collection)\
#             .filter(models.Collection.slug == slugify.slugify(value), models.Collection.user_id == self.user.id).first()
#         if coll:
#             raise v.ValidationError("%s : already exist" % value)
#         return _value if adapt else value
