import os
import ui
import util
import qkr.config

import tornado.options
from tornado.web import Application
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.template import Loader

from qkr.route import url_patterns
from qkr.util import setting_from_object

from tornadomail.backends.smtp import EmailBackend

define('port', default=8888, help='run on the given port', type=int)


class Qkr(Application):

    TEMPLATE_ROOT = os.path.join(os.path.dirname(__file__), "template")

    def __init__(self):
        settings = setting_from_object(qkr.config)
        settings.update({
            'app_name': 'Qkr',
            # 'app_domain': 'qakara.com',
            'allow_join': 'open',  # invite, open, close
            'template_path': self.TEMPLATE_ROOT,
            # 'static_url':'/static',
            # 'media_url':'/static/media',
            # 'static_path': os.path.join(os.path.dirname(__file__), "static"),
            'cookie_secret': "UdEZRdMZRiWlLF0UYURaFI6uLwKDeEqYnxGkto5J7Q4=",
            'login_url': '/login',
            'xsrf_cookies': True,  # reject post, put, delete requests that not contains _xsrf param
            'compress_response': True,
            'debug': True,
            'ui_modules': {'AuthMenu': ui.AuthMenu}
        })
        Application.__init__(self, url_patterns, **settings)

    @property
    def mail_connection(self):
        return EmailBackend(
            'localhost', 1025, '', '', False,
            template_loader=Loader(self.TEMPLATE_ROOT)
        )


if __name__ == "__main__":
    tornado.options.parse_command_line()
    server = HTTPServer(Qkr())
    server.listen(options.port)
    IOLoop.current().start()
