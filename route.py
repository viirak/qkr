import qkr.handler.auth as auth
import qkr.handler.check as check
import qkr.handler.bb as bb

from tornado.web import StaticFileHandler

url_patterns = [
    (r"/(favicon.ico)", StaticFileHandler, {'path': '/static/favicon.ico'}),

    (r"/", auth.Index),
    (r"/about", auth.About),
    (r"/login", auth.Login),
    (r"/logout", auth.Logout),
    (r"/join", auth.Join),
    (r"/join/(\w+)", auth.JoinByCode),
    (r"/p8", auth.ForgotPassword),
    (r"/p8/(\w+)", auth.ResetPassword),
    (r"/invite", auth.Invite),
    (r"/setting", auth.Setting),
    (r"/confirm_email/(\w+)", auth.ConfirmEmail),

    (r"/check/valid/password", check.Valid, dict(target='password')),
    (r"/check/valid/email", check.Valid, dict(target='email')),
    (r"/check/valid/username", check.Valid, dict(target='username')),
    (r"/check/valid/name", check.Valid, dict(target='name')),
    (r"/check/unique/email", check.Unique, dict(target='email')),
    (r"/check/unique/username", check.Unique, dict(target='username')),
    (r"/check/correct/password", check.Correct, dict(target='password')),

    (r"/bb/auth/user", bb.AuthUser),
    (r"/bb/auth/user/detail", bb.AuthUserDetail),
    (r"/bb/auth/user/email", bb.AuthUserEmail),
    (r"/bb/auth/user/password", bb.AuthUserPassword),
    (r"/bb/auth/user/picture", bb.AuthUserPicture),

    (r"/bb/users/(\w+)", bb.UserUsername),
    (r"/bb/users/(\w+)/follow", bb.UserFollowed),
    (r"/bb/users/(\w+)/notif", bb.UserFollowedNotif),
    (r"/bb/(\w+)/collections", bb.UserCollection),

    (r"/bb/topics/suggest", bb.TopicSuggest),
    (r"/bb/topics", bb.Topic),

    (r"/bb/tags/suggest", bb.TagSuggest),
    (r"/bb/tags", bb.Tag),

    # (r"/bb/(text|photo|link|code)/posts", bb.TypePost),
    (r"/bb/posts/text", bb.PostText),
    (r"/bb/posts/product", bb.PostProduct),

    (r"/bb/posts", bb.Post),
    (r"/bb/posts/menu", bb.PostMenu),
    (r"/bb/posts/(\d+)", bb.PostSingle),
    (r"/bb/posts/(\d+)/like", bb.PostLike),
    (r"/bb/posts/(\d+)/save", bb.PostSave),
    (r"/bb/posts/(\d+)/share", bb.PostShare),
    (r"/bb/posts/(\d+)/report", bb.PostReport),
    (r"/bb/posts/(\d+)/hide", bb.PostHide),
    (r"/bb/posts/(\d+)/subscribe", bb.PostSubscribe),
    (r"/bb/posts/(\d+)/comments", bb.PostComment),
    (r"/bb/posts/(\d+)/comments/(\d+)", bb.CommentSingle),
    (r"/bb/posts/(\d+)/comments/(\d+)/like", bb.CommentLike),
    (r"/bb/posts/(\d+)/comments/(\d+)/report", bb.CommentReport),
    (r"/bb/posts/(\d+)/comments/(\d+)/hide", bb.CommentHide),

    # (r"/bb/products/url", bb.ProductURL),

    (r"/bb/reports/categories", bb.ReportCategory)

]
