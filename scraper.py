# author: viirak
# date: Sunday, December 12 2015
import re
import requests
import ast
import qkr.config

from lxml import html
from urlparse import urlparse

class AmazonScrapper(object):
    """ scrape amazon product """

    # Access Key ID: AKIAJCNRXUEOYPRINJJA
    # Secret Access Key: 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

    asin = None
    title = None
    price = None
    brand = None
    image = None
    shipping = None
    features = None
    breadcrumb = None
    description = None

    def dict(self):
        return dict(
            title=self.title,
            price=self.price,
            brand=self.brand,
            image=self.image,
            description=self.features if self.features else self.description
        ) if self.tree is not None else None

    def __init__(self, product_url):
        self.product_url = product_url
        tree = self.__get_tree()
        self.tree = tree
        if tree is not None:
            self.asin = self.__get_asin()
            self.title = self.__get_title(tree)
            self.price = self.__get_price(tree)
            self.brand = self.__get_brand(tree)
            self.image = self.__get_image(tree)
            self.features = self.__get_features(tree)
            self.breadcrumb = self.__get_breadcrumb(tree)
            self.description = self.__get_description(tree)

    def __find_avail_path_text(self, tree, xpath_list):
        el = None
        for path in xpath_list:
            el = tree.xpath(path)
            if len(el) > 0:
                break
        return ' '.join(el[0].text.split()) if len(el) > 0 and el[0].text else None

    def __get_tree(self):
        o = urlparse(self.product_url)
        headers = {
            'Host': o.netloc,
            'Connection': 'keep-alive',
            'Cache-Control': 'max-age=0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Upgrade-Insecure-Requests': 1,
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36',
            'DNT': 1,
            'Referer': '',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8'
        }
        try:
            r = requests.get(self.product_url, headers=headers)
        except requests.exceptions.RequestException as e:
            return None
        print ">>> status code: " + str(r.status_code)
        tree = html.fromstring(r.content) if r.status_code == 200 else None
        return tree

    def __get_asin(self):
        match = re.search(r'(?i)(dp|product)(\/)\w+', self.product_url)
        return match.group().split('/')[1] if match else None

    def __get_title(self, tree):
        title_xpaths = [
            '//*[@id="productTitle"]',
            '//*[@id="fineArtTitle"]'
        ]
        return self.__find_avail_path_text(tree, title_xpaths)

    def __get_price(self, tree):
        # offer price
        offer_price_xpaths = [
            '//*[@id="priceblock_ourprice"]',
            '//*[@id="priceblock_saleprice"]',
            '//*[@id="buyNewSection"]/a/h5/div/div[2]/div/span[2]',
            '//*[@id="buyNewSection"]/h5/div/div[2]/div/span[2]',
            '//*[@id="price-quantity-container"]/div[2]/div/span[1]',
            '//*[@id="snsPrice"]/div/span[2]',
            '//*[@id="priceblock_dealprice"]'
        ]
        offer_price = self.__find_avail_path_text(tree, offer_price_xpaths)
        return offer_price.replace('$','')

    def __get_brand(self, tree):
        brand = None
        brand_xpaths = [
            '//a[@id="brand"]',
            '//a[@id="brandteaser"]',
            '//*[@id="fine-ART-ProductLabelArtistNameLink"]',
            '//*[@id="byline"]/span/span[1]/a[1]'
        ]
        brand_path = None
        brand_path_el = None
        for path in brand_xpaths:
            el = tree.xpath(path)
            if len(el) > 0:
                brand_path = path
                brand_path_el = el
                break

        if brand_path_el and len(brand_path_el) > 0:
            brand_text = brand_path_el[0].text
            if brand_text and brand_text.strip() != '':
                brand = brand_text.strip()
            elif brand_path:
                brand_path += '/@href'
                href = tree.xpath(brand_path)
                s = href[0].split("/")
                t = s[2] if s[1] == 'l' else s[1]
                brand = re.sub("[-]", " ", t)
        return brand

    def __get_features(self, tree):
        features = ""
        li_els = tree.xpath('//div[@id="featurebullets_feature_div"]//ul/li/span[@class="a-list-item"]')
        for i in range(0, len(li_els)):
            t = li_els[i].text.strip()
            features += '|' + t if i > 0 else t
        return features

    def __get_breadcrumb(self, tree):
        breadcrumb = ""
        els = tree.xpath('//*[@id="wayfinding-breadcrumbs_feature_div"]/ul/li/span/a')
        for i in range(0, len(els)):
            b = ' '.join(els[i].text.split())
            breadcrumb += '/'+ b
        return breadcrumb

    def __get_description(self, tree):
        desc_xpaths = [
            '//*[@id="productDescription"]/p',
            '//*[@id="productDescription"]/div[2]/div',
            '//*[@id="visual-rich-product-description"]/div/div[1]/div/div/div/div[2]/p',
            '//*[@id="productDescription"]//div[@class="productDescriptionWrapper"]'
        ]
        desc = self.__find_avail_path_text(tree, desc_xpaths)
        return desc

    def __get_image(self, tree):
        image_xpaths = [
            '//*[@id="landingImage"]/@data-a-dynamic-image',
            '//*[@id="fine-art-landingImage"]/@data-a-dynamic-image',
            '//*[@id="imgBlkFront"]/@data-a-dynamic-image'
        ]
        tar = None
        for path in image_xpaths:
            tar = tree.xpath(path)
            if len(tar) > 0:
                break
        return ast.literal_eval(tar[0]).keys()[0] if tar else None
