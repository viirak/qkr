import json
import re
import os
import hashlib
import string
import random
import urllib
import uuid
import math
import base64
import shutil
import dimensions
import cStringIO
import mimetypes
import tornado.escape
import datetime
import requests

from math import log
from urllib2 import urlopen, URLError, HTTPError
from binascii import a2b_base64
from PIL import Image, ImageSequence, ImageChops
from PIL.GifImagePlugin import getheader, getdata
from io import BytesIO
from random import choice
from tornado.web import RequestHandler
from urlparse import urlparse
# from hashids import Hashids

from qkr import config

salt = 'd7076a32845807bd12459f1432e4249641db6f43836c4c1aebf245fdaacf3a5c4b9b0c60'

def gen_token(length=16):
    chars = list(string.letters + string.digits)
    salt = ''.join([choice(chars) for i in range(length)])
    return salt

# def encrypt_id(id):
#     hashids = Hashids(salt=salt, min_length=16)
#     return hashids.encrypt(id)
#
# def decrypt_id(eid):
#     hashids = Hashids(salt=salt, min_length=16)
#     ids = hashids.decrypt(eid)
#     return ids[0] if ids else None

def setting_from_object(obj):
    settings = dict()
    for key in dir(obj):
        if key.isupper():
            settings[key.lower()] = getattr(obj, key)
    return settings


def cal_pop_score(created_at, **kwargs):
    # calculate popularity score of post
    # this algorithm is from reddit
    # Reddit ranking algorithms
    # http://amix.dk/blog/post/19588

    like_count = kwargs.get('like_count', None)
    comment_count = kwargs.get('comment_count', None)
    comment_like_count = kwargs.get('comment_like_count', None)
    save_count = kwargs.get('save_count', None)
    share_count = kwargs.get('share_count', None)
    report_count = kwargs.get('report_count', None)
    post_count = kwargs.get('post_count', None)
    collection_count = kwargs.get('collection_count', None)
    follower_count = kwargs.get('follower_count', None)
    child_count = kwargs.get('child_count', None)

    s = 0

    if like_count:
        s += like_count
    if comment_count:
        s += comment_count
    if comment_like_count:
        s += comment_like_count
    if save_count:
        s += save_count
    if share_count:
        s += share_count
    if child_count:
        s += child_count
    if report_count:
        s -= report_count
    # for user
    if post_count:
        s += post_count
    if follower_count:
        s += follower_count
    if collection_count:
        s += collection_count

    order = log(max(abs(s), 1), 10)
    sign = 1 if s > 0 else -1 if s < 0 else 0

    epoch = datetime.datetime(1970, 1, 1)
    # Returns the number of seconds from the epoch to date.
    td = created_at - epoch
    epoch_seconds = td.days * 86400 + td.seconds + (float(td.microseconds) / 1000000)
    seconds = epoch_seconds - 1134028003
    return round(sign * order + seconds / 45000, 7)


class QkImage(object):
    """ Image manipulation """

    def __init__(self, user, dir=None):
        """
        :param user: session user object
        :param target_dir: name of directory where picture will be saved in MEDIA_DIR
        """
        self.user = user
        self.media_path = config.MEDIA_PATH
        self.dir = dir

        # self.MEDIA_DIR = request.application.settings.get('media_path')


    # def _save_to_disk(self, input_file, target_path):
    #     """ save input file to disk """
    #     temp_path = target_path + '~'
    #     output_file = open(temp_path, 'wb')
    #     # Finally write the data to a temporary file
    #     input_file.seek(0)
    #     while True:
    #         data = input_file.read(2 << 16)
    #         if not data:
    #             break
    #         output_file.write(data)
    #     # If your data is really critical you may want to force it to disk first
    #     # using output_file.flush(); os.fsync(output_file.fileno())
    #     output_file.close()
    #     # Now that we know the file has been fully saved to disk move it into place.
    #     os.rename(temp_path, target_path)

    def _resize(self, img_path, box, fit, out):
        """ Down sample the image.
        @param img: Image -  an Image-object
        @param box: tuple(x, y) - the bounding box of the result image
        @param fix: boolean - crop the image to fill the box
        @param out: file-like-object - save the image into the output stream
        """
        img = Image.open(img_path)
        #preresize image with factor 2, 4, 8 and fast algorithm
        factor = 1
        while img.size[0] / factor > 2 * box[0] and img.size[1] * 2 / factor > 2 * box[1]:
            factor *= 2
        if factor > 1:
            img.thumbnail((img.size[0] / factor, img.size[1] / factor), Image.NEAREST)
        #calculate the cropping box and get the cropped part
        if fit:
            x1 = y1 = 0
            x2, y2 = img.size
            wRatio = 1.0 * x2 / box[0]
            hRatio = 1.0 * y2 / box[1]
            if hRatio > wRatio:
                y1 = int(y2 / 2 - box[1] * wRatio / 2)
                y2 = int(y2 / 2 + box[1] * wRatio / 2)
            else:
                x1 = int(x2 / 2 - box[0] * hRatio / 2)
                x2 = int(x2 / 2 + box[0] * hRatio / 2)
            img = img.crop((x1, y1, x2, y2))
        #Resize the image with best quality algorithm ANTI-ALIAS
        img.thumbnail(box, Image.ANTIALIAS)
        #save it into a file-like object
        # img.save(out, "JPEG", quality=75)
        img.save(out, quality=75)

    def _get_depth_path(self, hash):
        """
        Algorithm to calculate directory structure of uploaded Image
        :param hash:
        :return: directory path
        """
        n = 2
        alg = hashlib.md5()
        alg.update(hash)
        tmp_hash = str(alg.hexdigest()[:6])
        depth_ele = [tmp_hash[i:i + n] for i in range(0, len(tmp_hash), n)]
        path = ""
        for item in depth_ele:
            path += str(item) + '/'
        return path

    # def move_picture_from_tmp(self, tmp_filename, target_dir):
    #     '''
    #     Move image from tmp to target_dir folder name
    #     :param tmp_filename:
    #     :param target_dir:
    #     :return:
    #     '''
    #     (filename, ext) = os.path.splitext(tmp_filename)
    #     tmp_dir = os.path.join(self.MEDIA_DIR, 'tmp')
    #     tmp_file_path = os.path.join(tmp_dir, tmp_filename)
    #
    #     li = filename.split('_')
    #     hash_str = li[1] if len(li) > 1 else li[0]
    #     depth_path = self._get_depth_path(hash_str)
    #
    #     tar_dir_path = os.path.join(self.MEDIA_DIR, target_dir, depth_path)
    #     if not os.path.exists(tar_dir_path):
    #         os.makedirs(tar_dir_path)
    #
    #     tar_file_path = os.path.join(self.MEDIA_DIR, target_dir, depth_path, tmp_filename)
    #     # copy origin image to tar depth path
    #     if os.path.exists(tar_file_path):
    #         os.remove(tar_file_path)
    #     else:
    #         shutil.move(tmp_file_path, tar_file_path)
    #     return True

    # def upload(self, picture, sizes=None, crop=False):
    #     """
    #     default max size is 640px, uploaded image will be resized to width=640px
    #     :param picture: Picture file upload
    #     :param sizes: [width, width, ...] list of wanted width
    #     :param crop: allow image to be cropped
    #     :return: a hash file name of the picture
    #     """
    #     root_path = os.path.join(self.MEDIA_DIR, self.target_dir)
    #
    #     # temporary save uploaded file to system tmp
    #     tmp_path = os.path.join('/tmp', picture.filename)
    #     self._save_to_disk(picture.file, tmp_path)
    #
    #     hash_uuid = str(uuid.uuid4())
    #     hash_file_name = str(self.user.id) + '_' + hash_uuid
    #     target_dir_path = os.path.join(root_path, self._get_depth_path(hash_uuid)) if not self.target_dir == 'tmp' else root_path
    #     # create directory path
    #     if not os.path.exists(target_dir_path):
    #         os.makedirs(target_dir_path)
    #
    #     (input_file_name, input_file_ext) = os.path.splitext(picture.filename)
    #     target_path = os.path.join(target_dir_path, hash_file_name + input_file_ext)
    #     if os.path.exists(target_path):
    #         os.remove(target_path)
    #     self._resize(tmp_path, (640, 640), crop, target_path)
    #     os.remove(tmp_path)
    #
    #     # resize/crop for requested sizes
    #     if sizes:
    #         for size in sizes:
    #             resize_img_filename = hash_file_name + '_' + str(size) + input_file_ext
    #             resize_img_path = os.path.join(dir, resize_img_filename)
    #
    #             if not os.path.exists(resize_img_path):
    #                 self._resize(target_path, (size, size), crop, resize_img_path)
    #             else:
    #                 try:
    #                     os.remove(resize_img_path)
    #                 except OSError:
    #                     pass
    #     return hash_file_name + input_file_ext


    def save(self, img, sizes=None, crop=False):
        """
        default max size is 640px, uploaded image will be resized to width=640px
        :param sizes: [width, width, ...] list of wanted width
        :param crop: allow image to be cropped
        :return: a hash file name of the image
        """
        root_path = os.path.join(self.media_path, self.dir) if self.dir else self.media_path

        # temporary save uploaded file to system tmp
        tmp_path = os.path.join('/tmp', img['filename'])
        # save to disk
        out = open(tmp_path, 'wb')
        out.write(img['body'])
        out.close()

        hash_uuid = str(uuid.uuid4())
        hash_file_name = str(self.user.id) + '_' + hash_uuid
        target_dir_path = os.path.join(root_path, self._get_depth_path(hash_uuid)) if not self.media_path == '/tmp' else self.media_path
        # create target directory path
        if not os.path.exists(target_dir_path):
            os.makedirs(target_dir_path)

        # (input_file_name, input_file_ext) = os.path.splitext(img['filename'])
        input_file_ext = '.'+ img['content_type'].split('/')[1]
        target_path = os.path.join(target_dir_path, hash_file_name + input_file_ext)

        if os.path.exists(target_path):
            os.remove(target_path)

        # resize and save img to target path
        self._resize(tmp_path, (640, 640), crop, target_path)
        os.remove(tmp_path) # remove tmp img

        # resize/crop for requested sizes
        if sizes:
            for size in sizes:
                resize_img_filename = hash_file_name + '_' + str(size) + input_file_ext
                resize_img_path = os.path.join(target_dir_path, resize_img_filename)

                if not os.path.exists(resize_img_path):
                    self._resize(target_path, (size, size), crop, resize_img_path)
                else:
                    try:
                        os.remove(resize_img_path)
                    except OSError:
                        pass
        img_key = hash_file_name + input_file_ext
        return img_key


    def download_and_save(self, url, sizes=[], crop=False):

        root_path = os.path.join(self.media_path, self.dir) if self.dir else self.media_path
        hash_uuid = str(uuid.uuid4())
        # root_dir = os.path.join(self.MEDIA_DIR, target_dir)
        tmp_origin_path = os.path.join('/tmp', hash_uuid + '.jpg')
        # try:
        #     # urllib.urlretrieve(url, tmp_origin_path)
        #     f = urlopen(url)  # Open the url
        #     if f.headers.maintype == 'image':
        #         with open(tmp_origin_path, "wb") as local_file:  # Open our local file for writing
        #             local_file.write(f.read())
        # except HTTPError, e:  #handle errors
        #     print "HTTP Error:", e.code, url
        # except URLError, e:
        #     print "URL Error:", e.reason, url
        # o = urlparse(url)
        # headers = {
        #     'Host': o.netloc,
        #     'Connection': 'keep-alive',
        #     'Cache-Control': 'max-age=0',
        #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        #     'Upgrade-Insecure-Requests': 1,
        #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36',
        #     'DNT': 1,
        #     'Referer': '',
        #     'Accept-Encoding': 'gzip, deflate, sdch',
        #     'Accept-Language': 'en-US,en;q=0.8'
        # }
        # r = requests.get(url, headers=headers)
        r = requests.get(url)
        print ">>> status code: " + str(r.status_code)
        # print ">>> conent: " + str(r.content)
        if r.status_code == 200:
            f = open(tmp_origin_path,'wb')
            f.write(r.content)
            f.close()

        if not os.path.exists(tmp_origin_path):
            print ">>> file has not been downloaded."
            return None

        # d = os.path.join(root_dir, self._get_depth_path(uuid_str))
        # try:
        #     os.makedirs(d)  # create directory path
        # except OSError as exc:  # Python >2.5
        #     if exc.errno == errno.EEXIST and os.path.isdir(d):
        #         pass
        #     else:
        #         raise


        ori_img = Image.open(tmp_origin_path)

        # save original img
        hash_filename = str(self.user.id) + '_' + hash_uuid

        target_dir_path = os.path.join(root_path, self._get_depth_path(hash_uuid)) if not self.media_path == '/tmp' else self.media_path
        # create target directory path
        if not os.path.exists(target_dir_path):
            os.makedirs(target_dir_path)

        ori_img_path = os.path.join(target_dir_path, hash_filename + '.jpg')
        # if os.path.exists(ori_img_path):
        #     try:
        #         os.remove(ori_img_path)
        #     except OSError:
        #         pass
        ori_img.save(ori_img_path, quality=75)

        if len(sizes) > 0:
            # resize image
            for size in sizes:
                img_filename = hash_filename + '_' + str(size) + '.jpg'
                img_path = os.path.join(target_dir_path, img_filename)
                if os.path.exists(img_path):  # check if file exist
                    try:
                        os.remove(img_path)  # delete file
                    except OSError:
                        pass
                else:
                    self._resize(tmp_origin_path, (size, size), crop, img_path)
        os.remove(tmp_origin_path)  # delete origin file
        img_key = hash_filename + '.jpg'
        return img_key


    def get_path(self, img_key, width=None, height=None, crop=False):
        """
        get url file path
        :param width: wanted width
        :param height: wanted height
        :param crop: crop
        """
        root_path = os.path.join(self.media_path, self.dir) if self.dir else self.media_path
        (filename, file_ext) = os.path.splitext(img_key)
        li = filename.split('_')
        key = li[1] if len(li) > 1 else li[0]
        # no depht for tmp file
        depth_path = self._get_depth_path(key) if not self.media_path == '/tmp' else ''
        if width:
            resize_filename = filename + '_' + str(width) + file_ext
        else:
            resize_filename = filename + file_ext
        resize_file = os.path.join(root_path, depth_path, resize_filename)
        resize_file_path = os.path.join(self.dir, depth_path, resize_filename)

        if os.path.exists(resize_file):
            return resize_file_path
        else:
            ori_filename = filename + file_ext
            ori_file = os.path.join(root_path, depth_path, ori_filename)
            if os.path.exists(ori_file):
                try:
                    h = height if height else width
                    self._resize(ori_file, (width, h), crop, resize_file)
                    return resize_file_path
                except OSError:
                    pass
        return None

    def get_static_url(self, img_key, width=None, height=None, crop=False, include_host=False):
        """ gen full url of image """
        img_path = self.get_path(img_key, width, height, crop)
        if not img_path:
            return None
        static_url = os.path.join(config.MEDIA_URL, img_path)
        if include_host:
            host = 'http://' + config.APP_DOMAIN
            static_url = host + '/'+ static_url
        return static_url

    def get_dims(self, img_key):
        """
        :param img_key: img filename
        :return : [widht, height, content_type, filename]
        """
        root_dir = os.path.join(self.MEDIA_DIR, self.target_dir)
        (filename, file_ext) = os.path.splitext(img_key)
        li = filename.split('_')
        key = li[1] if len(li) > 1 else li[0]
        # no depht for tmp file
        depth_path = self._get_depth_path(key) if not self.target_dir == 'tmp' else ''
        resize_filename = filename + file_ext
        resize_file = os.path.join(root_dir, depth_path, resize_filename)
        if os.path.exists(resize_file):
            return dimensions.dimensions(resize_file)

    def save_imguri(self, imguri):
        """
        default max size is 640px, uploaded image will be resized to width=640px
        :param imguri: object contain {'filename': '', data: ''}
        :return: self
        """
        root_dir = os.path.join(self.MEDIA_DIR, self.target_dir)

        hash_uuid = str(uuid.uuid4())
        hash_file_name = str(self.user.id) + '_' + hash_uuid
        target_dir_path = os.path.join(root_dir, self._get_depth_path(hash_uuid)) if not self.target_dir == 'tmp' else root_dir
        # create directory path
        if not os.path.exists(target_dir_path):
            os.makedirs(target_dir_path)

        (input_file_name, input_file_ext) = os.path.splitext(imguri.get('filename'))
        target_img_path = os.path.join(target_dir_path, hash_file_name + input_file_ext)
        if os.path.exists(target_img_path):
            os.remove(target_img_path)

        # save file to tmp
        tmp_path = os.path.join('/tmp', imguri.get('filename'))
        image_data = re.sub('^data:image/.+;base64,', '', imguri.get('datauri')).decode('base64')
        with open(tmp_path, 'wb') as fp:
            fp.write(image_data)
            fp.close()

        # Open img file
        img = Image.open(tmp_path)
        # check if img is animated gif
        (width, height) = img.size
        content_type = mimetypes.guess_type(tmp_path)[0]

        try:
            img.seek(1)
        except EOFError:
            isanimated = False
        else:
            isanimated = True

        if not isanimated:
            #preresize image with factor 2, 4, 8 and fast algorithm
            factor = 1
            box = (640, 640)
            while img.size[0] / factor > 2 * box[0] and img.size[1] * 2 / factor > 2 * box[1]:
                factor *= 2
            if factor > 1:
                img.thumbnail((img.size[0] / factor, img.size[1] / factor), Image.NEAREST)
            #Resize the image with best quality algorithm ANTI-ALIAS
            img.thumbnail(box, Image.ANTIALIAS)
            img.save(target_img_path, quality=75)
            os.remove(tmp_path)
        else:
            # animated gif remain origin, since PIL cannot handle it
            shutil.move(tmp_path, target_img_path)

        image = dict({
            'owner': self.user.id,
            'target_dir': self.target_dir,
            'width': width,
            'height': height,
            'content_type': content_type,
            'depthpath': target_dir_path,
            'filename': hash_file_name + input_file_ext
        })

        return image

    def download_to_imguri(self, url):
        """
        :param url: image resource on the web
        """
        try:
            f = urlopen(url)  # Open the url
            filename = str(uuid.uuid4()) + '.' + str(f.headers['Content-Type']).split('/', 1)[1]
            headers = f.headers
            tmp_path = os.path.join('/tmp', filename)
            if f.headers.maintype == 'image':
                with open(tmp_path, "wb") as local_file:  # Open our local file for writing
                    local_file.write(f.read())
        except HTTPError, e:  # handle errors
            print "HTTP Error:", e.code, url
            raise HTTPError(url)
        except URLError, e:
            print "URL Error:", e.reason, url
            raise URLError(e.reason)

        # # convert image to datauri and return
        mime, _ = mimetypes.guess_type(tmp_path)
        with open(tmp_path, 'rb') as fp:
            data = fp.read()
            data64 = u''.join(base64.encodestring(data).splitlines())
            uri = u'data:%s;base64,%s' % (mime, data64)
        # after get datauri, remove the file
        if os.path.exists(tmp_path):
            os.remove(tmp_path)
        return {'headers': headers, 'datauri': uri}

    # def img_to_data(self, path):
    #     """ convert a file (specified by path) into a data URI
    #     """
    #     if os.path.exists(path):
    #         mime, _ = mimetypes.guess_type(path)
    #         with open(path, 'rb') as fp:
    #             data = fp.read()
    #             data64 = u''.join(base64.encodestring(data).splitlines())
    #             return u'data:%s;base64,%s' % (mime, data64)

    # def remove_media_tmp_images(self, img_key, sizes=None):
    #     tmp_path = os.path.join(self.MEDIA_DIR, 'tmp')
    #     file_path = os.path.join(tmp_path, img_key)
    #     if os.path.exists(file_path):
    #         os.remove(file_path)
    #     if sizes:
    #         (filename, ext) = os.path.splitext(img_key)
    #         for size in sizes:
    #             size_file = os.path.join(tmp_path, filename+'_'+str(size)+ext)
    #             if os.path.exists(size_file):
    #                 os.remove(size_file)
    #     return True

    def remove(self, img_key, sizes=None):
        """ remove image """
        root_path = os.path.join(self.media_path, self.dir) if self.dir else self.media_path
        (filename, file_ext) = os.path.splitext(img_key)
        li = filename.split('_')
        key = li[1] if len(li) > 1 else li[0]
        depth_path = self._get_depth_path(key) if not self.media_path == '/tmp' else ''
        files = []
        if sizes:
            for size in sizes:
                resize_filename = filename + '_' + str(size) + file_ext
                files.append(resize_filename)
        else:
            files.append(img_key)

        for f in files:
            file_path = os.path.join(root_path, depth_path, f)
            if os.path.exists(file_path):
                try:
                    os.remove(file_path)
                except OSError:
                    pass
