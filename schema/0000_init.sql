-- AUTHOR: VIRAK HOR
-- DATE: Sun 23 Aug 2015
-- ----------------
-- timestamp DEFAULT NOT NULL end 2038 (32 bit)
-- CURRENT_TIMESTAMP() to return UTC time
-- in my.cf set : default_time_zone='+00:00'
-- -----------
-- UNHEX() to reduce string of hex digit by half
-- char(32) = binary(16) if UNHEX()
-- HEX() to get if back
-- -----------
-- max char : 255
-- max varchar: 65535
-- max int unsigned: 4.3 billion
-- max tinyint unsigned: 255
-- max smallint unsigned: 65535
-- max mediumint unsigned: 16777215
-- -----------------------------------------------------------------------

--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` char(32) NOT NULL,
  `status` enum('active','cancel','inactive','suspend') NOT NULL DEFAULT 'inactive',
  `follower_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `following_count` smallint(6) unsigned NOT NULL DEFAULT 0,
  `post_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `comment_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `save_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `collection_count` smallint(6) unsigned NOT NULL DEFAULT 0,
  `share_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `invite_quota` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `pop_score` float unsigned NOT NULL DEFAULT 0.0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activated_at` timestamp NOT NULL DEFAULT 0,
  `suspended_at` timestamp NOT NULL DEFAULT 0,
  `cancelled_at` timestamp NOT NULL DEFAULT 0,
  `login_at` timestamp NOT NULL DEFAULT 0,
  `password_changed_at` timestamp NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_token`
--
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `token` char(32) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `expired_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_token_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_detail`
-- id = user.id (enforce 1:1)
--

DROP TABLE IF EXISTS `user_detail`;
CREATE TABLE `user_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gender` enum('male','female','other') NOT NULL DEFAULT 'other',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `picture` varchar(255) NOT NULL DEFAULT '-',
  `full_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `bio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),
  CONSTRAINT `user_detail_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_invitation`
--

DROP TABLE IF EXISTS `invitation`;
CREATE TABLE `invitation` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `status` enum('request','invite','join') NOT NULL,
  `token` varchar(32) NOT NULL DEFAULT '-',
  `token_expired_at` timestamp NOT NULL DEFAULT 0,
  `requested_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `invited_at` timestamp NOT NULL DEFAULT 0,
  `joined_at` timestamp NOT NULL DEFAULT 0,
  `via_user_id` int(11) unsigned NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `via_user_id` (`via_user_id`),
  CONSTRAINT `user_invitation_ibfk_1` FOREIGN KEY (`via_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `topic`
--
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `parent_id` int(11) unsigned,
  `picture` varchar(255) NOT NULL DEFAULT '-',
  `post_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `follower_count` mediumint(9) NOT NULL DEFAULT 0,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  `owner_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `slug` (`slug`),
  KEY `owner_id` (`owner_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `topic_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `topic_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `topic` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `tag`
--
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) unsigned,
  `post_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `owner_id` int(11) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `slug` (`slug`),
  KEY `owner_id` (`owner_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `tag_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `tag_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `tag` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `post`
--
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('draft','future','private','publish','trash') NOT NULL DEFAULT 'draft',
  `type` varchar(10) NOT NULL,
  `like_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `comment_count` smallint(6) unsigned NOT NULL DEFAULT 0,
  `comment_like_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `share_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `report_count` smallint(9) unsigned NOT NULL DEFAULT 0,
  `view_count` int(11) unsigned NOT NULL DEFAULT 0,
  `save_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `pop_score` float unsigned NOT NULL DEFAULT 0.0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published_at` timestamp NOT NULL DEFAULT 0,
  `trashed_at` timestamp NOT NULL DEFAULT 0,
  `owner_id` int(11) unsigned NOT NULL,
  `topic_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned,

  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `topic_id` (`topic_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `post_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`),
  CONSTRAINT `post_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `post_text`
-- id is post.id to enforce 1:1 relationship
--
DROP TABLE IF EXISTS `post_text`;
CREATE TABLE `post_text` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(255) NOT NULL DEFAULT '-',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT 0,

  `owner_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  UNIQUE KEY `post_id` (`post_id`),
  CONSTRAINT `post_text_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  CONSTRAINT `post_text_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `post_link`
-- id = post.id (enforce 1:1)
--
DROP TABLE IF EXISTS `post_link`;
CREATE TABLE `post_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) NOT NULL DEFAULT '-',
  `domain` varchar(30) NOT NULL,
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `owner_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_link_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  CONSTRAINT `post_link_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `post_photo`
--
DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `extension` enum('gif','jpg','jpeg','png') NOT NULL,
  `size` float unsigned NOT NULL DEFAULT '0.0',
  `width` smallint(6) unsigned NOT NULL,
  `height` smallint(6) unsigned NOT NULL,
  `caption` varchar(100) NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_photo_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `post_photo_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `comment`
--
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned,
  `text` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `like_count` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `child_count` smallint(6) unsigned NOT NULL DEFAULT 0,
  `report_count` smallint(6) unsigned NOT NULL DEFAULT 0,
  `pop_score` float unsigned NOT NULL DEFAULT 0.0,
  `owner_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `owner_id` (`owner_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `collection`
--
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `picture` varchar(255) NOT NULL DEFAULT '-',
  `post_count` smallint(6) NOT NULL DEFAULT 0,
  `follower_count` smallint(6) NOT NULL DEFAULT 0,
  `is_private` tinyint(1) NOT NULL DEFAULT 0,
  `owner_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `collection_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `report`
--
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) unsigned NOT NULL,
  `content_id` int(11) unsigned NOT NULL,
  `content_type` varchar(10) NOT NULL,
  `is_content_alive` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `report_count` smallint(6) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `report_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `report_category`
--
DROP TABLE IF EXISTS `report_category`;
CREATE TABLE `report_category` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(50) NOT NULL DEFAULT '-',
  `parent_id` tinyint(4) unsigned,
  `weight` tinyint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `report_category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `report_category` (`id`)
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- ------------------------------------------------------------------------------------------------------------
-- RELATION
-- ------------------------------------------------------------------------------------------------------------

--
-- Table structure for table `post_collection_rel`
--
DROP TABLE IF EXISTS `post_collection_rel`;
CREATE TABLE `post_collection_rel` (
  `post_id` int(11) unsigned NOT NULL,
  `collection_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`post_id`,`collection_id`),
  CONSTRAINT `post_collection_rel_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_collection_rel_ibfk_2` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_save_post`
--
DROP TABLE IF EXISTS `user_save_post`;
CREATE TABLE `user_save_post` (
  `user_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,
  `collection_id` int(11) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`user_id`, `post_id`),
  KEY `collection_id` (`collection_id`),
  CONSTRAINT `user_save_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_save_post_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_save_post_ibfk_3` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_follow_topic`
--
DROP TABLE IF EXISTS `user_follow_topic`;
CREATE TABLE `user_follow_topic` (
  `user_id` int(11) unsigned NOT NULL,
  `topic_id` int(11) unsigned NOT NULL,
  `is_notified` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`user_id`,`topic_id`),
  CONSTRAINT `user_follow_topic_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_follow_topic_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_follow_user`
--
DROP TABLE IF EXISTS `user_follow_user`;
CREATE TABLE `user_follow_user` (
  `follower_id` int(11) unsigned NOT NULL,
  `followed_id` int(11) unsigned NOT NULL,
  `is_notified` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`follower_id`,`followed_id`),
  CONSTRAINT `user_follow_user_ibfk_1` FOREIGN KEY (`follower_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_follow_user_ibfk_2` FOREIGN KEY (`followed_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_hide_comment`
--
DROP TABLE IF EXISTS `user_hide_comment`;
CREATE TABLE `user_hide_comment` (
  `user_id` int(11) unsigned NOT NULL,
  `comment_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`user_id`,`comment_id`),
  CONSTRAINT `user_hide_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_hide_comment_ibfk_2` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_hide_post`
--
DROP TABLE IF EXISTS `user_hide_post`;
CREATE TABLE `user_hide_post` (
  `user_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`user_id`,`post_id`),
  CONSTRAINT `user_hide_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_hide_post_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_like_comment`
--
DROP TABLE IF EXISTS `user_like_comment`;
CREATE TABLE `user_like_comment` (
  `user_id` int(11) unsigned NOT NULL,
  `comment_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`user_id`,`comment_id`),
  CONSTRAINT `user_like_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_like_comment_ibfk_2` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_like_post`
--
DROP TABLE IF EXISTS `user_like_post`;
CREATE TABLE `user_like_post` (
  `user_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`user_id`,`post_id`),
  CONSTRAINT `user_like_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_like_post_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_share_post`
--
DROP TABLE IF EXISTS `user_share_post`;
CREATE TABLE `user_share_post` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,
  `via_post_id` int(11) unsigned NOT NULL,
  `via_user_id` int(11) unsigned NOT NULL,
  `in_post_id` int(11) unsigned NOT NULL,
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `user_share_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_share_post_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_subscribe_post`
--
DROP TABLE IF EXISTS `user_subscribe_post`;
CREATE TABLE `user_subscribe_post` (
  `user_id` int(11) unsigned NOT NULL,
  `post_id` int(11) unsigned NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,

  PRIMARY KEY (`user_id`, `post_id`),
  CONSTRAINT `user_subscribe_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_subscribe_post_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_use_topic`
--
DROP TABLE IF EXISTS `user_use_topic`;
CREATE TABLE `user_use_topic` (
  `user_id` int(11) unsigned NOT NULL,
  `topic_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`user_id`,`topic_id`),
  CONSTRAINT `user_use_topic_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_use_topic_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `user_use_tag`
--
DROP TABLE IF EXISTS `user_use_tag`;
CREATE TABLE `user_use_tag` (
  `user_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`user_id`,`tag_id`),
  CONSTRAINT `user_use_tag_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_use_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `post_tag_rel`
--
DROP TABLE IF EXISTS `post_tag_rel`;
CREATE TABLE `post_tag_rel` (
  `post_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`post_id`,`tag_id`),
  CONSTRAINT `post_tag_rel_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_tag_rel_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `topic_tag_rel`
--
DROP TABLE IF EXISTS `topic_tag_rel`;
CREATE TABLE `topic_tag_rel` (
  `topic_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,

  PRIMARY KEY (`topic_id`,`tag_id`),
  CONSTRAINT `topic_tag_rel_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `topic_tag_rel_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Table structure for table `report_category_rel`
--
DROP TABLE IF EXISTS `report_category_rel`;
CREATE TABLE `report_category_rel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` int(11) unsigned NOT NULL,
  `category_id` tinyint(4) unsigned NOT NULL,
  `other_reason` varchar(100) NOT NULL DEFAULT '-',
  `user_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `report_category_rel_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `report_category_rel_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `report_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `report_category_rel_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- ------------------------------------------------------------------------------------------------------------
-- DUMP DATA
-- ------------------------------------------------------------------------------------------------------------

--
-- Dumping data for table `report_category`
--

LOCK TABLES `report_category` WRITE;
INSERT INTO `report_category` VALUES (1,'It\'s annoying or not interesting','post, comment',NULL,0),(2,'It\'s rude, vulgar or uses bad language','post, comment',NULL,0),(3,'It\'s harassment or hate speech','post, comment',NULL,0),(4,'It\'s threatening, violence or harmful behavior','post, comment',NULL,0),(5,'It\'s sexually explicit content','post, comment',NULL,0),(6,'It\'s spam or scam','post, comment',NULL,0),(7,'I\'m in this photo and I don\'t like it','post, comment, photo',NULL,0),(8,'I think this photo shouldn\'t be posted','post, comment, photo',NULL,0),(9,'This account is a fake','account',NULL,0),(10,'This account is annoying','account',NULL,0),(11,'This account is pretending to be me or someone I know','account',NULL,0),(12,'This account\'s posts are full of inappropriate content','account',NULL,0),(13,'This account represents a business or organization','account',NULL,0),(14,'This account might be compromised or hacked','post, comment, account',NULL,0),(15,'This account is abusive','post, comment, account',NULL,0),(16,'Other','post, comment, account',NULL,0);
UNLOCK TABLES;
