/* SQLEditor (Postgres)*/
/*
SMALLINT -32768 to +32767
INT -2147483648 to +2147483647
BIGINT -9223372036854775808 to +9223372036854775807
*/

CREATE TYPE "GENDER" AS ENUM ('male', 'female', 'other');
CREATE TYPE "ACCOUNT_STATUS" AS ENUM ('inactive', 'active', 'cancelled', 'suspended', 'deleted');
CREATE TYPE "POST_STATUS" AS ENUM ('publish', 'pending', 'draft','future','private','inherit','trash');
CREATE TYPE "PHOTO_EXTENSION" AS ENUM ('jpg', 'jpeg', 'gif','png');
CREATE TYPE "INVITATION_STATUS" AS ENUM ('requested', 'invited', 'joined');

CREATE TABLE "user"
(
  "id" BIGSERIAL,
  "email" VARCHAR(50) NOT NULL UNIQUE,
  "username" VARCHAR(30) NOT NULL UNIQUE,
  "password" CHAR(32) NOT NULL,
  "status" "ACCOUNT_STATUS" NOT NULL DEFAULT 'inactive',
  "follower_count" INT NOT NULL DEFAULT 0,
  "following_count" INT NOT NULL DEFAULT 0,
  "post_count" INT NOT NULL DEFAULT 0,
  "comment_count" SMALLINT NOT NULL DEFAULT 0,
  "save_count" INT NOT NULL DEFAULT 0,
  "collection_count" SMALLINT NOT NULL DEFAULT 0,
  "share_count" INT NOT NULL DEFAULT 0,
  "invite_quota" SMALLINT NOT NULL DEFAULT 0,
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),
  "password_changed_at" TIMESTAMP,
  "login_at" TIMESTAMP,
  "activated_at" TIMESTAMP,
  "suspended_at" TIMESTAMP,
  "cancelled_at" TIMESTAMP,

  PRIMARY KEY("id"),
  CHECK ("post_count" >= 0),
  CHECK ("follower_count" >= 0),
  CHECK ("following_count" >= 0),
  CHECK ("comment_count" >= 0),
  CHECK ("save_count" >= 0),
  CHECK ("collection_count" >= 0),
  CHECK ("share_count" >= 0),
  CHECK ("invite_quota" >= 0)
);

CREATE TABLE "user_detail"
(
  "id" BIGINT,
  "gender" "GENDER" NOT NULL DEFAULT 'other',
  "birthday" DATE,
  "picture" VARCHAR(255),
  "full_name" VARCHAR(50),
  "bio" VARCHAR(255),
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),
  "modified_at" TIMESTAMP,

  PRIMARY KEY("id")
);

CREATE TABLE "user_token"
(
  "id" SERIAL,
  "user_id" BIGINT NOT NULL,
  "token" CHAR(32) NOT NULL UNIQUE,
  "tag" VARCHAR(50) NOT NULL,
  "email" VARCHAR(50) NOT NULL,
  "expired_at" TIMESTAMP NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "user_token_fk" FOREIGN KEY("user_id") REFERENCES "user"("id")
);

CREATE TABLE "invitation"
(
  "id" SERIAL,
  "email" VARCHAR(50) NOT NULL UNIQUE,
  "status" "INVITATION_STATUS" NOT NULL,
  "via_user_id" BIGINT NOT NULL DEFAULT 0,
  "requested_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),
  "invited_at" TIMESTAMP,
  "joined_at" TIMESTAMP,

  PRIMARY KEY("id")
);

CREATE TABLE "post"
(
  "id" BIGSERIAL,
  "owner_id" BIGINT NOT NULL,
  "parent_id" BIGINT,
  "status" "POST_STATUS" NOT NULL DEFAULT 'draft',
  "like_count" INT NOT NULL DEFAULT 0,
  "comment_count" SMALLINT NOT NULL DEFAULT 0,
  "comment_like_count" SMALLINT NOT NULL DEFAULT 0,
  "share_count" INT NOT NULL DEFAULT 0,
  "report_count" SMALLINT NOT NULL DEFAULT 0,
  "view_count" INT NOT NULL DEFAULT 0,
  "save_count" INT NOT NULL DEFAULT 0,
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),
  "published_at" TIMESTAMP,
  "trashed_at" TIMESTAMP,

  PRIMARY KEY("id"),
  CONSTRAINT "post_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "post_parent_fk" FOREIGN KEY("parent_id") REFERENCES "post"("id"),
  CHECK ("like_count" >= 0),
  CHECK ("comment_count" >= 0),
  CHECK ("comment_like_count" >= 0),
  CHECK ("share_count" >= 0),
  CHECK ("report_count" >= 0),
  CHECK ("view_count" >= 0),
  CHECK ("save_count" >= 0)
);

CREATE TABLE "comment"
(
  "id" BIGSERIAL,
  "post_id" BIGINT NOT NULL,
  "parent_id" BIGINT,
  "owner_id" BIGINT NOT NULL,
  "text" VARCHAR(500) NOT NULL,
  "like_count" SMALLINT NOT NULL DEFAULT 0,
  "child_count" SMALLINT NOT NULL DEFAULT 0,
  "report_count" SMALLINT NOT NULL DEFAULT 0,
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "comment_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "comment_post_fk" FOREIGN KEY("post_id") REFERENCES "post"("id"),
  CONSTRAINT "comment_parent_fk" FOREIGN KEY("parent_id") REFERENCES "comment"("id"),
  CHECK ("like_count" >= 0),
  CHECK ("child_count" >= 0),
  CHECK ("report_count" >= 0)
);

CREATE TABLE "photo"
(
  "id" BIGSERIAL,
  "owner_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,
  "path" VARCHAR(255) NOT NULL,
  "extension" "PHOTO_EXTENSION" NOT NULL,
  "size" FLOAT NOT NULL DEFAULT 0,
  "width" SMALLINT NOT NULL,
  "height" SMALLINT NOT NULL,
  "caption" VARCHAR(100) NOT NULL,
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "photo_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "photo_post_fk" FOREIGN KEY("post_id") REFERENCES "post"("id")
);

CREATE TABLE "link"
(
  "id" BIGSERIAL,
  "owner_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,
  "title" VARCHAR(255) NOT NULL,
  "description" VARCHAR(255),
  "url" VARCHAR(255) NOT NULL,
  "picture" VARCHAR(255),
  "domain" VARCHAR(30) NOT NULL,
  "author" VARCHAR(20),
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "link_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "link_post_fk" FOREIGN KEY("post_id") REFERENCES "post"("id")
);

CREATE TABLE "text"
(
  "id" BIGSERIAL,
  "owner_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL UNIQUE,
  "text" VARCHAR(500) NOT NULL,
  "source" VARCHAR(255),
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "text_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "text_post_fk" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "product_brand"
(
  "id" SERIAL,
  "name" VARCHAR(30) NOT NULL UNIQUE,
  "logo_path" VARCHAR(255),
  "product_count" SMALLINT DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CHECK ("product_count" >= 0)
);

CREATE TABLE "product"
(
  "id" BIGSERIAL,
  "owner_id" BIGINT NOT NULL,
  "post_id" BIGINT UNIQUE,
  "title" VARCHAR(255) NOT NULL,
  "offer_url" VARCHAR(255) NOT NULL UNIQUE,
  "image_url" VARCHAR(255) NOT NULL,
  "features" VARCHAR(6000),
  "brand_id" INT NOT NULL,
  "domain" VARCHAR(30) NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "product_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "product_post_fk" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "product_price"
(
  "id" SERIAL,
  "product_id" BIGSERIAL NOT NULL,
  "price" FLOAT NOT NULL,
  "currency" VARCHAR(3) NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "product_price_fk" FOREIGN KEY("product_id") REFERENCES "product"("id") ON DELETE CASCADE,
  CHECK ("price" >= 0)
);


CREATE TABLE "topic"
(
  "id" SERIAL,
  "name" VARCHAR(50) NOT NULL,
  "slug" VARCHAR(50) NOT NULL UNIQUE,
  "description" VARCHAR(100),
  "parent_id" INT,
  "picture" VARCHAR(255),
  "post_count" INT NOT NULL DEFAULT 0,
  "follower_count" INT NOT NULL DEFAULT 0,
  "is_locked" BOOLEAN NOT NULL DEFAULT '0',
  "owner_id" BIGINT NOT NULL,
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "topic_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "topic_parent_fk" FOREIGN KEY("parent_id") REFERENCES "topic"("id"),
  CHECK ("post_count" >= 0),
  CHECK ("follower_count" >= 0)
);

CREATE TABLE "collection"
(
  "id" SERIAL,
  "name" VARCHAR(50) NOT NULL,
  "slug" VARCHAR(50) NOT NULL,
  "description" VARCHAR(255),
  "picture" VARCHAR(255),
  "post_count" SMALLINT NOT NULL DEFAULT 0,
  "follower_count" SMALLINT NOT NULL DEFAULT 0,
  "is_private" BOOLEAN NOT NULL DEFAULT '0',
  "owner_id" BIGINT NOT NULL,
  "pop_score" NUMERIC(6,2) NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "collection_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CHECK ("post_count" >= 0),
  CHECK ("follower_count" >= 0)
);

CREATE TABLE "tag"
(
  "id" SERIAL,
  "name" VARCHAR(50) NOT NULL,
  "slug" VARCHAR(50) NOT NULL UNIQUE,
  "post_count" INT NOT NULL DEFAULT 0 CHECK (post_count >= 0),
  "parent_id" INT,
  "owner_id" BIGINT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),
  PRIMARY KEY(id),
  CONSTRAINT "tag_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CONSTRAINT "tag_parent_fk" FOREIGN KEY("parent_id") REFERENCES "tag"("id")
);

CREATE TABLE "report"
(
  "id" BIGSERIAL,
  "owner_id" BIGINT NOT NULL,
  "content_id" BIGINT NOT NULL,
  "content_type" VARCHAR(10) NOT NULL,
  "is_content_alive" BOOLEAN NOT NULL DEFAULT '0',
  "report_count" SMALLINT NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "report_owner_fk" FOREIGN KEY("owner_id") REFERENCES "user"("id"),
  CHECK ("report_count" >= 0)
);

CREATE TABLE "report_category"
(
  "id" SMALLSERIAL,
  "title" VARCHAR(100) NOT NULL,
  "tags" VARCHAR(50),
  "parent_id" SMALLINT,
  "weight" SMALLINT NOT NULL DEFAULT 0,

  PRIMARY KEY("id"),
  CONSTRAINT "report_category_parent_fk" FOREIGN KEY("parent_id") REFERENCES "report_category"("id"),
  CHECK ("weight" >= 0)
);

---
--- SECONDARY TABLES FOR MANY TO MANY RELATIONSHIP
---

CREATE TABLE "post_topic_rel"
(
  "post_id" BIGINT NOT NULL,
  "topic_id" INT NOT NULL,

  PRIMARY KEY("post_id","topic_id"),
  CONSTRAINT "post_topic_rel_fk_1" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE,
  CONSTRAINT "post_topic_rel_fk_2" FOREIGN KEY("topic_id") REFERENCES "topic"("id") ON DELETE CASCADE
);

CREATE TABLE "post_tag_rel"
(
  "post_id" BIGINT NOT NULL,
  "tag_id" INT NOT NULL,

  PRIMARY KEY("post_id","tag_id"),
  CONSTRAINT "post_tag_rel_fk_1" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE,
  CONSTRAINT "post_tag_rel_fk_2" FOREIGN KEY("tag_id") REFERENCES "tag"("id") ON DELETE CASCADE
);

CREATE TABLE "post_collection_rel"
(
  "post_id" BIGINT NOT NULL,
  "collection_id" BIGINT NOT NULL,

  PRIMARY KEY("post_id","collection_id"),
  CONSTRAINT "post_collection_rel_fk_1" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE,
  CONSTRAINT "post_collection_rel_fk_2" FOREIGN KEY("collection_id") REFERENCES "collection"("id") ON DELETE CASCADE
);

-- CREATE TABLE "topic_tag_rel"
-- (
--   "topic_id" INT NOT NULL,
--   "tag_id" INT NOT NULL,
--
--   PRIMARY KEY("topic_id","tag_id"),
--   CONSTRAINT "topic_tag_rel_fk_1" FOREIGN KEY("topic_id") REFERENCES "topic"("id") ON DELETE CASCADE,
--   CONSTRAINT "topic_tag_rel_fk_2" FOREIGN KEY("tag_id") REFERENCES "tag"("id") ON DELETE CASCADE
-- );

CREATE TABLE "user_follow_topic"
(
  "user_id" BIGINT NOT NULL,
  "topic_id" INT NOT NULL,
  "is_notified" BOOLEAN NOT NULL DEFAULT '0',
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("user_id","topic_id"),
  CONSTRAINT "user_follow_topic_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_follow_topic_fk_2" FOREIGN KEY("topic_id") REFERENCES "topic"("id") ON DELETE CASCADE
);

CREATE TABLE "user_follow_user"
(
  "follower_id" BIGINT NOT NULL,
  "followed_id" BIGINT NOT NULL,
  "is_notified" BOOLEAN NOT NULL DEFAULT '0',
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("follower_id","followed_id"),
  CONSTRAINT "user_follow_user_fk_1" FOREIGN KEY("follower_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_follow_user_fk_2" FOREIGN KEY("followed_id") REFERENCES "user"("id") ON DELETE CASCADE
);

CREATE TABLE "user_hide_comment"
(
  "user_id" BIGINT NOT NULL,
  "comment_id" BIGINT NOT NULL,

  PRIMARY KEY("user_id","comment_id"),
  CONSTRAINT "user_hide_comment_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_hide_comment_fk_2" FOREIGN KEY("comment_id") REFERENCES "comment"("id") ON DELETE CASCADE
);

CREATE TABLE "user_hide_post"
(
  "user_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,

  PRIMARY KEY("user_id","post_id"),
  CONSTRAINT "user_hide_post_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_hide_post_fk_2" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "user_like_comment"
(
  "user_id" BIGINT NOT NULL,
  "comment_id" BIGINT NOT NULL,

  PRIMARY KEY("user_id","comment_id"),
  CONSTRAINT "user_like_comment_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_like_comment_fk_2" FOREIGN KEY("comment_id") REFERENCES "comment"("id") ON DELETE CASCADE
);

CREATE TABLE "user_like_post"
(
  "user_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("user_id","post_id"),
  CONSTRAINT "user_like_post_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_like_post_fk_2" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "user_save_post"
(
  "user_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,
  "collection_id" INT NOT NULL DEFAULT 0,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("user_id","post_id"),
  CONSTRAINT "user_save_post_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_save_post_fk_2" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "user_share_post"
(
  "id" BIGSERIAL,
  "user_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,
  "via_post_id" BIGINT NOT NULL,
  "via_user_id" BIGINT NOT NULL,
  "in_post_id" BIGINT NOT NULL,
  "comment" VARCHAR(500),
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "user_share_post_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_share_post_fk_2" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "user_subscribe_post"
(
  "user_id" BIGINT NOT NULL,
  "post_id" BIGINT NOT NULL,
  "is_active" BOOLEAN NOT NULL DEFAULT '1',

  PRIMARY KEY("user_id","post_id"),
  CONSTRAINT "user_subscribe_post_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_subscribe_post_fk_2" FOREIGN KEY("post_id") REFERENCES "post"("id") ON DELETE CASCADE
);

CREATE TABLE "user_use_topic"
(
  "user_id" BIGINT NOT NULL,
  "topic_id" INT NOT NULL,

  PRIMARY KEY("user_id","topic_id"),
  CONSTRAINT "user_use_topic_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_use_topic_fk_2" FOREIGN KEY("topic_id") REFERENCES "topic"("id") ON DELETE CASCADE
);

CREATE TABLE "user_use_tag"
(
  "user_id" BIGINT NOT NULL,
  "tag_id" INT NOT NULL,

  CONSTRAINT "user_use_tag_pk" PRIMARY KEY("user_id","tag_id"),
  CONSTRAINT "user_use_tag_fk_1" FOREIGN KEY("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
  CONSTRAINT "user_use_tag_fk_2" FOREIGN KEY("tag_id") REFERENCES "tag"("id") ON DELETE CASCADE
);

CREATE TABLE "report_category_rel"
(
  "id" SERIAL,
  "report_id" INT NOT NULL,
  "category_id" SMALLINT NOT NULL,
  "other_reason" VARCHAR(100),
  "user_id" BIGINT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),

  PRIMARY KEY("id"),
  CONSTRAINT "report_category_rel_fk_1" FOREIGN KEY("report_id") REFERENCES "report"("id") ON DELETE CASCADE,
  CONSTRAINT "report_category_rel_fk_2" FOREIGN KEY("category_id") REFERENCES "report_category"("id") ON DELETE CASCADE,
  CONSTRAINT "report_category_rel_fk_3" FOREIGN KEY("user_id") REFERENCES "user"("id")
);


-- INDEX
CREATE INDEX collection_owner_id_idx ON "collection"("owner_id");
CREATE INDEX comment_post_id_idx ON "comment"("post_id");
CREATE INDEX comment_parent_id_idx ON "comment"("parent_id");
CREATE INDEX comment_owner_id_idx ON "comment"("owner_id");
CREATE INDEX invitation_via_user_id_idx ON "invitation"("via_user_id");
CREATE INDEX photo_owner_id_idx ON "photo"("owner_id");
CREATE INDEX photo_post_id_idx ON "photo"("post_id");
CREATE INDEX post_owner_id_idx ON "post"("owner_id");
CREATE INDEX post_parent_id_idx ON "post"("parent_id");
CREATE INDEX post_link_owner_id_idx ON "link"("owner_id");
CREATE INDEX post_link_post_id_idx ON "link"("post_id");
CREATE INDEX post_text_owner_id_idx ON "text"("owner_id");
CREATE INDEX report_owner_id_idx ON "report"("owner_id");
CREATE INDEX tag_parent_id_idx ON "tag"("parent_id");
CREATE INDEX tag_owner_id_idx ON "tag"("owner_id");
CREATE INDEX topic_parent_id_idx ON "topic"("parent_id");
CREATE INDEX topic_owner_id_idx ON "topic"("owner_id");
CREATE INDEX user_token_user_id_idx ON "user_token"("user_id");
CREATE INDEX user_save_post_collection_id_idx ON "user_save_post"("collection_id");
CREATE INDEX report_category_rel_user_id_idx ON "report_category_rel"("user_id");




-- CREATE INDEX post_collection_rel_collection_id_idx ON post_collection_rel(collection_id);
-- ALTER TABLE post_collection_rel ADD CONSTRAINT post_collection_rel_ibfk_1 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE post_collection_rel ADD CONSTRAINT post_collection_rel_ibfk_2 FOREIGN KEY (collection_id) REFERENCES collection (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX report_category_parent_id_idx ON report_category(parent_id);
-- ALTER TABLE report_category ADD CONSTRAINT report_category_ibfk_1 FOREIGN KEY (parent_id) REFERENCES report_category (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- CREATE INDEX post_tag_rel_tag_id_idx ON post_tag_rel(tag_id);
-- ALTER TABLE post_tag_rel ADD CONSTRAINT post_tag_rel_ibfk_1 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE post_tag_rel ADD CONSTRAINT post_tag_rel_ibfk_2 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX topic_tag_rel_tag_id_idx ON topic_tag_rel(tag_id);
-- ALTER TABLE topic_tag_rel ADD CONSTRAINT topic_tag_rel_ibfk_1 FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE topic_tag_rel ADD CONSTRAINT topic_tag_rel_ibfk_2 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE ON UPDATE CASCADE;


-- ALTER TABLE collection ADD CONSTRAINT collection_ibfk_1 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ALTER TABLE comment ADD CONSTRAINT comment_ibfk_1 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE comment ADD CONSTRAINT comment_ibfk_3 FOREIGN KEY (parent_id) REFERENCES comment (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE comment ADD CONSTRAINT comment_ibfk_2 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- ALTER TABLE invitation ADD CONSTRAINT user_invitation_ibfk_1 FOREIGN KEY (via_user_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- ALTER TABLE photo ADD CONSTRAINT post_photo_ibfk_1 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT
-- ALTER TABLE photo ADD CONSTRAINT post_photo_ibfk_2 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ALTER TABLE post ADD CONSTRAINT post_ibfk_1 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE post ADD CONSTRAINT post_ibfk_2 FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE post ADD CONSTRAINT post_ibfk_3 FOREIGN KEY (parent_id) REFERENCES post (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ALTER TABLE post_link ADD CONSTRAINT post_link_ibfk_2 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE post_link ADD CONSTRAINT post_link_ibfk_1 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- ALTER TABLE post_text ADD CONSTRAINT post_text_ibfk_2 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE post_text ADD CONSTRAINT post_text_ibfk_1 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- ALTER TABLE report ADD CONSTRAINT report_ibfk_1 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- CREATE INDEX report_category_rel_report_id_idx ON report_category_rel(report_id);
-- CREATE INDEX report_category_rel_category_id_idx ON report_category_rel(category_id);

-- ALTER TABLE report_category_rel ADD CONSTRAINT report_category_rel_ibfk_1 FOREIGN KEY (report_id) REFERENCES report (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE report_category_rel ADD CONSTRAINT report_category_rel_ibfk_2 FOREIGN KEY (category_id) REFERENCES report_category (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE report_category_rel ADD CONSTRAINT report_category_rel_ibfk_3 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;


-- ALTER TABLE tag ADD CONSTRAINT tag_ibfk_2 FOREIGN KEY (parent_id) REFERENCES tag (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE tag ADD CONSTRAINT tag_ibfk_1 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- ALTER TABLE topic ADD CONSTRAINT topic_ibfk_2 FOREIGN KEY (parent_id) REFERENCES topic (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ALTER TABLE topic ADD CONSTRAINT topic_ibfk_1 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ALTER TABLE user_detail ADD CONSTRAINT user_detail_ibfk_1 FOREIGN KEY (id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- CREATE INDEX user_follow_topic_topic_id_idx ON user_follow_topic(topic_id);
-- ALTER TABLE user_follow_topic ADD CONSTRAINT user_follow_topic_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_follow_topic ADD CONSTRAINT user_follow_topic_ibfk_2 FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_follow_user_followed_id_idx ON user_follow_user(followed_id);
-- ALTER TABLE user_follow_user ADD CONSTRAINT user_follow_user_ibfk_1 FOREIGN KEY (follower_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_follow_user ADD CONSTRAINT user_follow_user_ibfk_2 FOREIGN KEY (followed_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_hide_comment_comment_id_idx ON user_hide_comment(comment_id);
-- ALTER TABLE user_hide_comment ADD CONSTRAINT user_hide_comment_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_hide_comment ADD CONSTRAINT user_hide_comment_ibfk_2 FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_hide_post_post_id_idx ON user_hide_post(post_id);
-- ALTER TABLE user_hide_post ADD CONSTRAINT user_hide_post_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_hide_post ADD CONSTRAINT user_hide_post_ibfk_2 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_like_comment_comment_id_idx ON user_like_comment(comment_id);
-- ALTER TABLE user_like_comment ADD CONSTRAINT user_like_comment_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_like_comment ADD CONSTRAINT user_like_comment_ibfk_2 FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_like_post_post_id_idx ON user_like_post(post_id);
-- ALTER TABLE user_like_post ADD CONSTRAINT user_like_post_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_like_post ADD CONSTRAINT user_like_post_ibfk_2 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_save_post_post_id_idx ON user_save_post(post_id);

-- ALTER TABLE user_save_post ADD CONSTRAINT user_save_post_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_save_post ADD CONSTRAINT user_save_post_ibfk_2 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_save_post ADD CONSTRAINT user_save_post_ibfk_3 FOREIGN KEY (collection_id) REFERENCES collection (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_share_post_user_id_idx ON user_share_post(user_id);
-- CREATE INDEX user_share_post_post_id_idx ON user_share_post(post_id);
-- ALTER TABLE user_share_post ADD CONSTRAINT user_share_post_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_share_post ADD CONSTRAINT user_share_post_ibfk_2 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_subscribe_post_post_id_idx ON user_subscribe_post(post_id);
-- ALTER TABLE user_subscribe_post ADD CONSTRAINT user_subscribe_post_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_subscribe_post ADD CONSTRAINT user_subscribe_post_ibfk_2 FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE;


-- ALTER TABLE user_token ADD CONSTRAINT user_token_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- CREATE INDEX user_use_tag_tag_id_idx ON user_use_tag(tag_id);
-- ALTER TABLE user_use_tag ADD CONSTRAINT user_use_tag_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_use_tag ADD CONSTRAINT user_use_tag_ibfk_2 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX user_use_topic_topic_id_idx ON user_use_topic(topic_id);
-- ALTER TABLE user_use_topic ADD CONSTRAINT user_use_topic_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE user_use_topic ADD CONSTRAINT user_use_topic_ibfk_2 FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE CASCADE ON UPDATE CASCADE;
