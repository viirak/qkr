import valideer as v
import peewee as pv
import qkr.validator as qv
import qkr.model as m

from tornado.web import RequestHandler
from qkr.handler.request import BaseRequest, UserRequest


class Valid(RequestHandler):
    def initialize(self, target):
        self.target = target

    def post(self):
        if self.target == 'password':
            validator = qv.Password(min_length=8, min_upper=0, min_digits=0)
        elif self.target == 'email':
            validator = qv.ValidEmail()
        elif self.target == 'name':
            validator = qv.ValidName(min_length=2)
        else:
            validator = 'string'
        try:
            v.parse({
                '+'+self.target: validator
            }).validate({self.target: self.get_argument(self.target)})
        except v.ValidationError, e:
            return self.write({'status': 'Error', 'msg': e.msg})
        self.write({'status': 'Ok', 'msg': ''})


class Unique(RequestHandler):

    def initialize(self, target):
        self.target = target

    def post(self):
        if self.target == 'username':
            validator = qv.UniqueUsername()
        elif self.target == 'email':
            validator = qv.UniqueEmail()
        else:
            validator = 'string'
        try:
            v.parse({
                '+'+self.target: validator
            }).validate({self.target: self.get_argument(self.target)})
        except v.ValidationError, e:
            return self.write({'status': 'Error', 'msg': e.msg})
        self.write({'status': 'Ok', 'msg': ''})


class Correct(UserRequest):
    """ check correct """

    def initialize(self, target):
        self.target = target

    def post(self):
        if self.target == 'password':
            password = self.get_argument('password')
            if not self.user.check_password(password):
                return self.write({'status': 'Error', 'msg': 'password is incorrect'})
            self.write({'status': 'Ok', 'msg': 'password is correct'})
