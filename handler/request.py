import re
import pickle
import tornado
from tornado.escape import to_unicode
from tornado import web, escape
import qkr.model as m
from tornado.web import RequestHandler as Request, HTTPError


class BaseRequest(Request):

    def prepare(self):
        fn = self.get_flash('notice')
        self.flash = fn if fn else None

    def get_current_user(self):
        return self.get_secure_cookie("user")

    def write_error(self, status_code, **kwargs):
        if status_code in [403, 404, 500]:
            self.render(str(status_code) + '.html')
        else:
            self.write('ERROR:'+str(status_code))

    # Extends Tornado's RequestHandler by adding flash functionality.
    def _cookie_name(self, key):
        return key + '_flash_cookie' # change this to store/retrieve flash
                                    # cookies under a different name

    def _get_flash_cookie(self, key):
        return self.get_secure_cookie(self._cookie_name(key))

    def has_flash(self, key):
        """
        Returns true if a flash cookie exists with a given key (string);
        false otherwise.
        """
        return self._get_flash_cookie(key) is not None

    def get_flash(self, key):
        """
        Returns the flash cookie under a given key after converting the
        cookie data into a Flash object.
        """
        if not self.has_flash(key):
            return None
        flash = tornado.escape.url_unescape(self._get_flash_cookie(key))
        try:
            flash_data = pickle.loads(flash)
            self.clear_cookie(self._cookie_name(key))
            return flash_data
        except:
            return None

    def set_flash(self, flash, key='error'):
        """
        Stores a Flash object as a flash cookie under a given key.
        """
        flash = pickle.dumps(flash)
        self.set_secure_cookie(self._cookie_name(key), tornado.escape.url_escape(flash))


class UserRequest(BaseRequest):

    def prepare(self):
        if not self.current_user:
            raise HTTPError(403)
        user = m.User.select().where(m.User.username == self.current_user).get()
        fn = self.get_flash('notice')
        self.flash = fn if fn else None
        self.user = user

class VisitorRequest(BaseRequest):

    def prepare(self):
        if self.current_user:
            raise HTTPError(404)
        fn = self.get_flash('notice')
        self.flash = fn if fn else None

class Flash(object):
    """
    A flash message along with optional (form) data.
    """

    def __init__(self, message, data=None):
        """
        'message': A string.
        'data': Can be anything.
        """
        self.message = message
        self.data = data
