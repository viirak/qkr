import datetime
import peewee as pv
import valideer as v
import qkr.validator as qv
import qkr.model as m
import qkr.util as util

from tornado.escape import xhtml_unescape
from qkr.handler.request import BaseRequest, UserRequest, VisitorRequest
from tornado.web import HTTPError
from tornadomail.message import EmailFromTemplate

from qkr.handler.request import Flash

class Index(BaseRequest):
    """ / """
    def get(self):
        if self.current_user:
            user = m.User.get(m.User.username == self.current_user)
            try:
                user_detail = m.UserDetail.get(m.UserDetail.id == user.id)
            except pv.DoesNotExist:
                user_detail = None

            return self.render(
                'bb/index.html',
                user=user,
                user_detail=user_detail.dict(user) if user_detail else None,
                flash=self.flash
            )
        else:
            return self.render('auth/index.html', flash=self.flash)


class About(VisitorRequest):
    """ /about """

    def get(self):
        self.render('auth/about.html', flash=self.flash)


class Login(VisitorRequest):
    """ /login """

    def get(self):
        self.render('auth/login.html', flash=self.flash)

    def post(self):
        """ return nothing, but status 200
        """
        username = self.get_argument('username', None)
        password = self.get_argument('password', None)
        remember_me = True if self.get_argument('remember_me') == 'true' else False
        if not username and password:
            raise HTTPError(400, 'username and password are required.')
        is_email = True if username.find('@', 0, len(username)) > 0 else False
        try:
            u = m.User
            if is_email:
                user = u.select().where(u.email == username).get()
            else:
                user = u.select().where(u.username == username).get()
        except pv.DoesNotExist:
            raise HTTPError(204, 'username not found')
        if not user.check_password(password):
            raise HTTPError(204, 'wrong password')
        expires_days = 7 if remember_me else 1
        self.set_secure_cookie('user', user.username, expires_days=expires_days)

        # check for user token
        try:
            confirm_email_token = m.UserToken.select().where(
                m.UserToken.user == user,
                m.UserToken.tag == 'confirm_email'
            ).get()
        except pv.DoesNotExist:
            confirm_email_token = None

        if confirm_email_token:
            msg = 'Please confirm your email. If you haven\'t received anything\
             you can <a href="/#settings">update your email</a>'
            flash = Flash(msg)
            self.set_flash(flash, 'notice')

        user.login_at = datetime.datetime.utcnow()
        user.save()


class Logout(UserRequest):
    """ /logout """
    def get(self):
        self.clear_cookie('user')
        self.redirect('/login')


class ForgotPassword(VisitorRequest):
    """ /p8 """

    @property
    def mail_connection(self):
        return self.application.mail_connection

    def get(self):
        self.render('auth/p8.html')

    def post(self):
        try:
            d = v.parse({'+email': qv.ValidEmail()}).validate({'email': self.get_argument('email')})
        except v.ValidationError:
            return self.write({'status': 'Error', 'msg': 'email not valid'})
        try:
            u = m.User
            user = u.select(u.id, u.username, u.email).where(u.email == d.get('email')).get()
        except pv.DoesNotExist:
            return self.write({'status': 'Error', 'msg': 'No user found with this email'})

        ut = m.UserToken
        token = util.gen_token(32)
        token_expire_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=7 * 24 * 3600)
        user_token = None
        try:
            user_token = ut.select(ut.id, ut.user, ut.tag).where(ut.user == user, ut.tag == 'reset_password').get()
        except pv.DoesNotExist:
            pass

        if user_token:
            user_token.token = token
            user_token.expired_at = token_expire_at
            user_token.save()
        else:
            ut.create(
                user=user,
                token=token,
                expired_at=token_expire_at,
                tag='reset_password',
                email=user.email
            )

        subject = self.application.settings.get('app_name') + ' Reset Password'
        from_mail = 'noreply@'+self.application.settings.get('app_domain')
        # email token to user
        mail = EmailFromTemplate(
            subject,
            'mail/p8.html',
            params={
                'username': user.username,
                'email': user.email,
                'token': token + token,
                'app_name': self.application.settings.get('app_name')
            },
            from_email=from_mail,
            to=[user.email],
            connection=self.mail_connection
        )
        mail.send()
        return self.write({'status': 'Ok', 'msg': ''})


class ResetPassword(VisitorRequest):
    """ /p8/{key} """

    def get(self, key):
        if not len(key) == 64:
            raise HTTPError(404)
        t = m.UserToken.select().where(
            m.UserToken.token == key[:32],
            m.UserToken.tag == 'reset_password',
            m.UserToken.expired_at > datetime.datetime.utcnow())
        if not t.count() > 0:
            raise HTTPError(404)
        self.render('auth/p8_reset.html')

    def post(self, key):
        if not len(key) == 64:
            raise HTTPError(404)
        try:
            t = m.UserToken.select().where(
                m.UserToken.token == key[:32],
                m.UserToken.tag == 'reset_password',
                m.UserToken.expired_at > datetime.datetime.utcnow()).get()
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({'+password': qv.Password(min_length=8, min_upper=0, min_digits=0)}).\
                validate({'password': self.get_argument('password')})
        except v.ValidationError, e:
            return self.write({'status': 'Error', 'msg': 'Password '+e.msg})
        t.user.password = m.User.create_password(d.get('password'))
        t.user.password_changed_at = datetime.datetime.utcnow()
        t.user.save()
        t.delete_instance()
        self.write({'status': 'Ok', 'msg': 'password has been saved.'})


class Join(VisitorRequest):
    """ /join """

    @property
    def mail_connection(self):
        return self.application.mail_connection

    def get(self):
        self.render('auth/join.html', allow_join=self.application.settings.get('allow_join'), flash=self.flash)

    def post(self):
        try:
            d = v.parse({
                '+email': qv.UniqueEmail,
                '+username': qv.UniqueUsername,
                '+password': qv.Password(min_length=8, min_upper=0, min_digits=0)
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError:
            raise HTTPError(400)

        new_user = m.User.create(
            username=d.get('username'),
            email=d.get('email'),
            password=m.User.create_password(d.get('password')),
            status='inactive'
        )
        token = util.gen_token(32)
        token_expire_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=7 * 24 * 3600)
        m.UserToken.create(
            user=new_user,
            token=token,
            expired_at=token_expire_at,
            tag='confirm_email',
            email=d.get('email')
        )
        mail_subject = self.application.settings.get('app_name') + ' Account Confirmation'
        mail_from = 'noreply@'+self.application.settings.get('app_domain')
        # email to user for confirmation
        mail = EmailFromTemplate(
            mail_subject,
            'mail/join.html',
            params={
                'email': d.get('email'),
                'first_name': d.get('firstName'),
                'token': token + token,
                'app_name': self.application.settings.get('app_name')
            },
            from_email=mail_from,
            to=[d.get('email')],
            connection=self.mail_connection
        )
        mail.send()
        flash = Flash(message="Please confirm your email to complete your registration. Thank for joining us!", data=None)
        self.set_flash(flash, 'notice')
        return self.write({'status': 'Ok', 'msg': ''})


class JoinByCode(VisitorRequest):
    """ /join/{key} """

    def get(self, key):
        self.write('join by code: ' + str(key))


class ConfirmEmail(BaseRequest):
    """ /confirm_email/{key} """

    def get(self, key):
        try:
            t = m.UserToken.select().where(
                m.UserToken.token == key[:32],
                m.UserToken.tag == 'confirm_email',
                m.UserToken.expired_at > datetime.datetime.utcnow()
            ).get()
        except pv.DoesNotExist:
            raise HTTPError(404)

        # get user
        try:
            user = m.User.select().where(m.User.id == t.user).get()
        except pv.DoesNotExist:
            raise HTTPError(404)

        if user.email != t.email:
            user.email = t.email

        user.status = 'active'
        user.activated_at = datetime.datetime.utcnow()
        user.update_pop_score()
        user.save()

        t.delete_instance()

        flash = Flash(message="Your email has been confirmed. Thank you!", data=None)
        self.set_flash(flash, 'notice')

        if not self.current_user:
            self.redirect("/login")
        else:
            self.redirect("/#")


class Invite(UserRequest):
    """ /invite
    """
    def get(self):
        user = m.User.select(m.User.invite_quota).where(m.User.username == self.current_user).get()
        self.render('auth/invite.html', quota=user.invite_quota)


class Setting(UserRequest):
    """ /setting
    """
    def get(self):
        user = m.User.select().where(m.User.username == self.current_user).get()
        self.render('auth/setting.html', user=user)
