import re
import os
import json
import datetime
import peewee as pv
import valideer as v
import qkr.validator as qv
import qkr.model as m
import qkr.util as util
import qkr.handler.helper as hp
import slugify
import tldextract
import qkr.scraper as scraper
import extraction
import requests

import tornado.escape

from amazon.api import AmazonAPI, AsinNotFound
from qkr.handler.request import BaseRequest, UserRequest
from tornado.web import HTTPError
from tornadomail.message import EmailFromTemplate

from urlparse import urlparse
from qkr import config


class AuthUser(UserRequest):
    """ /bb/auth/user
    """
    def get(self):
        # try:
        #     user_detail = m.UserDetail.select().where(m.UserDetail.id == self.user.id).get()
        # except pv.DoesNotExist:
        #     user_detail = None
        # check for email await to confirm
        try:
            confirm_email = m.UserToken.select().where(
                m.UserToken.user == self.user,
                m.UserToken.tag == 'confirm_email'
            ).get()
        except pv.DoesNotExist:
            confirm_email = None

        user_dict = self.user.dict_basic()
        # user_dict['detail'] = user_detail.dict() if user_detail else None
        if confirm_email:
            user_dict['confirm_email'] = confirm_email.dict()
        user_dict['domain'] = self.application.settings.get('app_domain')
        self.write(user_dict)


class AuthUserEmail(UserRequest):
    """ /bb/auth/user/email """

    @property
    def mail_connection(self):
        return self.application.mail_connection

    def post(self):
        try:
            d=v.parse({
                '+email': qv.UniqueEmail
            }).validate({'email': self.get_argument('email')})
        except v.ValidationError:
            raise HTTPError(400)

        new_email = d.get('email', None)
        if new_email:
            token = util.gen_token(32)
            token_expire_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=7 * 24 * 3600)
            nt_old = None
            nt_new = None
            try:
                nt_old = m.UserToken.select().where(
                    m.UserToken.user == self.user,
                    m.UserToken.tag == 'confirm_email',
                    m.UserToken.email == new_email
                ).get()
            except pv.DoesNotExist:
                nt_new = m.UserToken.create(
                    user=self.user,
                    token=token,
                    expired_at=token_expire_at,
                    tag='confirm_email',
                    email=new_email
                )

            if nt_old is not None:
                nt_old.token = token
                nt_old.expired_at = token_expire_at
                nt_old.save()

            nt = nt_old if nt_old else nt_new

            mail_subject = self.application.settings.get('app_name') + ' Confirm Email'
            mail_from = 'noreply@'+self.application.settings.get('app_domain')

            # email to user for confirmation
            mail = EmailFromTemplate(
                mail_subject,
                'mail/confirm_email.html',
                params={
                    'email': new_email,
                    'first_name': self.user.username,
                    'token': token + token,
                    'app_name': self.application.settings.get('app_name')
                },
                from_email=mail_from,
                to=[new_email],
                connection=self.mail_connection
            )
            mail.send()
            self.write({'status': 'Ok', 'token': nt.dict()})

    def delete(self):
        try:
            d=v.parse({
                '+email': qv.UniqueEmail
            }).validate({'email': self.get_argument('email')})
        except v.ValidationError:
            raise HTTPError(400)

        try:
            token = m.UserToken.select().where(
                m.UserToken.user == self.user,
                m.UserToken.email == d.get('email'),
                m.UserToken.tag == 'confirm_email'
            ).get()
        except pv.DoesNotExist:
            raise HTTPError(400)

        token.delete_instance()
        self.write({'status': 'Ok', 'token': None})



class AuthUserDetail(UserRequest):
    """ /bb/auth/user/detail """

    def post(self):
        try:
            d = v.parse({
                'full_name': qv.ValidName(allow_space=True),
                'birthday': qv.DateString(format="%Y-%m-%d"),
                'gender': 'string',
                'bio': 'string'
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            raise HTTPError(400, str(e))

        full_name = d.get('full_name', None)
        gender = d.get('gender', None)
        birthday = d.get('birthday', datetime.datetime.strptime("1900-01-01", "%Y-%m-%d").date())
        bio = d.get('bio', None)

        try:
            ud = m.UserDetail.select().where(m.UserDetail.id == self.user.id).get()
        except pv.DoesNotExist:
            ud = m.UserDetail.create(id=self.user.id, created_at=datetime.datetime.utcnow())

        if full_name:
            ud.full_name = full_name
        if gender:
            ud.gender = gender
        if birthday:
            ud.birthday = birthday
        if bio:
            ud.bio = bio
        ud.save()
        self.write({'status': 'Ok', 'user_detail': ud.dict(self.user)})


class AuthUserPassword(UserRequest):
    """ /bb/auth/user/password """

    def post(self):
        try:
            d = v.parse({
                'password': qv.Password(min_length=8, min_upper=0, min_digits=0),
                'password_old': 'string'
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            raise HTTPError(400, str(e))

        if not self.user.check_password(d.get('password_old')):
            raise HTTPError(400)

        self.user.password = self.user.create_password(d.get('password'))
        self.user.password_changed_at = datetime.datetime.utcnow()
        self.user.save()
        self.write({'status': 'Ok', 'msg': 'password has been saved.'})


class AuthUserPicture(UserRequest):
    """ /bb/auth/user/picture """
    def post(self):
        file_dict = self.request.files['picture'][0]
        try:
            d = v.parse({
                '+img': qv.ImageFileDict
            }).validate({'img': file_dict})
        except v.ValidationError, e:
            raise HTTPError(400, str(e))

        img = d.get('img')

        try:
            user_detail = m.UserDetail.select().where(m.UserDetail.id == self.user.id).get()
        except pv.DoesNotExist:
            user_detail = None

        profile_image = util.QkImage(self.user, 'proimg')

        # remove existing img before save a new one...
        if user_detail and user_detail.picture:
            profile_image.remove(user_detail.picture, [48, 250, 640])

        new_user_detail = None
        img_key = profile_image.save(img, crop=True)
        # img_path_250 = 'media/'+profile_image.get_path(img_key, width=250)
        # data['picture_url_m'] = self.static_url(img_path_250, include_host=True)
        # img_path_48 = 'media/'+profile_image.get_path(img_key, width=48)
        # data['picture_url_s'] = self.static_url(img_path_48, include_host=True)

        if user_detail:
            user_detail.picture = img_key
            user_detail.save()
        else:
            new_user_detail = m.UserDetail.create(
                id=self.user.id,
                picture=img_key,
                full_name='-',
                bio='-',
                created_at=datetime.datetime.utcnow()
            )
        data = new_user_detail.dict(self.user) if new_user_detail else user_detail.dict(self.user)
        html = '<textarea data-type="application/json">'+json.dumps({'user_detail': data})+'</textarea>'
        self.write(html)


class UserUsername(UserRequest):
    """ /bb/users/username """
    def get(self, username):
        try:
            user = m.User.select().where(m.User.username == username).get()
        except pv.DoesNotExist:
            raise HTTPError(404)
        user_dict = user.dict_basic()
        is_followed = user.is_followed(self.user)
        user_dict['is_followed'] = is_followed
        if is_followed:
            x = m.UserFollowUser
            ff = x.select(x.follower,x.followed,x.is_notified).where(x.follower==self.user, x.followed==user).get()
        user_dict['is_notified'] = ff.is_notified if is_followed else None
        self.write(user_dict)


class UserFollowed(UserRequest):
    """ /bb/users/{username}/follow """
    def post(self, username):
        try:
            user = m.User.get(m.User.username == username)
        except pv.DoesNotExist:
            raise HTTPError(404)

        try:
            d = v.parse({'+follow': qv.Boolean})\
                .validate({'follow': self.get_argument('follow')})
        except v.ValidationError:
            raise HTTPError(400)
        follow = d.get('follow')
        if follow:
            m.UserFollowUser.create(
                follower=self.user,
                followed=user,
                is_notified=False,
                created_at=datetime.datetime.utcnow()
            )
            user.follower_count += 1
        else:
            x = m.UserFollowUser
            q = x.delete().where(x.follower==self.user, x.followed==user)
            q.execute()
            user.follower_count -= 1

        user.update_pop_score()
        user.save()
        self.write({'status': 'Ok', 'msg': 'user followed.'})


class UserFollowedNotif(UserRequest):
    """ /bb/users/{username}/notif """
    def post(self, username):
        try:
            user = m.User.get(m.User.username == username)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({
                '+notif': qv.Boolean
            }).validate({'notif': self.get_argument('notif')})
        except v.ValidationError:
            raise HTTPError(400)

        if not user.is_followed(self.user):
            raise HTTPError(400)

        notif = d.get('notif')
        x = m.UserFollowUser
        q = x.update(is_notified=notif).where(x.follower==self.user,x.followed==user)
        q.execute()
        self.write({'status': 'Ok', 'msg': 'user is notified.'})


class UserCollection(UserRequest):
    """ /bb/username/collections """

    def post(self, username):
        try:
            user = m.User.get(m.User.username == username)
        except pv.DoesNotExist:
            raise HTTPError(404)

        try:
            d=v.parse({
                '+name': qv.ValidName(allow_digit=True, allow_space=True),
                'private': qv.Boolean,
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            return self.write({'status': 'Error', 'msg': str(e.message)})

        name = d.get('name')
        is_private = d.get('private', False)
        collection = m.Collection.create(
            name=name,
            slug=slugify.slugify(name),
            description='-',
            is_private=is_private,
            owner=self.user,
            created_at=datetime.datetime.utcnow()
        )
        self.user.collection_count += 1
        self.user.update_pop_score()
        self.user.save()
        self.write({'status': 'Ok', 'collection': collection.dict()})

    def get(self, username):
        try:
            user = m.User.get(m.User.username == username)
        except pv.DoesNotExist:
            raise HTTPError(404)

        collections_dict = []
        x = m.Collection
        collections = x.select().where(x.owner == user).order_by(x.name.asc())
        if collections.count() > 0:
            collections_dict = [coll.dict() for coll in collections]
        return self.write({'status': 'Ok', 'collections': collections_dict})


class Topic(UserRequest):
    """ /bb/topics
    """
    def post(self):
        try:
            d = v.parse({
                '+name': qv.ValidName(allow_space=True)
                }).validate({'name': self.get_argument('name')})
        except v.ValidationError:
            raise HTTPError(400)
        name = d.get('name')
        t = m.Topic.create(
            name=name,
            slug=slugify.slugify(name),
            owner=self.user,
            created_at=datetime.datetime.utcnow()
        )
        self.write({'status': 'Ok', 'topic': t.dict()})


class TopicSuggest(UserRequest):
    """ /bb/topics/suggest
    """
    def get(self):
        try:
            d = v.parse({'+q': 'string'}).validate({'q': self.get_argument('q')})
        except v.ValidationError:
            raise HTTPError(400)
        topics = m.Topic.select().where(m.Topic.name.contains(d.get('q'))).order_by(m.Topic.name)
        suggestions = [{'value': t.name, 'data': t.id} for t in topics]
        self.write({'query': d.get('q'), 'suggestions': suggestions})


class Tag(UserRequest):
    """ /bb/tags """
    def post(self):
        try:
            d = v.parse({
                '+name': qv.ValidName(allow_space=True),
                'topic': 'string'
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError:
            raise HTTPError(400)
        name = d.get('name')
        topic_id = d.get('topic', None)
        if topic_id:
            topic = m.Topic.select().where(m.Topic.id == topic_id).get()
            if not topic:
                raise HTTPError(400)

        t = m.Tag.create(
            name=name,
            slug=slugify.slugify(name),
            owner=self.user,
            created_at=datetime.datetime.utcnow()
        )

        if topic_id:
            m.TopicTagRel.create(topic=topic, tag=t)

        self.write({'status': 'Ok', 'tag': t.dict()})


class TagSuggest(UserRequest):
    """ /bb/tag/suggest
    """
    def get(self):
        try:
            d = v.parse({'+q': 'string'}).validate({'q': self.get_argument('q')})
        except v.ValidationError:
            raise HTTPError(400)
        tags = m.Tag.select().where(m.Tag.name.contains(d.get('q'))).order_by(m.Tag.name)
        suggestions = [{'value': t.name, 'data': t.id} for t in tags]
        self.write({'query': d.get('q'), 'suggestions': suggestions})


class Post(UserRequest):
    """ /bb/posts """
    def get(self):
        try:
            d = v.parse({
                'owner': 'string',
                'type': 'string',
                'privacy': 'string',
                'topic': v.String,
                'tag': v.String,
                'page_size': v.AdaptBy(int),
                'page_num': v.AdaptBy(int),
                'sort': 'string',
                'parent': qv.Boolean,
                'saved_by': v.AdaptBy(int),
                'start_ts': v.AdaptBy(int),
                'end_ts': v.AdaptBy(int),
                'c8nid': v.AdaptBy(int)  # collection id
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError:
            raise HTTPError(403)

        posts = m.Post.select()

        where = {'status': 'publish'}

        # post type
        post_type = d.get('type', None)
        if post_type:
            where['type'] = post_type

        # filter owner
        owner_username = d.get('owner', None)
        if owner_username:
            try:
                user = m.User.select().where(m.User.username == owner_username).get()
                where['owner'] = user
            except pv.DoesNotExist:
                pass

        # filter topic
        topic_slug = d.get('topic', None)
        if topic_slug:
            try:
                topic = m.Topic.select().where(m.Topic.slug == topic_slug).get()
                where['topic'] = topic
            except pv.DoesNotExist:
                pass

        # start_ts, end_ts
        start_ts = d.get('start_ts', None)
        end_ts = d.get('end_ts', None)
        if start_ts and end_ts:
            start = datetime.datetime.fromtimestamp(start_ts)
            end = datetime.datetime.fromtimestamp(end_ts)
            posts = posts.where(m.Post.created_at > start, m.Post.created_at < end)

        # parent
        parent = d.get('parent', True)
        if not parent:
            posts = posts.where(m.Post.parent == None)

        # apply where filter
        if where:
            for key, value in where.items():
                if value is not None:
                    posts = posts.where(getattr(m.Post, key) == value)

        sort_str = d.get('sort', 'pop')
        if sort_str and sort_str == 'pop':
            posts = posts.order_by(m.Post.pop_score.desc(), m.Post.created_at.desc())
        else:
            posts = posts.order_by(m.Post.created_at.desc())

        count = posts.count()
        offset = 0

        # paginate
        page_num = d.get('page_num', 1)
        page_size = d.get('page_size', 10)

        if page_num and page_size:
            posts = posts.paginate(page_num, page_size)
            offset = page_num * page_size

        has_more = False if offset >= count else True

        d_posts = hp.dict_posts_with_comments(posts, self.user)
        self.write({'posts': d_posts, 'meta': {'total': count, 'has_more': has_more}})


class PostMenu(UserRequest):

    def get(self):
        """ generate user post menu data
        """
        # get topics user used
        # get all user posts
        topics = []
        dates = []
        types = {'knowledge': 0, 'question': 0}
        # privacy = {'private': 0, 'public': 0}
        # where = {'owner': self.user}
        # posts = mgr.PostManager().posts(no_parent=True, filters=filters)
        posts = m.Post.select().where(m.Post.owner == self.user)

        # for key, value in where.items():
        #     if value is not None:
        #         posts = posts.where(getattr(m.Post, key) == value)

        if posts.count() > 0:
            for post in posts:
                # post_topic = post.topic if post.topic else None
                # if post_topic:
                #     t = next((x for x in topics if x['topic']['slug'] == post_topic.slug), None)
                #     if t:
                #         t['post_count'] += 1
                #     else:
                #         topics.append({'topic': post_topic.dict(), 'post_count': 1})

                pd = post.created_at.date()
                y = next((x for x in dates if x['year'] == pd.year), None)
                if y:
                    y['post_count'] += 1
                else:
                    dates.append({'year': pd.year, 'months': [], 'post_count': 1})
                # loop again
                yr = next((x for x in dates if x['year'] == pd.year), None)
                if yr:
                    months = yr.get('months')
                    mon = next((x for x in months if x['name'] == pd.strftime("%B")), None)
                    if mon:
                        mon['post_count'] += 1
                    else:
                        months.append({'name': pd.strftime("%B"), 'post_count': 1})
                # if post.type == 'knowledge':
                #     types['knowledge'] += 1
                # elif post.type == 'question':
                #     types['question'] += 1
                # if post.privacy == 'private':
                #     privacy['private'] += 1
                # elif post.privacy == 'public':
                #     privacy['public'] += 1
        self.write({'topics': topics, 'dates': dates, 'types': types, 'privacy': {}})


class PostSingle(UserRequest):
    """ /bb/posts/{id} """

    def delete(self, post_id):
        q = m.Post.delete().where(m.Post.id == post_id)
        q.execute()
        self.user.post_count -= 1
        self.user.save()


class PostComment(UserRequest):
    """ /bb/posts/{id}/comment """

    def post(self, post_id):
        try:
            post = m.Post.select().where(m.Post.id == post_id).get()
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({
                '+text': 'string',
                'parent': v.AdaptBy(int)
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except pv.ValidationError:
            raise HTTPError(400)

        parent = d.get('parent', None)
        if parent:
            parent_comment = m.Comment.get(m.Comment.id == parent)
            parent_comment.child_count += 1
            parent_comment.update_pop_score()
            parent_comment.save()

        comment = m.Comment.create(
            parent=parent_comment if parent else None,
            text=d.get('text'),
            post=post,
            owner=self.user,
            created_at=datetime.datetime.utcnow()
        )
        comment.update_pop_score()
        comment.save()

        post.comment_count += 1
        post.update_pop_score()
        post.save()

        # subscribe user to this post
        x = m.UserSubscribePost
        usp = x.select(x.user,x.post,).where(x.user==self.user,x.post==post)
        if usp.count() == 0:
            x.create(user=self.user,post=post,is_active=True)

        self.user.comment_count += 1
        self.user.update_pop_score()
        self.user.save()

        self.write({'status': 'Ok', 'comment': comment.dict()})

    def get(self, post_id):
        try:
            post = m.Post.select(m.Post.id).where(m.Post.id == post_id).get()
        except pv.DoesNotExist:
            raise HTTPError(404)

        try:
            d = v.parse({
                'offset': v.AdaptBy(int),
                'page_size': v.AdaptBy(int),
                'page_num': v.AdaptBy(int),
                'parent': v.AdaptBy(int),
                'sort': 'string',
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError:
            raise HTTPError(400)

        offset = d.get('offset', 0)
        page_size = d.get('page_size', 10)
        page_num = d.get('page_num', 1)
        parent = d.get('parent', None)
        sort = d.get('sort', 'pop')

        comments = m.Comment.select()
        where = {'post': post}

        comments = comments.where(m.Comment.parent == parent)
        # apply where filter
        if where:
            for key, value in where.items():
                if value is not None:
                    comments = comments.where(getattr(m.Comment, key) == value)

        if sort == 'pop':
            comments = comments.order_by(m.Comment.pop_score.desc(), m.Comment.created_at.desc())
        else:
            comments = comments.order_by(m.Comment.created_at.desc())

        count = comments.count()

        # paginate
        if page_num and page_size:
            comments = comments.paginate(page_num, page_size)
            offset = page_num * page_size

        has_more = False if offset >= count else True

        comments_dict = []
        for com in comments:
            if not com.is_hidden(self.user):
                com_dict = com.dict()
                com_dict['is_liked'] = com.is_liked(self.user)
                if com.child_count > 0:
                    children = m.Comment\
                    .select().where(m.Comment.parent == com)\
                    .order_by(m.Comment.pop_score.desc(), m.Comment.created_at.asc())\
                    .limit(1)
                    children_dict = []
                    for child in children:
                        child_dict = child.dict()
                        child_dict['is_liked'] = child.is_liked(self.user)
                        children_dict.append(child_dict)
                    com_dict['children'] = children_dict
                comments_dict.append(com_dict)

        self.write({'comments': comments_dict, 'meta': {'total': count, 'has_more': has_more}})


# class TypePost(UserRequest):
#     """ /bb/(text|photo|link|code)/posts """
#
#     def post(self, arg):
#         if arg == 'text':
#             try:
#                 d = v.parse({
#                     '+text': 'string',
#                     'source': 'string',
#                     '+privacy': qv.QkPrivacyString,
#                     'topic': v.AdaptBy(int),
#                     'tags': qv.StringListOf(v.Integer),
#                 }).validate({k: self.get_argument(k) for k in self.request.arguments})
#             except v.ValidationError:
#                 raise HTTPError(403, log_message="validation error")  # bad request
#
#             topic_id = d.get('topic', None)
#             if topic_id:
#                 try:
#                     topic = m.Topic.select().where(m.Topic.id == topic_id).get()
#                 except pv.DoesNotExist:
#                     raise HTTPError(403, log_message="topic not found")
#
#             post = m.Post.create(
#                 status='publish',
#                 type='text',
#                 created_at=datetime.datetime.utcnow(),
#                 published_at=datetime.datetime.utcnow(),
#                 owner=self.user,
#                 # topic=topic,
#             )
#
#             m.PostText.create(
#                 text=d.get('text'),
#                 owner=self.user,
#                 post=post,
#                 source=str(d.get('source')) if d.get('source') else '-',
#                 created_at=datetime.datetime.utcnow()
#             )
#
#             tag_ids = d.get('tags', None)
#             if tag_ids:
#                 tags = m.Tag.select().where(m.Tag.id << tag_ids)
#                 if not tags:
#                     raise HTTPError(403, log_message="tags not found")
#                 for tag in tags:
#                     m.PostTagRel.create(post=post, tag=tag)
#
#             # update pop score
#             post.update_pop_score()
#             post.save()
#
#             # update counter
#             self.user.post_count += 1
#             self.user.update_pop_score()
#             self.user.save()
#
#         self.write({'status': 'Ok', 'msg': 'post created'})


class PostText(UserRequest):
    """ /bb/posts/text """

    def post(self):
        try:
            d = v.parse({
                '+text': 'string',
                'source': 'string',
                '+privacy': qv.QkPrivacyString,
                'topic': v.AdaptBy(int),
                'tags': qv.StringListOf(v.Integer),
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError:
            raise HTTPError(403, log_message="validation error")  # bad request

        topic_id = d.get('topic', None)
        if topic_id:
            try:
                topic = m.Topic.select().where(m.Topic.id == topic_id).get()
            except pv.DoesNotExist:
                raise HTTPError(403, log_message="topic not found")

        text = m.PostText.create(
            text=d.get('text'),
            owner=self.user,
            source=str(d.get('source')) if d.get('source') else '-',
            created_at=datetime.datetime.utcnow()
        )

        post = m.Post.create(
            status='publish',
            created_at=datetime.datetime.utcnow(),
            published_at=datetime.datetime.utcnow(),
            owner=self.user,
            text=text
        )

        tag_ids = d.get('tags', None)
        if tag_ids:
            tags = m.Tag.select().where(m.Tag.id << tag_ids)
            if not tags:
                raise HTTPError(403, log_message="tags not found")
            for tag in tags:
                m.PostTagRel.create(post=post, tag=tag)

        # update pop score
        post.update_pop_score()
        post.save()

        # update counter
        self.user.post_count += 1
        self.user.update_pop_score()
        self.user.save()

        self.write({'status': 'Ok', 'msg': 'post created'})


class PostProduct(UserRequest):
    """ /bb/posts/product """

    def get(self):
        try:
            d = v.parse({'+url': qv.ValidURL,}).validate({
                'url': self.get_argument('url')
            })
        except v.ValidationError:
            raise HTTPError(403, log_message="validation error")  # bad request

        # analyze url
        # using TLDExtract at https://github.com/john-kurkowski/tldextract
        url = d.get('url', None)
        cache_path = config.CACHE_PATH + '/.tld_set'
        cache_extract = tldextract.TLDExtract(cache_file=cache_path)
        ext = cache_extract(url)
        tld = ext.subdomain + '.' + ext.registered_domain if ext.subdomain else ext.registered_domain

        # amazon product
        if ext.registered_domain == 'amazon.com':
            # get asin
            match = re.search(r'(?i)(dp|product)(\/)\w+', url)
            asin = match.group().split('/')[1] if match else None
            if not asin:
                # raise HTTPError(400, log_message="bad url, not amazon product url")
                return self.write({'status':'Error', 'msg':'Not amazon product'})

            # product offer url
            product_base_url = 'http://'+tld+'/dp/'
            offer_url = "{0}{1}/?tag={2}".format(
                product_base_url,
                asin,
                config.AMAZON_ASSOC_TAG
            )

            # check if Product already been posted
            try:
                qp = m.Product.get(m.Product.offer_url == offer_url)
            except pv.DoesNotExist:
                qp = None

            if qp and qp.post:
                # response['post'] = qp.post.dict(self.user)
                # return self.write(response)
                return self.write({
                    'status':'Exist',
                    'msg':'Product has already been posted.',
                    'post': qp.post.dict(self.user)
                })

            # get product info via ProductAPI
            amazon = AmazonAPI(
                config.AMAZON_ACCESS_KEY,
                config.AMAZON_SECRET_KEY,
                config.AMAZON_ASSOC_TAG
            )
            try:
                ap = amazon.lookup(ItemId=asin)
            except AsinNotFound:
                raise HTTPError(400, log_message="ASIN not found.")

            # get brand
            brand = ap.brand
            if ap.publisher:
                brand = ap.publisher
            elif ap.manufacturer:
                brand = ap.menufacturer

            product = dict(
                title=ap.title,
                description=ap.features,
                image=ap.large_image_url,
                price=ap.price_and_currency,
                brand=brand,
                url=ap.offer_url
            )

            return self.write({'status':'Ok', 'product':product})

        else:
            # check if url has already been posted.
            # ...


            # using extract to get basic page information
            from qkr.extraction_extra import ProductExtractor
            r = requests.get(url)
            # requests.get('https://kennethreitz.com', verify=True)
            extractor = ProductExtractor()
            extracted = extractor.extract(r.text)
            # product = None

            product = dict(
                title=extracted.title,
                description=extracted.description,
                image=extracted.image,
                brand=extracted.brand,
                price=extracted.price
            )

            print "*** product: " + str(product)

            return self.write({'status':'Ok', 'product':product})

    def post(self):
        response = {'status': 'Ok'}
        try:
            d = v.parse({
                '+url': qv.ValidURL,
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError:
            raise HTTPError(403, log_message="validation error")  # bad request

        url = d.get('url', None)
        # print ">>>> " + str(url)

        # https://github.com/john-kurkowski/tldextract
        cache_path = config.CACHE_PATH + '/.tld_set'
        cache_extract = tldextract.TLDExtract(cache_file=cache_path)
        ext = cache_extract(url)
        tld = ext.subdomain + '.' + ext.registered_domain if ext.subdomain else ext.registered_domain
        # print ">>> URL: " + url
        # print ">>>> subdomain: " + str(ext.subdomain)
        # print ">>>> reg_domain: " + str(ext.registered_domain)
        # print ">>>> domain: " + str(ext.domain)
        # print ">>> tld: " + tld

        ap_dict = None
        # Amazon product url form http://amazon.com/dp/B015IWUI1K
        if ext.registered_domain == 'amazon.com':
            # get asin
            match = re.search(r'(?i)(dp|product)(\/)\w+', url)
            asin = match.group().split('/')[1] if match else None
            if not asin:
                raise HTTPError(400, log_message="bad url, not amazon product url")

            # make offer url
            product_base_url = 'http://'+tld+'/dp/'
            offer_url = "{0}{1}/?tag={2}".format(
                product_base_url,
                asin,
                config.AMAZON_ASSOC_TAG
            )
            # check offer url
            try:
                qp = m.Product.get(m.Product.offer_url == offer_url)
            except pv.DoesNotExist:
                qp = None

            if qp and qp.post:
                response['post'] = qp.post.dict(self.user)
                return self.write(response)

            # get amazon product
            amazon = AmazonAPI(config.AMAZON_ACCESS_KEY, config.AMAZON_SECRET_KEY, config.AMAZON_ASSOC_TAG)
            try:
                ap = amazon.lookup(ItemId=asin)
            except AsinNotFound:
                raise HTTPError(400, log_message="ASIN not found.")

            if ap:
                ap_dict = dict(
                    title = ap.title,
                    price = ap.price_and_currency,
                    list_price = ap.list_price,
                    sale_rank = ap.sales_rank,
                    image = ap.large_image_url,
                    offer_url = ap.offer_url,
                    features = ap.features,
                    brand = ap.brand,
                    authors = ap.authors,
                    publisher = ap.publisher,
                    manufacturer = ap.manufacturer,
                )
        elif ext.registered_domain == 'ebay.com':
            print ">>> get ebay product ..."


        # new_post = None
        if ap_dict:
            # get or create brand
            # create product
            # create price
            # create post
            # return post

            # get brand
            product_by = ap_dict.get('brand')
            if ap_dict.get('publisher'):
                product_by = ap_dict.get('publisher')
            elif ap_dict.get('manufacturer'):
                product_by = ap_dict.get('menufacturer')
            try:
                brand = m.Brand.select().where(m.Brand.name == product_by).get()
            except pv.DoesNotExist:
                brand = m.Brand.create(name = product_by)

            # product
            p_feat = ap_dict.get('features')

            try:
                product = m.Product.select().where(m.Product.offer_url == ap_dict.get('offer_url')).get()
            except pv.DoesNotExist:
                # download and save product image
                product_image = util.QkImage(self.user, 'productimg')
                img_key = product_image.download_and_save(ap_dict.get('image'))
                # create new product
                product = m.Product.create(
                    title = ap_dict.get('title'),
                    offer_url = ap_dict.get('offer_url'),
                    image_url = img_key,
                    features = p_feat if len(p_feat) > 0 else None,
                    domain = ext.registered_domain,
                    brand = brand,
                    owner = self.user
                )
            # price
            price = ap_dict.get('price')[0]
            currency = ap_dict.get('price')[1]
            price = m.ProductPrice.select().where(
                m.ProductPrice.product == product,
                m.ProductPrice.price == price,
                m.ProductPrice.currency == currency
            )
            if not price.count() > 0:
                price = m.ProductPrice.create(
                    price = ap_dict.get('price')[0],
                    currency = ap_dict.get('price')[1],
                    product = product
                )

            brand.product_count += 1
            brand.save()

            try:
                post = product.post
            except pv.DoesNotExist:
                post = m.Post.create(
                    status='publish',
                    created_at=datetime.datetime.utcnow(),
                    published_at=datetime.datetime.utcnow(),
                    owner=self.user,
                    product=product
                )
                product.post = post
                product.save()
                self.user.post_count += 1
                self.user.save()

            d = post.dict(self.user) if post else None
            response['post'] = d
        self.write(response)


class PostLike(UserRequest):
    """ /bb/posts/{id}/like """

    def post(self, post_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({'like': qv.Boolean})\
                .validate({'like': self.get_argument('like')})
        except v.ValidationError:
            raise HTTPError(400)
        like = d.get('like')
        if like:
            user_like_post = m.UserLikePost.create(
                user=self.user,
                post=post,
                created_at=datetime.datetime.utcnow()
            )
            post.like_count += 1
        else:
            x = m.UserLikePost
            q = x.delete().where(x.user == self.user, x.post == post)
            q.execute()
            post.like_count -= 1

        post.update_pop_score()
        post.save()
        self.write({'status': 'Ok', 'msg': 'like saved'})


class PostHide(UserRequest):
    """ /bb/posts/{id}/hide """

    def post(self, post_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({'hide': qv.Boolean})\
                .validate({'hide': self.get_argument('hide')})
        except v.ValidationError:
            raise HTTPError(400)
        hide = d.get('hide')
        if hide:
            m.UserHidePost.create(user=self.user,post=post)
        else:
            x = m.UserHidePost
            q = x.delete().where(x.user == self.user, x.post == post)
            q.execute()

        self.write({'status': 'Ok', 'msg': 'Post is hidden'})


class PostSubscribe(UserRequest):
    """ /bb/posts/{id}/subscribe """

    def post(self, post_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({'subscribe': qv.Boolean})\
                .validate({'subscribe': self.get_argument('subscribe')})
        except v.ValidationError:
            raise HTTPError(400)
        subscribe = d.get('subscribe')

        x = m.UserSubscribePost
        if subscribe:
            usp = x.select(x.user,x.post).where(x.user==self.user,x.post==post)
            if usp.count() == 0:
                x.create(user=self.user,post=post,is_active=True)
            else:
                q = x.update(is_active=True).where(x.user==self.user,x.post==post)
                q.execute()
        else:
            q = x.update(is_active=False).where(x.user==self.user,x.post==post)
            q.execute()

        self.write({'status': 'Ok', 'msg': 'Post is subscribed.'})


class PostSave(UserRequest):
    """ /bb/posts/{id}/save """

    def post(self, post_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({
                '+save': qv.Boolean,
                'collection_id': v.AdaptBy(int)
                }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            return self.write({'status': 'Error', 'msg': str(e.message)})

        save = d.get('save')
        x = m.UserSavePost
        qq = x.select(x.user,x.post).where(x.user == self.user,x.post == post)
        if save:
            collection_id = d.get('collection_id', None)
            if not collection_id:
                raise HTTPError(400)
            try:
                collection = m.Collection.get(
                    m.Collection.id == collection_id,
                    m.Collection.owner == self.user
                )
            except pv.DoesNotExist:
                raise HTTPError(400)

            if qq.count() == 0:
                user_save_post = x.create(
                    user=self.user,
                    post=post,
                    collection=collection,
                    created_at=datetime.datetime.utcnow()
                )
                post.save_count += 1

                post_in_collection = m.PostCollectionRel.create(
                    post = post,
                    collection = collection
                )
                collection.post_count += 1
                collection.save()

        else:
            if qq.count() > 0:
                q = x.delete().where(x.user == self.user, x.post == post)
                q.execute()
                post.save_count -= 1

                y = m.PostCollectionRel
                post_in_collections = y.select(y.post, y.collection).where(y.post == post)
                if post_in_collections.count() > 0:
                    for pic in post_in_collections:
                        pic.collection.post_count -= 1
                        pic.collection.save()
                        pic_query = y.delete().where(y.post == post, y.collection == pic.collection)
                        pic_query.execute()

        post.update_pop_score()
        post.save()
        self.user.save_count += 1
        self.user.update_pop_score()
        self.user.save()
        self.write({'status': 'Ok', 'msg': 'saved'})


class PostShare(UserRequest):
    """ /bb/posts/{id}/share """

    def post(self, post_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({
                'comment': 'string'
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            return self.write({'status': 'Error', 'msg': str(e.message)})

        comment = d.get('comment', '-')
        post_to_share = post.parent if post.parent else post

        # share to prifle stream
        share_post = m.Post.create(
            status='publish',
            type='text',
            created_at=datetime.datetime.utcnow(),
            published_at=datetime.datetime.utcnow(),
            owner=self.user,
            # topic=post_to_share.topic,
            parent=post_to_share
        )

        if share_post:
            if comment != '-':
                m.PostText.create(
                    text=comment,
                    owner=self.user,
                    post=share_post,
                    source='-',
                    created_at=datetime.datetime.utcnow()
                )

            m.UserSharePost.create(
                user=self.user,
                post=post,
                via_user_id=post.owner.id,
                via_post_id=post.id,
                in_post_id=share_post.id,
                comment=comment,
                created_at=datetime.datetime.utcnow()
            )

        if share_post:
            share_post.update_pop_score()
            share_post.save()

            post.share_count += 1
            post.update_pop_score()
            post.save()

            if post.parent:
                post.parent.share_count += 1
                post.parent.update_pop_score()
                post.parent.save()

            self.user.share_count += 1
            self.user.update_pop_score()
            self.user.save()

            self.write({'status': 'Ok', 'msg': 'post shared'})


class PostReport(UserRequest):
    """ /bb/posts/{id}/report """

    def post(self, post_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({
                '+opt': v.AdaptBy(int)
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            raise HTTPError(400)

        opt = d.get('opt')
        try:
            category = m.ReportCategory.get(m.ReportCategory.id == opt)
        except pv.DoesNotExist:
            raise HTTPerror(400)

        try:
            report = m.Report.get(
                m.Report.content_id == post.id,
                m.Report.content_type == 'post'
            )
        except pv.DoesNotExist:
            report = None

        if not report:
            new_report = m.Report.create(
                content_type='post',
                content_id=post.id,
                owner=self.user,
                created_at=datetime.datetime.utcnow()
            )

        r = report if report else new_report
        m.ReportCategoryRel.create(
            report=r,
            category=category,
            user=self.user,
            other_reason='-',
            created_at=datetime.datetime.utcnow()
        )

        r.report_count += 1
        r.save()
        post.report_count += 1
        # post.update_pop_score() # unless this report is approved
        post.save()
        self.write({'status': 'Ok', 'msg': 'report saved'})


class CommentSingle(UserRequest):
    """ /bb/posts/{id}/comments/{id} """

    def delete(self, post_id, comment_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
            comment = m.Comment.get(m.Comment.id == comment_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        post.comment_like_count -= comment.like_count
        post.comment_count -= 1
        comment.delete_instance()
        post.update_pop_score()
        post.save()


class CommentLike(UserRequest):
    """ /bb/posts/{id}/comments/{id}/like """

    def post(self, post_id, comment_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
            comment = m.Comment.get(m.Comment.id == comment_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({'like': qv.Boolean})\
                .validate({'like': self.get_argument('like')})
        except v.ValidationError:
            raise HTTPError(400)
        like = d.get('like')
        if like:
            user_like_comment = m.UserLikeComment.create(user=self.user,comment=comment)
            comment.like_count += 1
            post.comment_like_count += 1
        else:
            x = m.UserLikeComment
            q = x.delete().where(x.user == self.user, x.comment == comment)
            q.execute()
            comment.like_count -= 1
            post.comment_like_count -= 1

        comment.update_pop_score()
        comment.save()
        post.update_pop_score()
        post.save()
        self.write({'status': 'Ok', 'msg': 'like saved'})


class CommentHide(UserRequest):
    """ /bb/posts/{id}/comments/{id}/hide """

    def post(self, post_id, comment_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
            comment = m.Comment.get(m.Comment.id == comment_id)
        except pv.DoesNotExist:
            raise HTTPError(404)

        try:
            d = v.parse({'hide': qv.Boolean})\
                .validate({'hide': self.get_argument('hide')})
        except v.ValidationError:
            raise HTTPError(400)
        hide = d.get('hide')
        if hide:
            user_hide_comment = m.UserHideComment.create(user=self.user,comment=comment)
            # comment.like_count += 1
            # post.comment_like_count += 1
        else:
            x = m.UserHideComment
            q = x.delete().where(x.user == self.user, x.comment == comment)
            q.execute()
            # comment.like_count -= 1
            # post.comment_like_count -= 1

        comment.update_pop_score()
        comment.save()
        # post.update_pop_score()
        # post.save()
        self.write({'status': 'Ok', 'msg': 'Comment hidden'})


class CommentReport(UserRequest):
    """ /bb/posts/{id}/comments/{id}/report """

    def post(self, post_id, comment_id):
        try:
            post = m.Post.get(m.Post.id == post_id)
            comment = m.Comment.get(m.Comment.id == comment_id)
        except pv.DoesNotExist:
            raise HTTPError(404)
        try:
            d = v.parse({
                '+opt': v.AdaptBy(int)
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            raise HTTPError(400)

        opt = d.get('opt')
        try:
            category = m.ReportCategory.get(m.ReportCategory.id == opt)
        except pv.DoesNotExist:
            raise HTTPerror(400)

        try:
            report = m.Report.get(
                m.Report.content_id == comment.id,
                m.Report.content_type == 'comment'
            )
        except pv.DoesNotExist:
            report = None

        if not report:
            new_report = m.Report.create(
                content_type='comment',
                content_id=comment.id,
                owner=self.user,
                created_at=datetime.datetime.utcnow()
            )

        r = report if report else new_report
        m.ReportCategoryRel.create(
            report=r,
            category=category,
            user=self.user,
            other_reason='-',
            created_at=datetime.datetime.utcnow()
        )

        r.report_count += 1
        r.save()
        comment.report_count += 1
        # post.update_pop_score() # unless this report is approved
        comment.save()
        self.write({'status': 'Ok', 'msg': 'report saved'})


class ReportCategory(UserRequest):
    """ /bb/reports/categories """
    def get(self):
        try:
            d = v.parse({'+type': 'string'}).validate({'type': self.get_argument('type')})
        except v.ValidationError:
            raise HTTPError(400)
        type = d.get('type')
        if not type in ['post','comment']:
            raise HTTPError(400)
        cats = m.ReportCategory.select().where(m.ReportCategory.tags.contains(type))
        cats_dict = [cat.dict() for cat in cats]
        self.write({'status': 'Ok', 'categories': cats_dict})


class ProductURL(UserRequest):
    """ /bb/products/url """
    def get(self):
        product = None
        try:
            d = v.parse({
                '+url': qv.ValidURL
            }).validate({k: self.get_argument(k) for k in self.request.arguments})
        except v.ValidationError, e:
            raise HTTPError(400) # bad request
        url = d.get('url', None)
        print ">>>> " + str(url)

        # https://github.com/john-kurkowski/tldextract
        cache_path = config.CACHE_PATH + '/.tld_set'
        cache_extract = tldextract.TLDExtract(cache_file=cache_path)
        ext = cache_extract(url)
        print ">>> URL: " + url
        print ">>>> subdomain: " + str(ext.subdomain)
        print ">>>> reg_domain: " + str(ext.registered_domain)
        print ">>>> domain: " + str(ext.domain)
        tld = ext.subdomain + '.' + ext.registered_domain if ext.subdomain else ext.registered_domain
        print ">>> tld: " + tld

        # Amazon product url form http://amazon.com/dp/B015IWUI1K
        if ext.registered_domain == 'amazon.com':
            product = scraper.AmazonScrapper(product_url=url)
            print ">>> product: " + str(product.dict())

        # self.write({'product':product.dict()})
