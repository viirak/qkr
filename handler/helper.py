import qkr.model as m
import peewee as pv


def dict_posts_with_comments(posts, user):
    """
    Denationalize list of post object
    :param request: HTTP Request object
    :param posts: list of Post object
    :param user: User object
    :return: list of dictionary of post object with comments
    """
    pl = []
    for p in posts:
        if not p.is_hidden(user):
            p_dict = p.dict(user)
            p_dict['is_liked'] = p.is_liked(user)
            p_dict['is_saved'] = p.is_saved(user)
            p_dict['is_shared'] = p.is_shared(user)
            p_dict['is_subscribed'] = p.is_subscribed(user)
            if p.comment_count > 0:
                # get latest 3 comments
                comments = m.Comment\
                .select()\
                .where(m.Comment.post == p, m.Comment.parent == None)\
                .order_by(m.Comment.pop_score.desc(), m.Comment.created_at.desc())\
                .limit(3)
                comments_dict = []
                for com in comments:
                    if not com.is_hidden(user):
                        com_dict = com.dict()
                        com_dict['is_like'] = com.is_liked(user)
                        if com.child_count > 0:
                            children = m.Comment\
                            .select().where(m.Comment.parent == com)\
                            .order_by(m.Comment.pop_score.desc(), m.Comment.created_at.asc())\
                            .limit(1)
                            children_dict = []
                            for child in children:
                                child_dict = child.dict()
                                child_dict['is_like'] = child.is_liked(user)
                                children_dict.append(child_dict)
                            com_dict['children'] = children_dict
                        comments_dict.append(com_dict)
                p_dict['comments'] = comments_dict
            pl.append(p_dict)
    return pl
