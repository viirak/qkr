import sys
import re
import getpass
from model import User


def create_user():
    new_user = {}
    # input username
    while True:
        username = raw_input('Username: ')
        if not re.match("^[a-zA-Z]*$", username):
            print 'Special characters are now allowed in username'
        elif len(username) < 4:
            print 'Username is too shot. Please try again'
        else:
            if User.select(User.username).where(User.username == username).count() > 0:
                print 'username [ %s ] exists! please choose another one and try it again!' % username
            else:
                new_user['username'] = username
                break
    # input email
    while True:
        email = raw_input('Email: ')
        if not re.match(".+@.+\..+", email):
            print 'Email not valid. Please try again'
        else:
            if User.select(User.email).where(User.email == email).count() > 0:
                print 'Email [ %s ] exists! please choose another one and try it again!' % email
            else:
                new_user['email'] = email
                break
    # input password
    while True:
        pwd = getpass.getpass('Password:')
        if len(pwd) < 8:
            print 'Password is too short (at least 8 characters). Please try again'
        else:
            new_user['password'] = pwd
            break

    print 'Creating new user ...'
    User.create(
        username=new_user.get('username'),
        email=new_user.get('email'),
        password=User.create_password(new_user.get('password')),
        status='active',
    )
    print '%s created!' % new_user.get('username')
    sys.exit(0)


if __name__ == '__main__':
    create_user()