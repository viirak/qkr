import time
import datetime
import hashlib
import random
import ast

import peewee as pv

import util

from math import log


# db = pv.MySQLDatabase(
#     'qkr',  # Required by Peewee.
#     user='root',  # Will be passed directly to psycopg2.
#     password='12345678',  # Ditto.
#     host='192.168.33.13',  # Ditto.
#     port=3306,
#     charset='utf8mb4'  # allow PyMySql to talk emoji
# )

db = pv.PostgresqlDatabase(
    'qkr',  # Required by Peewee.
    user='qkr',  # Will be passed directly to psycopg2.
    password='123456',  # Ditto.
    host='192.168.33.14',  # Ditto.
    port=5432
)
# or
# qkr_db = 'mysql://root:12345678@192.168.33.13:3306/qkr?charset=latin1'
# database = connect(qkr_db)


class BaseModel(pv.Model):
    class Meta:
        database = db


class EnumField(pv.Field):
    """ enable enum type for peewee.MySQLDatabase
    """
    db_field = 'enum'

    def __init__(self, *args, **kwargs):
        self.enum_values = None
        if "values" in kwargs:
            self.enum_values = kwargs['values']
        pv.Field.__init__(self, kwargs)

    def db_value(self, value):
        if self.enum_values is None:
            return str(value)
        if value in self.enum_values or value in range(len(self.enum_values)):
            return value
        else:
            return ""

    def python_value(self, value):
        return str(value)

# pv.MySQLDatabase.register_fields({'enum': 'ENUM'})
pv.PostgresqlDatabase.register_fields({'enum': 'ENUM'})


class User(BaseModel):
    id = pv.PrimaryKeyField()
    email = pv.CharField(max_length=50, null=False)
    username = pv.CharField(max_length=30, null=False)
    password = pv.FixedCharField(max_length=32, null=False)
    status = EnumField(['active', 'inactive', 'cancelled', 'suspended', 'deleted'])
    follower_count = pv.IntegerField(null=False, default=0)
    following_count = pv.IntegerField(null=False, default=0)
    post_count = pv.IntegerField(null=False, default=0)
    comment_count = pv.IntegerField(null=False, default=0)
    save_count = pv.IntegerField(null=False, default=0)
    collection_count = pv.IntegerField(null=False, default=0)
    share_count = pv.IntegerField(null=False, default=0)
    invite_quota = pv.IntegerField(null=False, default=0)
    pop_score = pv.FloatField(null=False, default=0.0)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())
    activated_at = pv.DateTimeField()
    suspended_at = pv.DateTimeField()
    cancelled_at = pv.DateTimeField()
    login_at = pv.DateTimeField()
    password_changed_at = pv.DateTimeField()

    class Meta:
        db_table = 'user'

    def dict_basic(self):
        try:
            detail = UserDetail.select().where(UserDetail.id == self.id).get()
        except pv.DoesNotExist:
            detail = None
        return dict(
            id=self.id,
            email=self.email,
            username=self.username,
            status=self.status,
            post_count=self.post_count,
            share_count=self.share_count,
            save_count=self.save_count,
            collection_count=self.collection_count,
            follower_count=self.follower_count,
            following_count=self.following_count,
            created_at=self.created_at.strftime("%s"),
            password_changed_at=self.password_changed_at.strftime("%s") if self.password_changed_at else None,
            detail=detail.dict(self) if detail else None,
            permalink='/#/'+self.username
        )

    @staticmethod
    def create_password(raw):
        salt = util.gen_token(8)
        pwd = '%s%s%s' % (salt, raw, 'qkr')
        hsh = hashlib.sha1(pwd).hexdigest()[:22]
        spt = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '=']
        return "%s%s%s" % (salt, random.choice(spt), hsh)

    def check_password(self, raw):
        spt = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '=']
        s = next((x for x in spt if x in self.password), None)
        if not s:
            return False
        salt, hsh = str(self.password).split(s)
        pwd = '%s%s%s' % (salt, raw, 'qkr')
        return hsh == hashlib.sha1(pwd).hexdigest()[:22]

    def update_pop_score(self):
        self.pop_score = util.cal_pop_score(
            created_at=self.created_at,
            post_count=self.post_count,
            comment_count=self.comment_count,
            save_count=self.save_count,
            share_count=self.share_count,
            collection_count=self.collection_count,
            follower_count=self.follower_count,
            following_count=self.following_count
        )

    def is_followed(self, user):
        x = UserFollowUser
        xx = x.select(x.follower, x.followed).where(x.follower == user, x.followed == self)
        return True if any(assoc.follower == user for assoc in xx) else False


class UserToken(BaseModel):
    id = pv.PrimaryKeyField()
    user = pv.ForeignKeyField(User, related_name='tokens')
    token = pv.FixedCharField(max_length=32, null=False)
    email = pv.CharField(max_length=50, null=False)
    expired_at = pv.DateTimeField(null=False)
    tag = pv.CharField(max_length=50, null=False)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'user_token'

    def dict(self):
        return dict(
            email=self.email,
            tag=self.tag,
            expired_at=self.expired_at.strftime("%s")
        )


class UserDetail(BaseModel):
    id = pv.PrimaryKeyField()
    gender = EnumField(['male', 'female', 'other'], default='other')
    birthday = pv.DateField()
    picture = pv.CharField(max_length=255)
    full_name = pv.CharField(max_length=50)
    bio = pv.CharField(max_length=255)
    modified_at = pv.DateTimeField()
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'user_detail'

    def dict(self, user):
        profile_image = util.QkImage(user, 'proimg')
        return dict(
            id=self.id,
            gender=self.gender,
            birthday=self.birthday.strftime('%m/%d/%Y') if self.birthday else None,
            full_name=self.full_name,
            bio=self.bio,
            picture_url_m=profile_image.get_static_url(self.picture, width=250, include_host=False) if self.picture else None,
            picture_url_s=profile_image.get_static_url(self.picture, width=48, include_host=False) if self.picture else None,
            created_at=self.created_at.strftime("%s")
        )


class Topic(BaseModel):
    id = pv.PrimaryKeyField()
    name = pv.CharField(max_length=50, null=False)
    slug = pv.CharField(max_length=50, null=False)
    description = pv.CharField(max_length=100)
    parent = pv.ForeignKeyField('self', null=True, related_name='children')
    picture = pv.CharField(max_length=255)
    post_count = pv.IntegerField(null=False, default=0)
    follower_count = pv.IntegerField(null=False, default=0)
    is_locked = pv.BooleanField(null=False, default=False)
    pop_score = pv.FloatField(null=False, default=0.0)
    owner = pv.ForeignKeyField(User, related_name='topics')
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'topic'

    def dict(self):
        topic = dict(
            id=self.id,
            name=self.name,
            slug=self.slug,
            description=self.description,
            parent=self.parent,
            picture=self.picture,
            post_count=self.post_count,
            follower_count=self.follower_count,
            is_locked=self.is_locked,
            owner_id=self.owner.id,
            created_at=self.created_at.strftime("%s")
        )
        return topic

    def update_pop_score(self):
        self.pop_score = util.cal_pop_score(
            created_at=self.created_at,
            post_count=self.post_count,
            follower_count=self.follower_count
        )


class Tag(BaseModel):

    id = pv.PrimaryKeyField()
    name = pv.CharField(max_length=50, null=False)
    slug = pv.CharField(max_length=50, null=False)
    post_count = pv.IntegerField(null=False, default=0)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    parent = pv.ForeignKeyField('self', null=True, related_name='children')
    owner = pv.ForeignKeyField(User, related_name='tags')

    class Meta:
        db_table = 'tag'

    def dict(self):
        return dict(
            id=self.id,
            name=self.name,
            slug=self.slug,
            parent=self.parent,
            post_count=self.post_count,
            owner_id=self.owner.id,
            created_at=self.created_at.strftime("%s")
        )


class Collection(BaseModel):

    id = pv.PrimaryKeyField()
    name = pv.CharField(max_length=50, null=False)
    slug = pv.CharField(max_length=50, null=False)
    description = pv.CharField(max_length=100, null=False, default='-')
    picture = pv.CharField(max_length=255, null=False, default='-')
    post_count = pv.IntegerField(null=False, default=0)
    follower_count = pv.IntegerField(null=False, default=0)
    is_private = pv.BooleanField(null=False, default=False)
    pop_score = pv.FloatField(null=False, default=0.0)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    owner = pv.ForeignKeyField(User, related_name='collections')

    class Meta:
        db_table = 'collection'

    def dict(self):
        return dict(
            id=self.id,
            name=self.name,
            slug=self.slug,
            description=self.description,
            picture=self.picture,
            post_count=self.post_count,
            follower_count=self.follower_count,
            owner=self.owner.dict_basic(),
            created_at=self.created_at.strftime("%s")
        )

    def update_pop_score(self):
        self.pop_score = util.cal_pop_score(
            created_at=self.created_at,
            post_count=self.post_count,
            follower_count=self.follower_count
        )


class Post(BaseModel):
    id = pv.PrimaryKeyField()
    status = EnumField(['draft', 'future', 'private', 'publish', 'trash'])
    like_count = pv.IntegerField(null=False, default=0)
    comment_count = pv.IntegerField(null=False, default=0)
    comment_like_count = pv.IntegerField(null=False, default=0)
    share_count = pv.IntegerField(null=False, default=0)
    report_count = pv.IntegerField(null=False, default=0)
    view_count = pv.IntegerField(null=False, default=0)
    save_count = pv.IntegerField(null=False, default=0)
    pop_score = pv.FloatField(null=False, default=0.0)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())
    published_at = pv.DateTimeField()
    trashed_at = pv.DateTimeField()
    # topic_id = pv.IntegerField(null=False)

    parent = pv.ForeignKeyField('self', null=True, related_name='children')
    owner = pv.ForeignKeyField(User, related_name='posts')
    # topic = pv.ForeignKeyField(Topic, related_name='posts')

    class Meta:
        db_table = 'post'

    def dict(self, user=None):
        post = dict(
            id=self.id,
            like_count=self.like_count,
            comment_count=self.comment_count,
            comment_like_count=self.comment_like_count,
            share_count=self.share_count,
            view_count=self.view_count,
            save_count=self.save_count,
            created_at=self.created_at.strftime("%s"),
            published_at=self.published_at.strftime("%s") if self.published_at else None,
            owner=self.owner.dict_basic(),
            # topics=[t.dict() for t in self.get_topics()] if self.get_topics() else None,
            tags=[t.dict() for t in self.get_tags()] if self.get_tags() else None,
            photos=None,
            pop_score=self.pop_score,
            parent=self.parent.dict(user) if self.parent else None,
            permalink=self.get_permalink()
        )
        try:
            post_text = PostText.get(PostText.post == self)
        except pv.DoesNotExist:
            post_text = None
        post['text'] = post_text.dict() if post_text else None

        if user:
            try:
                post_product = Product.get(Product.post == self)
            except pv.DoesNotExist:
                post_product = None
            post['product'] = post_product.dict(user) if post_product else None

        return post

    def get_topics(self):
        topics = Topic.select().join(PostTopicRel).where(PostTopicRel.post == self)
        return topics if topics else None

    def get_tags(self):
        tags = Tag.select().join(PostTagRel).where(PostTagRel.post == self)
        return tags if tags else None

    def get_topic(self):
        return self.topic.dict()

    def is_liked(self, user):
        x = UserLikePost
        xx = x.select(x.user, x.post).where(x.post == self, x.user == user)
        return True if any(assoc.user == user for assoc in xx) else False

    def is_saved(self, user):
        x = UserSavePost
        xx = x.select(x.user, x.post).where(x.post == self, x.user == user)
        return True if any(assoc.user == user for assoc in xx) else False

    def is_shared(self, user):
        x = UserSharePost
        xx = x.select(x.user, x.post).where(x.post == self, x.user == user)
        return True if any(assoc.user == user for assoc in xx) else False

    def is_hidden(self, user):
        x = UserHidePost
        xx = x.select(x.user, x.post).where(x.post == self, x.user == user)
        return True if any(assoc.user == user for assoc in xx) else False

    def is_subscribed(self, user):
        x = UserSubscribePost
        xx = x.select(x.user, x.post, x.is_active).where(x.post == self, x.user == user, x.is_active == True)
        return True if any(assoc.user == user for assoc in xx) else False

    def set_pop_score(self, score):
        self.pop_score = score

    def update_pop_score(self):
        self.pop_score = util.cal_pop_score(
            created_at=self.created_at,
            like_count=self.like_count,
            comment_count=self.comment_count,
            comment_like_count=self.comment_like_count,
            save_count=self.save_count,
            share_count=self.share_count,
            report_count=self.report_count
        )

    def get_permalink(self):
        # return '/#/posts/' + util.encrypt_id(self.id)
        return '/#/posts/' + str(self.id)


class PostText(BaseModel):
    id = pv.PrimaryKeyField()
    text = pv.CharField(max_length=500, null=False)
    source = pv.CharField(max_length=255, null=False, default='-')
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    owner = pv.ForeignKeyField(User, related_name='post_texts')
    post = pv.ForeignKeyField(Post, related_name='text')

    class Meta:
        db_table = 'text'

    def dict(self):
        return dict(
            id=self.id,
            text=self.text,
            source=self.source,
            created_at=self.created_at.strftime("%s")
        )

class Brand(BaseModel):
    id = pv.PrimaryKeyField()
    name = pv.CharField(max_length=30, null=False)
    logo_path = pv.CharField(max_length=255)
    product_count = pv.IntegerField(null=False, default=0)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'product_brand'

    def dict(self):
        return dict(
            id=self.id,
            name=self.name,
            logo_path=self.logo_path,
            product_count=self.product_count,
            created_at=self.created_at.strftime("%s")
        )

class Product(BaseModel):
    id = pv.PrimaryKeyField()
    title = pv.CharField(max_length=255, null=False)
    offer_url = pv.CharField(max_length=255, null=False)
    image_url = pv.CharField(max_length=255, null=False)
    features = pv.CharField(max_length=5000)
    domain = pv.CharField(max_length=30, null=False)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    brand = pv.ForeignKeyField(Brand, related_name='products')
    owner = pv.ForeignKeyField(User, related_name='post_products')
    post = pv.ForeignKeyField(Post, related_name='product')

    class Meta:
        db_table = 'product'

    def dict(self, user):
        product_image = util.QkImage(user, 'productimg')
        d = dict(
            id=self.id,
            title=self.title,
            offer_url=self.offer_url,
            image_url=product_image.get_static_url(self.image_url) if self.image_url else None,
            features=ast.literal_eval(self.features) if self.features else None,
            domain=self.domain,
            owner=self.owner.dict_basic(),
            brand=self.brand.dict() if self.brand else None,
            created_at=self.created_at.strftime("%s")
        )

        product_prices = ProductPrice.select().where(ProductPrice.product == self).order_by(ProductPrice.id.desc())
        d['prices'] = [p.dict() for p in self.prices] if product_prices.count() > 0 else None
        return d

class ProductPrice(BaseModel):
    id = pv.PrimaryKeyField()
    price = pv.FloatField(null=False, default=0.0)
    currency = pv.CharField(max_length=3, null=False)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())
    product = pv.ForeignKeyField(Product, related_name='prices')

    class Meta:
        db_table = 'product_price'

    def dict(self):
        return dict(
            id=self.id,
            price=self.price,
            currency=self.currency,
            created_at=self.created_at.strftime("%s")
        )


class Comment(BaseModel):
    id = pv.PrimaryKeyField()
    text = pv.CharField(max_length=500, null=False)
    like_count = pv.IntegerField(null=False, default=0)
    child_count = pv.IntegerField(null=False, default=0)
    report_count = pv.IntegerField(null=False, default=0)
    pop_score = pv.FloatField(null=False, default=0.0)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    parent = pv.ForeignKeyField('self', null=True, related_name='children')
    owner = pv.ForeignKeyField(User, related_name='comments')
    post = pv.ForeignKeyField(Post, related_name='comments')

    class Meta:
        db_table = 'comment'

    def dict(self):
        return dict(
            id=self.id,
            text=self.text,
            parent=self.parent.id if self.parent else None,
            like_count=self.like_count,
            child_count=self.child_count,
            owner=self.owner.dict_basic(),
            created_at=self.created_at.strftime("%s"),
        )

    def is_liked(self, user):
        x = UserLikeComment
        xx = x.select(x.user, x.comment).where(x.comment == self, x.user == user)
        return True if any(assoc.user == user for assoc in xx) else False

    def is_hidden(self, user):
        x = UserHideComment
        xx = x.select(x.user, x.comment).where(x.comment == self, x.user == user)
        return True if any(assoc.user == user for assoc in xx) else False

    def update_pop_score(self):
        self.pop_score = util.cal_pop_score(
            created_at=self.created_at,
            like_count=self.like_count,
            child_count=self.child_count,
            report_count=self.report_count
        )


class ReportCategory(BaseModel):
    id = pv.PrimaryKeyField()
    title = pv.CharField(max_length=100, null=False)
    tags = pv.CharField(max_length=50, null=False, default=0)
    weight = pv.IntegerField(null=False, default=0)
    parent = pv.ForeignKeyField('self', null=True, related_name='children')

    class Meta:
        db_table = 'report_category'

    def dict(self):
        return dict(
            id=self.id,
            title=self.title,
            tags=self.tags,
            weight=self.weight
        )


class Report(BaseModel):
    id = pv.PrimaryKeyField()
    content_id = pv.IntegerField(null=False, default=0)
    content_type = pv.CharField(max_length=10, null=False)
    report_count = pv.IntegerField(null=False, default=0)
    is_content_alive = pv.BooleanField(null=False, default=True)
    owner = pv.ForeignKeyField(User, related_name='reports')
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'report'

    def dict(self):
        return dict(
            id=self.id,
            content_id=self.content_id,
            content_type=self.content_type,
            report_count=self.report_count,
            is_content_alive=self.is_content_alive,
            owner=self.owner.dict_basic(),
            created_at=self.created_at.strftime("%s")
        )


##############################################################################
## RELATION TABLES
#############################################################################

# MANY TO MANY RELATIONSHIP

class PostTopicRel(BaseModel):
    post = pv.ForeignKeyField(Post, related_name='post_topic_rels')
    topic = pv.ForeignKeyField(Topic, related_name='post_topic_rels')

    class Meta:
        db_table = 'post_topic_rel'
        primary_key = pv.CompositeKey('post', 'topic')

class TopicTagRel(BaseModel):
    topic = pv.ForeignKeyField(Topic, related_name='topic_tag_rels')
    tag = pv.ForeignKeyField(Tag, related_name='topic_tag_rels')

    class Meta:
        db_table = 'topic_tag_rel'
        primary_key = pv.CompositeKey('topic', 'tag')


class PostTagRel(BaseModel):
    post = pv.ForeignKeyField(Post, related_name='post_tag_rels')
    tag = pv.ForeignKeyField(Tag, related_name='post_tag_rels')

    class Meta:
        db_table = 'post_tag_rel'
        primary_key = pv.CompositeKey('post', 'tag')


class PostCollectionRel(BaseModel):
    post = pv.ForeignKeyField(Post, related_name='post_collection_rels')
    collection = pv.ForeignKeyField(Collection, related_name='post_collection_rels')

    class Meta:
        db_table = 'post_collection_rel'
        primary_key = pv.CompositeKey('post', 'collection')


class ReportCategoryRel(BaseModel):
    report = pv.ForeignKeyField(Report, related_name='report_category_rels')
    category = pv.ForeignKeyField(ReportCategory, related_name='report_category_rels')
    user = pv.ForeignKeyField(User, related_name='report_category_rels')
    other_reason = pv.CharField(max_length=100, null=False, default='-')
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'report_category_rel'


# USER VS POST
class UserLikePost(BaseModel):
    user = pv.ForeignKeyField(User, related_name='user_like_post')
    post = pv.ForeignKeyField(Post, related_name='user_like_post')
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'user_like_post'
        primary_key = pv.CompositeKey('user', 'post')


class UserHidePost(BaseModel):
    user = pv.ForeignKeyField(User, related_name='user_hide_post')
    post = pv.ForeignKeyField(Post, related_name='user_hide_post')

    class Meta:
        db_table = 'user_hide_post'
        primary_key = pv.CompositeKey('user', 'post')


class UserSubscribePost(BaseModel):
    user = pv.ForeignKeyField(User, related_name='user_subscribe_post')
    post = pv.ForeignKeyField(Post, related_name='user_subscribe_post')
    is_active = pv.BooleanField(null=False, default=True)

    class Meta:
        db_table = 'user_subscribe_post'
        primary_key = pv.CompositeKey('user', 'post')


class UserSavePost(BaseModel):
    user = pv.ForeignKeyField(User, related_name='user_save_posts')
    post = pv.ForeignKeyField(Post, related_name='user_save_posts')
    collection = pv.ForeignKeyField(Collection, related_name='user_save_posts')
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'user_save_post'
        primary_key = pv.CompositeKey('user', 'post')


class UserSharePost(BaseModel):
    id = pv.PrimaryKeyField()
    user = pv.ForeignKeyField(User, related_name='user_share_posts')
    post = pv.ForeignKeyField(Post, related_name='user_share_posts')
    comment = pv.CharField(max_length=500, null=False)
    via_user_id = pv.IntegerField(null=False)
    via_post_id = pv.IntegerField(null=False)
    in_post_id = pv.IntegerField(null=False)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'user_share_post'


# USER VS COMMENT
class UserLikeComment(BaseModel):
    user = pv.ForeignKeyField(User, related_name='user_like_comment')
    comment = pv.ForeignKeyField(Comment, related_name='user_like_comment')

    class Meta:
        db_table = 'user_like_comment'
        primary_key = pv.CompositeKey('user', 'comment')


class UserHideComment(BaseModel):
    user = pv.ForeignKeyField(User, related_name='user_hide_comment')
    comment = pv.ForeignKeyField(Comment, related_name='user_hide_comment')

    class Meta:
        db_table = 'user_hide_comment'
        primary_key = pv.CompositeKey('user', 'comment')


# USER VS USER
class UserFollowUser(BaseModel):
    follower = pv.ForeignKeyField(User, related_name='user_follow_user')
    followed = pv.ForeignKeyField(User, related_name='user_followed_user')
    is_notified = pv.BooleanField(null=False, default=False)
    created_at = pv.DateTimeField(null=False, default=datetime.datetime.utcnow())

    class Meta:
        db_table = 'user_follow_user'
        primary_key = pv.CompositeKey('follower', 'followed')
